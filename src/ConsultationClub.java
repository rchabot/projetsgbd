package federationbasket.src;

import java.sql.*;

public class ConsultationClub {

    public static ResultSet infoClub(String nomClub) throws SQLException{
	String SQL = "SELECT * FROM CLUB "
	    + "WHERE CLUB_NOM = UPPER(?);";

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	
	ResultSet rs = stmt.executeQuery();
	
	return rs;
    }


    public static ResultSet bureauClub(String nomClub) throws SQLException{
	String SQL = "SELECT p.PERSONNE_PRENOM as PRENOM, p.PERSONNE_NOM as NOM, pm.FONCTION, p.PERSONNE_DATE_ENTREE AS DATE_ENTREE  "
	    + "FROM PERSONNE p, PERSONNE_MORALE pm, CLUB c "
	    + "WHERE p.PERSONNE_ID = pm.PERSONNE_ID "
	    + "AND p.CLUB_ID = c.CLUB_ID "
	    + "AND c.CLUB_NOM = UPPER(?);";
	
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet listeJoueurs(String nomClub) throws SQLException{
	String SQL = "SELECT j.JOUEUR_ID, p.PERSONNE_PRENOM as PRENOM, p.PERSONNE_NOM as NOM, j.JOUEUR_NUM_LICENCE as LICENCE, g.CLUB_NOM as CLUB, g.CATEGORIE_NOM as CATEGORIE, g.EQUIPE_NUMERO as NUMERO_EQUIPE, p.PERSONNE_DATE_ENTREE AS DATE_ENTREE "
	    + "FROM JOUEUR j, PERSONNE p, GetEquipe g "
	    + "WHERE p.PERSONNE_ID = j.JOUEUR_ID "
	    + "AND j.EQUIPE_ID = g.EQUIPE_ID "
	    + "AND g.CLUB_NOM = ? ; ";
	
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);	
	stmt.setString(1, nomClub);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet listePersonnesAtDate(String nomClub, String date) throws SQLException{
	String CLUB_ID = "(select CLUB_ID from CLUB where CLUB_NOM = ? )";
	String SQL = "SELECT p.PERSONNE_PRENOM as PRENOM, p.PERSONNE_NOM as NOM, p.PERSONNE_DATE_ENTREE AS DATE_ENTREE "
	    + "FROM PERSONNE p "
	    + "WHERE p.CLUB_ID = " + CLUB_ID
	    + "AND p.PERSONNE_DATE_ENTREE = ?; ";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);	
	stmt.setString(1, nomClub);
	stmt.setString(2, date);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet listePersonnesToDate(String nomClub, String date) throws SQLException{
	String CLUB_ID = "(select CLUB_ID from CLUB where CLUB_NOM = ? )";
	String SQL = "SELECT p.PERSONNE_PRENOM as PRENOM, p.PERSONNE_NOM as NOM, p.PERSONNE_DATE_ENTREE AS DATE_ENTREE "
	    + "FROM PERSONNE p "
	    + "WHERE p.CLUB_ID = " + CLUB_ID
	    + "AND p.PERSONNE_DATE_ENTREE <= ?; ";
	
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);	
	stmt.setString(1, nomClub);
	stmt.setString(2, date);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet listeEntraineurs(String nomClub) throws SQLException{
	String SQL = "SELECT p.PERSONNE_PRENOM as PRENOM, p.PERSONNE_NOM as NOM, ent.EQUIPE_ID, g.CLUB_NOM as CLUB, g.CATEGORIE_NOM as CATEGORIE, g.EQUIPE_NUMERO as NUMERO_EQUIPE, p.PERSONNE_DATE_ENTREE AS DATE_ENTREE "
	    + "FROM PERSONNE p, GetEquipe g, ENTRAINE ent "
	    + "WHERE p.PERSONNE_ID = ent.ENTRAINEUR_ID "
	    + "AND ent.EQUIPE_ID = g.EQUIPE_ID "
	    + "AND g.CLUB_NOM = ? ; ";
	
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);	
	stmt.setString(1, nomClub);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    

    public static ResultSet resultatsClub(String nomClub, String dateMin, String dateMax) throws SQLException{
	String SQL = "SELECT eqA.CLUB_NOM AS EQUIPE_A, eqA.CATEGORIE_NOM AS CATEGORIE, eqA.EQUIPE_NUMERO AS NUM, SCORE_EQUIPE_A, SCORE_EQUIPE_B, eqB.CLUB_NOM AS EQUIPE_B, eqB.CATEGORIE_NOM AS CAT, eqB.EQUIPE_NUMERO AS NUM, r.RENCONTRE_DATE AS DATE, r.NUMERO_SAISON AS SAISON, r.NUMERO_JOURNEE AS JOURNEE "
	    + "FROM RENCONTRE r, GetEquipe eqA, GetEquipe eqB "
	    + "WHERE (eqA.CLUB_NOM = ? OR eqB.CLUB_NOM = ?) "
	    + "AND r.EQUIPE_A_ID = eqA.EQUIPE_ID "
	    + "AND r.EQUIPE_B_ID = eqB.EQUIPE_ID "
	    + "AND r.RENCONTRE_DATE <= ? "
	    + "AND r.RENCONTRE_DATE >= ? "
	    + "ORDER BY r.RENCONTRE_DATE ;";
	
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	stmt.setString(2, nomClub);
	stmt.setString(3, dateMax);
	stmt.setString(4, dateMin);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }


}
