package federationbasket.src;

import java.sql.*;

public class Statistiques {
    
    /*
     * TYPE correspond au classeùment que l'on veut (classements des fautes ou classement par points marqués
     */
    public static ResultSet classementJoueurs(String categorie, int numSaison, int numJournee, String TYPE) throws SQLException{
	String SQL = "select p.PERSONNE_PRENOM, p.PERSONNE_NOM, s.STATS_" + TYPE + " "
	    + "from RENCONTRE r, CATEGORIE cat, EQUIPE e, JOUEUR j, PERSONNE p, STATS s "
	    + "where j.JOUEUR_ID = p.PERSONNE_ID "
	    + "and j.JOUEUR_ID = s.JOUEUR_ID "
	    + "and r.RENCONTRE_ID = s.RENCONTRE_ID "
	    + "and r.NUMERO_SAISON = ?  "
	    + "and r.NUMERO_JOURNEE = ? "
	    + "and (cat.CATEGORIE_NOM = ? "
	    + "and cat.CATEGORIE_ID = e.CATEGORIE_ID "
	    + "and e.CATEGORIE_ID = cat.CATEGORIE_ID "
	    + "and j.EQUIPE_ID = e.EQUIPE_ID) "
	    + "order by STATS_" + TYPE + " desc ;";

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1, numSaison);
	stmt.setInt(2, numJournee);
	stmt.setString(3, categorie);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static double moyenneJoueur(int joueurId, int numSaison, String TYPE) {
	String SQL = "SELECT AVG(STATS_"+TYPE+") "
	    + "FROM ( "
	    + "SELECT STATS_"+TYPE + " "
	    + "FROM RENCONTRE r, STATS s "
	    + "WHERE s.JOUEUR_ID = ? "
	    + "AND s.RENCONTRE_ID = r.RENCONTRE_ID "
	    + "AND r.NUMERO_SAISON = ? "
	    + ") AS RESULT ;";
	double res = 0.0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setInt(1, joueurId);
	    stmt.setInt(2, numSaison);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    res = rs.getDouble(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return res;
    }


    public static double moyenneJoueur(int joueurId, String date, String TYPE) {
	String SQL = "SELECT AVG(STATS_"+TYPE+") "
	    + "FROM ( "
	    + "SELECT STATS_"+TYPE + " "
	    + "FROM RENCONTRE r, STATS s "
	    + "WHERE s.JOUEUR_ID = ? "
	    + "AND s.RENCONTRE_ID = r.RENCONTRE_ID "
	    + "AND r.RENCONTRE_DATE <= ? "
	    + ") AS RESULT ;";
	double res = 0.0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setInt(1, joueurId);
	    stmt.setString(2, date);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    res = rs.getDouble(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return res;
    }

    public static double moyenneClub(int clubId, int numSaison){
	return 7.0;
    }

    public static double moyenneClub(int clubId, String date){
	return 7.0;
    }

    public static double moyenneEquipe(int equipeId, int numSaison) {
	String SQL = "SELECT AVG(POINTS) "
	    + "FROM( "
	    + "select SCORE_EQUIPE_A AS POINTS "
	    + "FROM RENCONTRE r "
	    + "WHERE r.EQUIPE_A_ID = ? "
	    + "AND r.NUMERO_SAISON = ? "
	    + "UNION "
	    + "select SCORE_EQUIPE_B AS POINTS "
	    + "FROM RENCONTRE r "
	    + "WHERE r.EQUIPE_B_ID = ? "
	    + "AND r.NUMERO_SAISON = ? "
	    + ") AS RESULT ;";
	double res = 0.0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setInt(1, equipeId);
	    stmt.setInt(2, numSaison);
	    stmt.setInt(3, equipeId);
	    stmt.setInt(4, numSaison);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    res = rs.getDouble(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return res;
    }

        public static double moyenneEquipe(int equipeId, String date) {
	String SQL = "SELECT AVG(POINTS) "
	    + "FROM( "
	    + "select SCORE_EQUIPE_A AS POINTS "
	    + "FROM RENCONTRE r "
	    + "WHERE r.EQUIPE_A_ID = ? "
	    + "AND r.RENCONTRE_DATE <= ? "
	    + "UNION "
	    + "select SCORE_EQUIPE_B AS POINTS "
	    + "FROM RENCONTRE r "
	    + "WHERE r.EQUIPE_B_ID = ? "
	    + "AND r.RENCONTRE_DATE <= ? "
	    + ") AS RESULT ;";
	double res = 0.0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setInt(1, equipeId);
	    stmt.setString(2, date);
	    stmt.setInt(3, equipeId);
	    stmt.setString(4, date);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    res = rs.getDouble(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return res;
    }

    public static double moyenneParMatch(int numSaison) {
	String PTS_MATCH = 
	    "(select sum(score) as pts_marques_match, RENCONTRE_ID "
	    + "from (select r.SCORE_EQUIPE_A as score, r.RENCONTRE_ID "
	          + "from RENCONTRE r	"
	          + "where r.NUMERO_SAISON = ? "
	          + "UNION ALL "
	          + "select r.SCORE_EQUIPE_B as score, r.RENCONTRE_ID "
	          + "from RENCONTRE r "
	          + "where r.NUMERO_SAISON = ?) "
	    + "as result "
	    + "group by RENCONTRE_ID) ";

	String SQL = "select avg(pts_marques_match) "
	    + "from " + PTS_MATCH
	    + "as result ;" ;
	double res = 0.0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setInt(1, numSaison);
	    stmt.setInt(2, numSaison);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    res = rs.getDouble(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return res;
    } 

    public static double moyenneParMatch(int numSaison, String cat) {
	String CAT_ID = "(select CATEGORIE_ID "
	    + "FROM CATEGORIE c "
	    + "WHERE c.CATEGORIE_NOM = ?) ";
	
	String PTS_MATCH = 
	    "(select sum(score) as pts_marques_match, RENCONTRE_ID "
	    + "from (select r.SCORE_EQUIPE_A as score, r.RENCONTRE_ID "
	          + "from RENCONTRE r, EQUIPE eqA "
	          + "where r.NUMERO_SAISON = ? "
	          + "and r.EQUIPE_A_ID = eqA.EQUIPE_ID "
	          + "and eqA.CATEGORIE_ID = " + CAT_ID
	          + "UNION ALL "
          	  + "select r.SCORE_EQUIPE_B as score, r.RENCONTRE_ID "	                
	          + "from RENCONTRE r, EQUIPE eqB "
	          + "where r.NUMERO_SAISON = ? "
	    + "and r.EQUIPE_B_ID = eqB.EQUIPE_ID "
	    + "and eqB.CATEGORIE_ID = " + CAT_ID +") "
	    + "as result "
	    + "group by RENCONTRE_ID) ";

	String SQL = "select avg(pts_marques_match) "
	    + "from " + PTS_MATCH
	    + "as result ;" ;

	double res = 0.0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setInt(1, numSaison);
	    stmt.setString(2, cat);
	    stmt.setInt(3, numSaison);
	    stmt.setString(4, cat);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    res = rs.getDouble(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return res;
    } 

    public static double moyenneParMatch(String date) {
	String PTS_MATCH = 
	    "(select sum(score) as pts_marques_match, RENCONTRE_ID "
	    + "from (select r.SCORE_EQUIPE_A as score, r.RENCONTRE_ID "
	          + "from RENCONTRE r	"
	          + "where r.RENCONTRE_DATE <= ? "
	          + "UNION ALL "
	          + "select r.SCORE_EQUIPE_B as score, r.RENCONTRE_ID "
	          + "from RENCONTRE r "
	          + "where r.RENCONTRE_DATE <= ?) "
	    + "as result "
	    + "group by RENCONTRE_ID) ";

	String SQL = "select avg(pts_marques_match) "
	    + "from " + PTS_MATCH
	    + "as result ;" ;

	double res = 0.0;
	try{
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setString(1, date);
	    stmt.setString(2, date);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    res = rs.getDouble(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return res;
    } 

    public static double moyenneParMatch(String date, String cat){
	String CAT_ID = "(select CATEGORIE_ID "
	    + "FROM CATEGORIE c "
	    + "WHERE c.CATEGORIE_NOM = ?) ";
	
	String PTS_MATCH = 
	    "(select sum(score) as pts_marques_match, RENCONTRE_ID "
	    + "from (select r.SCORE_EQUIPE_A as score, r.RENCONTRE_ID "
	          + "from RENCONTRE r, EQUIPE eqA "
	          + "where r.RENCONTRE_DATE <= ? "
	          + "and r.EQUIPE_A_ID = eqA.EQUIPE_ID "
	          + "and eqA.CATEGORIE_ID = " + CAT_ID
	          + "UNION ALL "
          	  + "select r.SCORE_EQUIPE_B as score, r.RENCONTRE_ID "	                
	          + "from RENCONTRE r, EQUIPE eqB "
	          + "where r.RENCONTRE_DATE <= ? "
	    + "and r.EQUIPE_B_ID = eqB.EQUIPE_ID "
	    + "and eqB.CATEGORIE_ID = " + CAT_ID +") "
	    + "as result "
	    + "group by RENCONTRE_ID) ";

	String SQL = "select avg(pts_marques_match) "
	    + "from " + PTS_MATCH
	    + "as result ;" ;

	double res = 0.0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setString(1, date);
	    stmt.setString(2, cat);
	    stmt.setString(3, date);
	    stmt.setString(4, cat);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    res = rs.getDouble(1);
	    rs.close();
	} catch (SQLException e) {
	    e.printStackTrace();
	}
	return res;
    } 

    public static ResultSet classementEquipes(int numSaison, String cat) throws SQLException{
	String CAT_ID = "(select CATEGORIE_ID "
	    + "FROM CATEGORIE c "
	    + "WHERE c.CATEGORIE_NOM = ?) ";


	String POUR_CONTRE = "(SELECT EQUIPE, SUM(POUR) AS POUR, SUM(CONTRE) AS CONTRE "
	    + "FROM ( "
	    + "SELECT e.EQUIPE_ID AS EQUIPE, SCORE_EQUIPE_A AS POUR, SCORE_EQUIPE_B AS CONTRE, adv.EQUIPE_ID AS ADVERSAIRE "
	    + "FROM RENCONTRE r, EQUIPE e, EQUIPE adv "
	    + "WHERE r.EQUIPE_A_ID = e.EQUIPE_ID "
	    + "AND r.EQUIPE_A_ID = e.EQUIPE_ID "
	    + "AND r.EQUIPE_B_ID = adv.EQUIPE_ID "
	    + "AND e.CATEGORIE_ID = " + CAT_ID
	    + "AND r.NUMERO_SAISON = ? "
	    + "UNION "
	    + "SELECT e.EQUIPE_ID AS EQUIPE , SCORE_EQUIPE_B, SCORE_EQUIPE_A, adv.EQUIPE_ID AS ADVERSAIRE "
	    + "FROM RENCONTRE r, EQUIPE adv, EQUIPE e "
	    + "WHERE r.EQUIPE_B_ID = e.EQUIPE_ID "
	    + "AND r.EQUIPE_A_ID = adv.EQUIPE_ID "
	    + "AND r.EQUIPE_B_ID = e.EQUIPE_ID "
	    + "AND e.CATEGORIE_ID = " + CAT_ID
	    + "AND r.NUMERO_SAISON = ? "
	    + "ORDER BY EQUIPE) AS RESULT "
	    + "GROUP BY EQUIPE) ";

	String PTS = "(select EQUIPE_ID, sum(points) as PTS "
	    + "from (select 3*count(*) points, EQUIPE_ID "
	    + "from (select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID "
	    + "from RENCONTRE r, EQUIPE e "
	    + "where e.CATEGORIE_ID = " + CAT_ID
	    + "and r.EQUIPE_A_ID = e.EQUIPE_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B "
	    + "union "
	    + "select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID "
	    + "from RENCONTRE r, EQUIPE e "
	    + "where e.CATEGORIE_ID = " + CAT_ID
	    + "and r.EQUIPE_B_ID = e.EQUIPE_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B) victoire "
	    + "group by EQUIPE_ID "

	    + "UNION "

	    + "select count(*) points,EQUIPE_ID "
	    + "from (select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID "
	    + "from RENCONTRE r,  EQUIPE e "
	    + "where e.CATEGORIE_ID = " + CAT_ID
	    + "and r.EQUIPE_A_ID = e.EQUIPE_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B "
	    + "union "
	    + "select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID "
	    + "from RENCONTRE r,  EQUIPE e "
	    + "where  e.CATEGORIE_ID = " + CAT_ID
	    + "and r.EQUIPE_B_ID = e.EQUIPE_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B) nul "
	    + "group by EQUIPE_ID "

	    + "UNION "

	    + "select 0*count(*) points, EQUIPE_ID AS ID "
	    + "from ( "
	    + "select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID "
	    + "from RENCONTRE r, EQUIPE e "
	    + "where e.CATEGORIE_ID = " + CAT_ID
	    + "and r.EQUIPE_A_ID = e.EQUIPE_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B "
	    + "UNION "
	    + "select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID "
	    + "from RENCONTRE r, EQUIPE e "
	    + "where e.CATEGORIE_ID = " + CAT_ID
	    + "and r.EQUIPE_B_ID = e.EQUIPE_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B) victoire "
      	    + "group by EQUIPE_ID) as total "

	    + "group by EQUIPE_ID "
	    + "ORDER BY sum(points) desc) ";
	
	String NUL = "(SELECT e.EQUIPE_ID, COUNT(ID_NUL) AS nb_nuls "
	    + "FROM EQUIPE e "
	    + "LEFT JOIN ( "
	    + "select r.RENCONTRE_ID, e.EQUIPE_ID AS ID_NUL "
	    + "from RENCONTRE r, EQUIPE e "
	    + "where e.EQUIPE_ID = r.EQUIPE_A_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B "
	    + "UNION "
	    + "select r.RENCONTRE_ID, e.EQUIPE_ID AS ID_NUL "
	    + "from RENCONTRE r, EQUIPE e "
	    + "where e.EQUIPE_ID = r.EQUIPE_B_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B "
	    + "ORDER BY RENCONTRE_ID) AS NUL "
	    + "ON e.EQUIPE_ID = NUL.ID_NUL "
	    + "WHERE e.CATEGORIE_ID = " + CAT_ID
	    + "group by e.equipe_id) ";
	
	String VICTOIRE = "(SELECT e.EQUIPE_ID, COUNT(VAINQUEUR_ID) AS nb_victoires "
	    + "FROM EQUIPE e "
	    + "LEFT JOIN ( "
	    + "select r.RENCONTRE_ID, e.EQUIPE_ID AS VAINQUEUR_ID "
	    + "from RENCONTRE r, EQUIPE e "
	    + "where e.EQUIPE_ID = r.EQUIPE_A_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B "
	    + "UNION "
	    + "select r.RENCONTRE_ID, e.EQUIPE_ID AS VAINQUEUR_ID "
	    + "from RENCONTRE r, EQUIPE e "
	    + "where e.EQUIPE_ID = r.EQUIPE_B_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B "
	    + "ORDER BY RENCONTRE_ID ) AS VICTOIRE  "
	    + "ON e.EQUIPE_ID = VICTOIRE.VAINQUEUR_ID "
	    + "WHERE e.CATEGORIE_ID = " + CAT_ID
	    + "GROUP BY e.EQUIPE_ID) ";

	String DEFAITE = "(SELECT e.EQUIPE_ID, COUNT(PERDANT_ID) AS nb_defaites "
	    + "FROM EQUIPE e "
	    + "LEFT JOIN ( "
	    + "select r.RENCONTRE_ID, e.EQUIPE_ID AS PERDANT_ID "
	    + "from RENCONTRE r, EQUIPE e "
	    + "where e.EQUIPE_ID = r.EQUIPE_A_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B "
	    + "UNION "
	    + "select r.RENCONTRE_ID, e.EQUIPE_ID AS PERDANT_ID "
	    + "from RENCONTRE r, EQUIPE e "
	    + "where e.EQUIPE_ID = r.EQUIPE_B_ID "
	    + "and r.NUMERO_SAISON = ? "
	    + "and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B "
	    + "ORDER BY RENCONTRE_ID ) AS DEFAITE "
	    + "ON e.EQUIPE_ID = DEFAITE.PERDANT_ID "
	    + "WHERE e.CATEGORIE_ID = " + CAT_ID
	    + "GROUP BY e.EQUIPE_ID) ";

	String SQL = "SELECT E.CLUB_NOM, PTS, V.nb_victoires AS V, N.nb_nuls AS N, D.nb_defaites AS D, POUR, CONTRE "
	    + "FROM "
	    + POUR_CONTRE + "AS A, " // 1,2,3,4
	    + PTS + "AS B, " //5,6,7,8,9,10,11,12,13,14,15,16
	    + VICTOIRE + "AS V, " //17,18,19
	    + NUL + "AS N, " //20,21,22
	    + DEFAITE + "AS D, " //23,24,25
	    + "GetEquipe E "
	    + "WHERE E.EQUIPE_ID = A.EQUIPE "
	    + "AND E.EQUIPE_ID = B.EQUIPE_ID "
	    + "AND E.EQUIPE_ID = V.EQUIPE_ID "
	    + "AND E.EQUIPE_ID = D.EQUIPE_ID "
	    + "AND E.EQUIPE_ID = N.EQUIPE_ID "
	    + "ORDER BY PTS DESC; ";
	
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, cat);
	stmt.setInt(2, numSaison);
	stmt.setString(3, cat);
	stmt.setInt(4, numSaison);

	stmt.setString(5, cat);
	stmt.setInt(6, numSaison);
	stmt.setString(7, cat);
	stmt.setInt(8, numSaison);
	stmt.setString(9, cat);
	stmt.setInt(10, numSaison);
	stmt.setString(11, cat);
	stmt.setInt(12, numSaison);
	stmt.setString(13, cat);
	stmt.setInt(14, numSaison);
	stmt.setString(15, cat);
	stmt.setInt(16, numSaison);

	stmt.setInt(17, numSaison);
	stmt.setInt(18, numSaison);
	stmt.setString(19, cat);
	stmt.setInt(20, numSaison);
	stmt.setInt(21, numSaison);
	stmt.setString(22, cat);
	stmt.setInt(23, numSaison);
	stmt.setInt(24, numSaison);
	stmt.setString(25, cat);

	ResultSet rs = stmt.executeQuery();
	return rs;
    }
}
