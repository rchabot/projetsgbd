package federationbasket.src;

import java.sql.*;

public class Trigger {

    public static void launchCreation(){
	try {
	    System.out.println("Creating triggers . . .");

	    afterInsertEquipe();

	    System.out.println("Triggers successfully created ");
	} catch (SQLException e){
	    e.printStackTrace();
	}
    }

    public static void launchDropping(){
	try {
	    System.out.println("Creating triggers . . .");

	    Connection conn = ConnectionDB.getInstance();
	    Statement stmt = conn.createStatement();
	    String SQL = "drop trigger after_insert_equipe ";
	    stmt.executeUpdate(SQL);
	    stmt.close();

	    System.out.println("Triggers successfully created ");
	} catch (SQLException e){
	    e.printStackTrace();
	}
    }

    public static void afterInsertEquipe() throws SQLException{
	String SQL = "CREATE TRIGGER after_insert_equipe "
	    + "AFTER UPDATE ON EQUIPE "
	    + "FOR EACH ROW "
	    + "INSERT INTO GetEquipe(EQUIPE_ID,CLUB_NOM,CATEGORIE_NOM,EQUIPE_NUMERO) "
	    + "select e.EQUIPE_ID, c.CLUB_NOM, cat.CATEGORIE_NOM, e.EQUIPE_NUMERO "
	    + "from EQUIPE e, CLUB c, CATEGORIE cat "
	    + "where e.CLUB_ID = c.CLUB_ID and e.CATEGORIE_ID = cat.CATEGORIE_ID  and e.EQUIPE_ID=NEW.EQUIPE_ID;";
	

	Connection conn = ConnectionDB.getInstance();
	Statement stmt = conn.createStatement();
	stmt.executeUpdate(SQL);
	stmt.close();
    }

}
