package federationbasket.src;

import java.sql.*;

public class Donnees {
    public static void launchCreation() throws SQLException{
	Connection conn = null;
	Statement stmt = null;

	conn = ConnectionDB.getInstance();
	    
	stmt = conn.createStatement();
	    
	System.out.println("Inserting data ...");

	insertClubs(stmt);
	insertCategories(stmt);
	insertPersonnes(stmt);
	insertPersonnesMorale(stmt);
	insertEquipes(stmt);
	insertJoueurs(stmt);
	insertEntraine(stmt);
	insertRencontre(stmt);
	insertStats(stmt);

	System.out.println("Data inserted successfully");
	    
    }


    public static void delete(Statement stmt) throws SQLException{
	String DELETE_CATEGORIE = "delete from CATEGORIE;";
	String DELETE_CLUB = "delete from CLUB;";
	String DELETE_EQUIPE = "delete from EQUIPE;";
	String DELETE_ENTRAINE = "delete from ENTRAINE;";
	String DELETE_JOUEUR = "delete from JOUEUR;";
	String DELETE_PERSONNE = "delete frome PERSONNE;";
	String DELETE_PERSONNE_MORALE = "delete from PERSONNE_MORALE;";
	String DELETE_RENCONTRE = "delete from RENCONTRE;";
	String DELETE_STATS = "delete from STATS;";
	String COMMIT = "commit;";

	stmt.executeUpdate(DELETE_CATEGORIE);
	stmt.executeUpdate(DELETE_CLUB);
	stmt.executeUpdate(DELETE_EQUIPE);
	stmt.executeUpdate(DELETE_ENTRAINE);
	stmt.executeUpdate(DELETE_JOUEUR);
	stmt.executeUpdate(DELETE_PERSONNE);
	stmt.executeUpdate(DELETE_PERSONNE_MORALE);
	stmt.executeUpdate(DELETE_RENCONTRE);
	stmt.executeUpdate(DELETE_STATS);
	stmt.executeUpdate(COMMIT);
	
    }

    public static void insertClubs(Statement stmt) throws SQLException{
	String[] col = {"CLUB_NOM", "CLUB_ADRESSE", "CLUB_TELEPHONE"};
	String TABLE = "CLUB";
	MAJ.insert(TABLE, "('CSP', 'BEAUBLANC', '0678985867')", col, stmt);
	MAJ.insert(TABLE, "('SA', 'AT&T CENTER', '0001112223')", col, stmt);
	MAJ.insert(TABLE, "('LAKERS', 'STAPLES CENTER', '1112223334')", col, stmt);
	MAJ.insert(TABLE, "('ALL STARS', 'BERCY', '2223334445')", col, stmt);
    }


    public static void insertCategories(Statement stmt) throws SQLException{
	String[] col = {"CATEGORIE_NOM"};
	String TABLE = "CATEGORIE";
	MAJ.insert(TABLE, "('BABY-BASKETTEUR')", col, stmt);
	MAJ.insert(TABLE, "('MINI-POUSSIN')", col, stmt);
	MAJ.insert(TABLE, "('POUSSIN')", col, stmt);
	MAJ.insert(TABLE, "('BENJAMIN')", col, stmt);
	MAJ.insert(TABLE, "('MINIME')", col, stmt);
	MAJ.insert(TABLE, "('CADET')", col, stmt);
	MAJ.insert(TABLE, "('JUNIOR')", col, stmt);
	MAJ.insert(TABLE, "('SENIOR')", col, stmt);
    }

    public static void insertPersonnes(Statement stmt) throws SQLException{
	String[] col = {"PERSONNE_NOM", "PERSONNE_PRENOM", "PERSONNE_DATE_DE_NAISSANCE", "PERSONNE_ADRESSE", "PERSONNE_DATE_ENTREE", "CLUB_ID"};
	String TABLE = "PERSONNE";
	
	// CLUB CSP
	MAJ.insert(TABLE, "('FORTE', 'FRED', NULL, NULL, '1990-10-10', 1)", col, stmt);
	MAJ.insert(TABLE, "('DUPRAZ', 'JEAN-MARC', NULL, NULL, NULL, 1)", col, stmt);
	MAJ.insert(TABLE, "('ADJOINT', 'ENTRAINEUR', '1960-10-01', 'Rue de la soif', '1985-01-01', 1)", col, stmt);
	MAJ.insert(TABLE, "('MOERMAN', 'ADRIEN', NULL, NULL, NULL, 1)", col, stmt);
	MAJ.insert(TABLE, "('GOMIS', 'JOSEPH', NULL, NULL, NULL, 1)", col, stmt); //5
	MAJ.insert(TABLE, "('WESTERMAN', 'LEO', NULL, NULL, NULL, 1)", col, stmt);
	MAJ.insert(TABLE, "('REMPLACANT', 'CSP', '1982-10-08', '31 rue du banc', '2013-01-01', 1)", col, stmt); 
	MAJ.insert(TABLE, "('ENTRAINEUR', 'SANS EMPLOI', NULL, NULL, NULL, 1)", col, stmt);
	// CLUB SA
	MAJ.insert(TABLE, "('POPOVICH', 'GREGG', NULL, NULL, NULL, 2)", col, stmt); 
	MAJ.insert(TABLE, "('PARKER', 'TONY', NULL, NULL, NULL, 2)", col, stmt); //10
	MAJ.insert(TABLE, "('DIAW', 'BORIS', NULL, NULL, NULL, 2)", col, stmt);
	MAJ.insert(TABLE, "('GINOBILI', 'MARIO', NULL, NULL, NULL, 2)", col, stmt); 
	MAJ.insert(TABLE, "('REMPLACANT', 'SA', NULL, NULL, NULL, 2)", col, stmt); 
	// CLUB LAKERS
	MAJ.insert(TABLE, "('ENTRAINEUR','LAKERS', NULL, NULL, NULL, 3)", col, stmt);
	MAJ.insert(TABLE, "('BRYANT', 'KOBE', NULL, NULL, NULL, 3)", col, stmt); //15
	MAJ.insert(TABLE, "('NASH', 'STEVE', NULL, NULL, NULL, 3)", col, stmt); 
	MAJ.insert(TABLE, "('YOUNG', 'NICK', NULL, NULL, NULL, 3)", col, stmt);
	// CLUB ALL STARS
	MAJ.insert(TABLE, "('ENTRAINEUR','ALL STARS', NULL, NULL, NULL, 4)", col, stmt);
	MAJ.insert(TABLE, "('JORDAN','MICHAEL', NULL, NULL, NULL, 4)", col, stmt);
	MAJ.insert(TABLE, "('JOHNSON', 'MAGIC', NULL, NULL, NULL, 4)", col, stmt); //20
	MAJ.insert(TABLE, "('LEBRON', 'JAMES', NULL, NULL, NULL, 4)", col, stmt);
	// CLUB LIMOGES 2e CATEGORIE 1ere equipe
	MAJ.insert(TABLE, "('JUNIOR1', 'CSPUN', NULL, NULL, NULL, 1)", col, stmt);
	MAJ.insert(TABLE, "('JUNIOR1', 'CSPDEUX', NULL, NULL, NULL, 1)", col, stmt);
	MAJ.insert(TABLE, "('JUNIOR1', 'HOMONYME', NULL, NULL, NULL, 1)", col, stmt);
	// CLUB LIMOGES 2e CATEGORIE 1ere equipe
	MAJ.insert(TABLE, "('JUNIOR2', 'CSPUN', NULL, NULL, NULL, 1)", col, stmt); //25
	MAJ.insert(TABLE, "('JUNIOR2', 'CSPDEUX', NULL, NULL, NULL, 1)", col, stmt);
	MAJ.insert(TABLE, "('JUNIOR1', 'HOMONYME', NULL, NULL, NULL, 1)", col, stmt);
	// CLUB SA 2e CATEGORIE
	MAJ.insert(TABLE, "('JUNIOR', 'SAUN', NULL, NULL, NULL, 2)", col, stmt);
	MAJ.insert(TABLE, "('JUNIOR', 'SADEUX', NULL, NULL, NULL, 2)", col, stmt);
	MAJ.insert(TABLE, "('JUNIOR', 'SATROIS', NULL, NULL, NULL, 2)", col, stmt); //30
	
    }
    
    public static void insertPersonnesMorale(Statement stmt) throws SQLException{
	String[] col = {"PERSONNE_ID", "FONCTION"};
	String TABLE = "PERSONNE_MORALE";
	
	MAJ.insert(TABLE, "(1, 'PRESIDENT')", col, stmt);
    }

    public static void insertEquipes(Statement stmt) throws SQLException{
	String[] col = {"EQUIPE_NUMERO", "CLUB_ID", "CATEGORIE_ID"};
	String TABLE = "EQUIPE";
	
	MAJ.insert(TABLE, "(1, 1, 8)", col, stmt);
	MAJ.insert(TABLE, "(1, 2, 8)", col, stmt);
	MAJ.insert(TABLE, "(1, 3, 8)", col, stmt);
	MAJ.insert(TABLE, "(1, 4, 8)", col, stmt);
	MAJ.insert(TABLE, "(1, 1, 7)", col, stmt);
	MAJ.insert(TABLE, "(2, 1, 7)", col, stmt);
	MAJ.insert(TABLE, "(1, 2, 7)", col, stmt);
	
    }

    public static void insertJoueurs(Statement stmt) throws SQLException{
	String[] col = {"JOUEUR_ID", "JOUEUR_NUM_LICENCE", "EQUIPE_ID"};
	String TABLE = "JOUEUR";
	
	MAJ.insert(TABLE, "(4, 'AAAA0001', 1)", col, stmt);
	MAJ.insert(TABLE, "(5, 'AAAA0002', 1)", col, stmt);
	MAJ.insert(TABLE, "(6, 'AAAA0003', 1)", col, stmt);
	MAJ.insert(TABLE, "(7, 'AAAA0004', 1)", col, stmt);
	MAJ.insert(TABLE, "(10,'BBBB0001', 2)", col, stmt);
	MAJ.insert(TABLE, "(11,'BBBB0002', 2)", col, stmt);
	MAJ.insert(TABLE, "(12,'BBBB0003', 2)", col, stmt);
	MAJ.insert(TABLE, "(13,'BBBB0004', 2)", col, stmt);
	MAJ.insert(TABLE, "(15,'CCCC0001', 3)", col, stmt);
	MAJ.insert(TABLE, "(16,'CCCC0002', 3)", col, stmt);
	MAJ.insert(TABLE, "(17,'CCCC0003', 3)", col, stmt);
	MAJ.insert(TABLE, "(19,'DDDD0001', 4)", col, stmt);
	MAJ.insert(TABLE, "(20,'DDDD0002', 4)", col, stmt);
	MAJ.insert(TABLE, "(21,'DDDD0003', 4)", col, stmt);
	MAJ.insert(TABLE, "(22,'AAAA0005', 5)", col, stmt);
	MAJ.insert(TABLE, "(23,'AAAA0006', 5)", col, stmt);
	MAJ.insert(TABLE, "(24,'AAAA0007', 5)", col, stmt);
	MAJ.insert(TABLE, "(25,'AAAA0008', 6)", col, stmt);
	MAJ.insert(TABLE, "(26,'AAAA0009', 6)", col, stmt);
	MAJ.insert(TABLE, "(27,'AAAA0010', 6)", col, stmt);
	MAJ.insert(TABLE, "(28,'BBBB0005', 7)", col, stmt);
	MAJ.insert(TABLE, "(29,'BBBB0006', 7)", col, stmt);
	MAJ.insert(TABLE, "(30,'BBBB0007', 7)", col, stmt);
    }
    
    public static void insertEntraine(Statement stmt) throws SQLException{
	String[] col = {"ENTRAINEUR_ID", "EQUIPE_ID"};
	String TABLE = "ENTRAINE";
	
	MAJ.insert(TABLE, "(2,1)", col, stmt);
	MAJ.insert(TABLE, "(3,1)", col, stmt);
	MAJ.insert(TABLE, "(3,5)", col, stmt);
	MAJ.insert(TABLE, "(3,6)", col, stmt);
	MAJ.insert(TABLE, "(9,2)", col, stmt);
	MAJ.insert(TABLE, "(14,3)", col, stmt);
	MAJ.insert(TABLE, "(15,3)", col, stmt); // Entraineur-Joueur
	MAJ.insert(TABLE, "(18,4)", col, stmt);
	
    }

    public static void insertRencontre(Statement stmt) throws SQLException{
	String[] col = {"EQUIPE_A_ID", "EQUIPE_B_ID", "RENCONTRE_DATE", "NUMERO_JOURNEE", "NUMERO_SAISON", "SCORE_EQUIPE_A", "SCORE_EQUIPE_B"};
	String TABLE = "RENCONTRE";

	MAJ.insert(TABLE, "(1, 2, '2015-01-10', 1, 1, 64, 63)", col, stmt);
	MAJ.insert(TABLE, "(3, 4, '2015-01-10', 1, 1, 75, 97)", col, stmt);
	MAJ.insert(TABLE, "(1, 4, '2015-01-12', 2, 1, 63, 72)", col, stmt);
	MAJ.insert(TABLE, "(3, 1, '2015-01-13', 3, 1, 70, 70)", col, stmt);
	MAJ.insert(TABLE, "(2, 3, '2015-01-14', 2, 1, 80, 59)", col, stmt);
	MAJ.insert(TABLE, "(4, 2, '2015-01-14', 3, 1, 100, 85)", col, stmt);
	MAJ.insert(TABLE, "(5, 7, '2015-01-14', 1, 1, 55, 59)", col, stmt); // Match junior
	MAJ.insert(TABLE, "(1, 2, '2016-01-08', 1, 2, 77, 88)", col, stmt);
    }

    public static void insertStats(Statement stmt) throws SQLException{
	String[] col = {"JOUEUR_ID", "RENCONTRE_ID", "STATS_POINTS", "STATS_FAUTES"};
	String TABLE = "STATS";
	
	// Rencontre 1
	MAJ.insert(TABLE, "(4 , 1, 20, 3)", col, stmt);
	MAJ.insert(TABLE, "(5 , 1, 33, 2)", col, stmt);
	MAJ.insert(TABLE, "(6 , 1, 7, 5)", col, stmt);
	//Remplaçnat qui marque
	MAJ.insert(TABLE, "(7 , 1, 4, 1)", col, stmt); 
	MAJ.insert(TABLE, "(10, 1, 25, 2)", col, stmt);
	MAJ.insert(TABLE, "(11, 1, 20, 3)", col, stmt);
	MAJ.insert(TABLE, "(12, 1, 18, 4)", col, stmt); 
	// Remplaçant absent de la feuille de match
	// Rencontre 2
	MAJ.insert(TABLE, "(15, 2, 38, 1)", col, stmt);
	MAJ.insert(TABLE, "(16, 2, 10, 4)", col, stmt);
	MAJ.insert(TABLE, "(17, 2, 27, 4)", col, stmt);
	MAJ.insert(TABLE, "(19, 2, 33, 2)", col, stmt);
	MAJ.insert(TABLE, "(20, 2, 40, 3)", col, stmt);
	MAJ.insert(TABLE, "(21, 2, 24, 4)", col, stmt);
	// Rencontre 3
	MAJ.insert(TABLE, "(4 , 3, 20, 3)", col, stmt);
	MAJ.insert(TABLE, "(5 , 3, 23, 2)", col, stmt);
	MAJ.insert(TABLE, "(6 , 3, 17, 2)", col, stmt);
	//Remplaçant sur la feuille de match
	MAJ.insert(TABLE, "(7 , 3, 0, 0)", col, stmt); 
	MAJ.insert(TABLE, "(19, 3, 39, 2)", col, stmt);
	MAJ.insert(TABLE, "(20, 3, 23, 3)", col, stmt);
	MAJ.insert(TABLE, "(21, 3, 20, 3)", col, stmt);
	// Rencontre 4
	MAJ.insert(TABLE, "(4 , 4, 20, 1)", col, stmt);
	MAJ.insert(TABLE, "(5 , 4, 28, 4)", col, stmt);
	MAJ.insert(TABLE, "(6 , 4, 22, 2)", col, stmt);
	MAJ.insert(TABLE, "(15, 4, 29, 3)", col, stmt);
	MAJ.insert(TABLE, "(16, 4, 11, 1)", col, stmt);
	MAJ.insert(TABLE, "(17, 4, 30, 2)", col, stmt);
	// Rencontre 5
	MAJ.insert(TABLE, "(10, 5, 37, 2)", col, stmt);
	MAJ.insert(TABLE, "(11, 5, 23, 4)", col, stmt);
	MAJ.insert(TABLE, "(12, 5, 20, 3)", col, stmt); 
	MAJ.insert(TABLE, "(15, 5, 36, 3)", col, stmt);
	MAJ.insert(TABLE, "(16, 5, 8, 0)", col, stmt);
	MAJ.insert(TABLE, "(17, 5, 15, 4)", col, stmt);
	// Rencontre 6
	MAJ.insert(TABLE, "(19, 6, 39, 1)", col, stmt);
	MAJ.insert(TABLE, "(20, 6, 21, 4)", col, stmt);
	MAJ.insert(TABLE, "(21, 6, 40, 3)", col, stmt);
	MAJ.insert(TABLE, "(10, 6, 30, 2)", col, stmt);
	MAJ.insert(TABLE, "(11, 6, 35, 4)", col, stmt);
	MAJ.insert(TABLE, "(12, 6, 20, 3)", col, stmt); 
	// Rencontre junior
	MAJ.insert(TABLE, "(25, 7, 15, 3)", col, stmt);
	MAJ.insert(TABLE, "(26, 7, 15, 2)", col, stmt);
	MAJ.insert(TABLE, "(27, 7, 25, 1)", col, stmt);
	MAJ.insert(TABLE, "(28, 7, 30, 5)", col, stmt);
	MAJ.insert(TABLE, "(29, 7, 19, 4)", col, stmt);
	MAJ.insert(TABLE, "(30, 7, 10, 3)", col, stmt);
	// Rencontre saison 2
	MAJ.insert(TABLE, "(4 , 8, 29, 3)", col, stmt);
	MAJ.insert(TABLE, "(5 , 8, 33, 2)", col, stmt);
	MAJ.insert(TABLE, "(6 , 8, 15, 5)", col, stmt);
	MAJ.insert(TABLE, "(10, 8, 29, 2)", col, stmt);
	MAJ.insert(TABLE, "(11, 8, 20, 3)", col, stmt);
	MAJ.insert(TABLE, "(12, 8, 39, 4)", col, stmt); 
    }
}
