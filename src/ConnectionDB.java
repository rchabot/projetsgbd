package federationbasket.src;

import java.sql.*;

class ConnectionDB {
    static String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
    static String DB_NAME = "FederationBasket";
    static String DB_URL = "jdbc:mysql://localhost/" + DB_NAME;
    static String USER = "root";
    static String PASS = "rchabot";
    static Connection connect;
    
    public static Connection getInstance(){
	if (connect == null){
	    try{
		connect = DriverManager.getConnection(DB_URL, 
						      USER, 
						      PASS);
	    }catch(SQLException e){
		e.printStackTrace();
	    }
	}
	return connect;
    }
}
