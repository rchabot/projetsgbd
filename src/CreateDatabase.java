package federationbasket.src;

import java.sql.*;

public class CreateDatabase {

    static String DB_URL = "jdbc:mysql://localhost/";

    public static void createDatabase() throws SQLException {
	Statement stmt = null;
	Connection conn = null;
	conn = DriverManager.getConnection(DB_URL, 
					   ConnectionDB.USER, 
					   ConnectionDB.PASS);
    	    
	stmt = conn.createStatement();
	
	System.out.println("Creating "+ConnectionDB.DB_NAME+" database ...");

	stmt.executeUpdate("create database if not exists "+ConnectionDB.DB_NAME);
	Table.launchCreation();
	Donnees.launchCreation();
	Vues.launchCreation();
	Trigger.launchCreation();
	
	System.out.println(ConnectionDB.DB_NAME+" created");
	conn.close();
    }

    public static void main(String... args) throws Exception{
	try{
	    createDatabase();
	    
	}catch(SQLException se){
	    se.printStackTrace();
	}catch(Exception e){
	    e.printStackTrace();
	}
	
    }
    
}
