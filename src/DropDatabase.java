package federationbasket.src;

import java.sql.*;

public class DropDatabase {

    public static void dropDatabase() throws SQLException {
	Statement stmt = null;
	Connection conn = null;
	conn = ConnectionDB.getInstance();
	    
	stmt = conn.createStatement();
	
	System.out.println("Dropping "+ConnectionDB.DB_NAME+" database ...");

	Table.launchDropping();
	Vues.launchDropping();
	//Trigger.launchDropping();
	stmt.executeUpdate("drop database if exists "+ConnectionDB.DB_NAME);

	System.out.println(ConnectionDB.DB_NAME+" deleted");
    }

    public static void main(String... args) throws Exception{
	try{
	    dropDatabase();

	}catch(SQLException se){
	    se.printStackTrace();
	}catch(Exception e){
	    e.printStackTrace();
	}
	
    }
    
}
