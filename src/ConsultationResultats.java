package federationbasket.src;

import java.sql.*;

public class ConsultationResultats{
    public static ResultSet listeScoreToDate(String date) throws SQLException {
	String SQL = "SELECT eqA.CLUB_NOM AS EQUIPE_A, eqA.CATEGORIE_NOM AS CATEGORIE, eqA.EQUIPE_NUMERO AS NUM, SCORE_EQUIPE_A, SCORE_EQUIPE_B, eqB.CLUB_NOM AS EQUIPE_B, eqB.CATEGORIE_NOM AS CAT, eqB.EQUIPE_NUMERO AS NUM, r.RENCONTRE_DATE AS DATE, r.NUMERO_SAISON AS SAISON, r.NUMERO_JOURNEE AS JOURNEE "
	    + "FROM RENCONTRE r, GetEquipe eqA, GetEquipe eqB "
	    + "WHERE r.EQUIPE_A_ID = eqA.EQUIPE_ID "
	    + "AND r.EQUIPE_B_ID = eqB.EQUIPE_ID "
	    + "AND r.RENCONTRE_DATE <= ? "
	    + "ORDER BY DATE;";
 
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, date);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }


    public static ResultSet listeScoreToDate(int numSaison) throws SQLException {
	String SQL = "SELECT eqA.CLUB_NOM AS EQUIPE_A, eqA.CATEGORIE_NOM AS CATEGORIE, eqA.EQUIPE_NUMERO AS NUM, SCORE_EQUIPE_A, SCORE_EQUIPE_B, eqB.CLUB_NOM AS EQUIPE_B, eqB.CATEGORIE_NOM AS CAT, eqB.EQUIPE_NUMERO AS NUM, r.RENCONTRE_DATE AS DATE, r.NUMERO_SAISON AS SAISON, r.NUMERO_JOURNEE AS JOURNEE "
	    + "FROM RENCONTRE r, GetEquipe eqA, GetEquipe eqB "
	    + "WHERE r.EQUIPE_A_ID = eqA.EQUIPE_ID "
	    + "AND r.EQUIPE_B_ID = eqB.EQUIPE_ID "
	    + "AND r.NUMERO_SAISON <= ? "
	    + "ORDER BY DATE;";

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1, numSaison);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }


    public static ResultSet listeScoreAtDate(int numSaison, int numJournee) throws SQLException {
	String SQL = "SELECT eqA.CLUB_NOM AS EQUIPE_A, eqA.CATEGORIE_NOM AS CATEGORIE, eqA.EQUIPE_NUMERO AS NUM, SCORE_EQUIPE_A, SCORE_EQUIPE_B, eqB.CLUB_NOM AS EQUIPE_B, eqB.CATEGORIE_NOM AS CAT, eqB.EQUIPE_NUMERO AS NUM, r.RENCONTRE_DATE AS DATE, r.NUMERO_SAISON AS SAISON, r.NUMERO_JOURNEE AS JOURNEE "
	    + "FROM RENCONTRE r, GetEquipe eqA, GetEquipe eqB "
	    + "WHERE r.EQUIPE_A_ID = eqA.EQUIPE_ID "
	    + "AND r.EQUIPE_B_ID = eqB.EQUIPE_ID "
	    + "AND r.NUMERO_SAISON = ? "
	    + "AND r.NUMERO_JOURNEE = ? "
	    + "ORDER BY DATE;";

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1, numSaison);
	stmt.setInt(2, numJournee);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    
    public static ResultSet listeScoreAtDate(String date) throws SQLException {
	String SQL = "SELECT eqA.CLUB_NOM AS EQUIPE_A, eqA.CATEGORIE_NOM AS CATEGORIE, eqA.EQUIPE_NUMERO AS NUM, SCORE_EQUIPE_A, SCORE_EQUIPE_B, eqB.CLUB_NOM AS EQUIPE_B, eqB.CATEGORIE_NOM AS CAT, eqB.EQUIPE_NUMERO AS NUM, r.RENCONTRE_DATE AS DATE, r.NUMERO_SAISON AS SAISON, r.NUMERO_JOURNEE AS JOURNEE "
	    + "FROM RENCONTRE r, GetEquipe eqA, GetEquipe eqB "
	    + "WHERE r.EQUIPE_A_ID = eqA.EQUIPE_ID "
	    + "AND r.EQUIPE_B_ID = eqB.EQUIPE_ID "
	    + "AND r.RENCONTRE_DATE = ? "
	    + "ORDER BY DATE;";
 
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, date);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet listeScoreAtDate(int numSaison, String cat) throws SQLException {
	String SQL = "SELECT eqA.CLUB_NOM AS EQUIPE_A, eqA.EQUIPE_NUMERO AS NUM, SCORE_EQUIPE_A, SCORE_EQUIPE_B, eqB.CLUB_NOM AS EQUIPE_B, eqB.EQUIPE_NUMERO AS NUM, r.RENCONTRE_DATE AS DATE, r.NUMERO_SAISON AS SAISON, r.NUMERO_JOURNEE AS JOURNEE "
	    + "FROM RENCONTRE r, GetEquipe eqA, GetEquipe eqB "
	    + "WHERE r.EQUIPE_A_ID = eqA.EQUIPE_ID "
	    + "AND r.EQUIPE_B_ID = eqB.EQUIPE_ID "
	    + "AND eqA.CATEGORIE_NOM = ? "
	    + "AND eqB.CATEGORIE_NOM = ? "
	    + "AND r.NUMERO_SAISON = ? "
	    + "ORDER BY DATE;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, cat);
	stmt.setString(2, cat);
	stmt.setInt(3, numSaison);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }
    
    public static int nbVictoires(String nomClub) {
	String CLUB_ID = "(select CLUB_ID "
	    + "from CLUB c "
	    + "where c.CLUB_NOM = UPPER(?)) ";

	String GAGNE_QD_A = "select r.RENCONTRE_ID "
	    + "from CLUB c, RENCONTRE r, EQUIPE e "
	    + "where c.CLUB_ID = " + CLUB_ID 
	    + "and e.CLUB_ID = c.CLUB_ID "
	    + "and r.EQUIPE_A_ID = e.EQUIPE_ID "
	    + "and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B ";

	String GAGNE_QD_B = "select r.RENCONTRE_ID "
	    + "from CLUB c, RENCONTRE r, EQUIPE e "
	    + "where c.CLUB_ID = " + CLUB_ID 
	    + "and e.CLUB_ID = c.CLUB_ID "
	    + "and r.EQUIPE_B_ID = e.EQUIPE_ID "
	    + "and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B ";

	String SQL = "select count(*) NB_VICTOIRES from "
	    + "(" + GAGNE_QD_A + " union " + GAGNE_QD_B +") " 
	    + "as result ;";
	int n = 0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setString(1, nomClub);
	    stmt.setString(2, nomClub);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    n = rs.getInt(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return n;
    }

    
    public static int nbDefaites(String nomClub)  {
	String CLUB_ID = "(select CLUB_ID "
	    + "from CLUB c "
	    + "where c.CLUB_NOM = UPPER(?)) ";

	String PERDU_QD_A = "select r.RENCONTRE_ID "
	    + "from CLUB c, RENCONTRE r, EQUIPE e "
	    + "where c.CLUB_ID = " + CLUB_ID 
	    + "and e.CLUB_ID = c.CLUB_ID "
	    + "and r.EQUIPE_A_ID = e.EQUIPE_ID "
	    + "and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B ";

	String PERDU_QD_B = "select r.RENCONTRE_ID "
	    + "from CLUB c, RENCONTRE r, EQUIPE e "
	    + "where c.CLUB_ID = " + CLUB_ID 
	    + "and e.CLUB_ID = c.CLUB_ID "
	    + "and r.EQUIPE_B_ID = e.EQUIPE_ID "
	    + "and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B ";
	
	String SQL = "select count(*) nb_de_victoires from "
	    + "(" + PERDU_QD_A + " union " + PERDU_QD_B +") " 
	    + "as result ;";

	int n = 0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setString(1, nomClub);
	    stmt.setString(2, nomClub);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    n = rs.getInt(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return n;
    }


    public static int nbNuls(String nomClub)  {
	String CLUB_ID = "(select CLUB_ID "
	    + "from CLUB c "
	    + "where c.CLUB_NOM = UPPER(?)) ";

	String NUL_A = "select r.RENCONTRE_ID "
	    + "from CLUB c, RENCONTRE r, EQUIPE e "
	    + "where c.CLUB_ID = " + CLUB_ID 
	    + "and e.CLUB_ID = c.CLUB_ID "
	    + "and r.EQUIPE_A_ID = e.EQUIPE_ID "
	    + "and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B ";

	String NUL_B = "select r.RENCONTRE_ID "
	    + "from CLUB c, RENCONTRE r, EQUIPE e "
	    + "where c.CLUB_ID = " + CLUB_ID 
	    + "and e.CLUB_ID = c.CLUB_ID "
	    + "and r.EQUIPE_B_ID = e.EQUIPE_ID "
	    + "and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B ";
	
	String SQL = "select count(*) nb_de_victoires from "
	    + "(" + NUL_A + " union " + NUL_B +") " 
	    + "as result ;";
		int n = 0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setString(1, nomClub);
	    stmt.setString(2, nomClub);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    n = rs.getInt(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return n;
    }

    public static int nbVictoires(String nomClub, String cat, int numEq) {
	String EQUIPE_ID = "(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";

	String GAGNE_QD_A = "select r.RENCONTRE_ID "
	    + "from RENCONTRE r "
	    + "where r.EQUIPE_A_ID = " + EQUIPE_ID 
	    + "and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B ";

	String GAGNE_QD_B = "select r.RENCONTRE_ID "
	    + "from RENCONTRE r "
	    + "where r.EQUIPE_B_ID = " + EQUIPE_ID 
	    + "and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B ";

	String SQL = "select count(*) nb_de_victoires from "
	    + "(" + GAGNE_QD_A + " union " + GAGNE_QD_B +") " 
	    + "as result ;";
	int n = 0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setString(1, nomClub);
	    stmt.setString(2, cat);
	    stmt.setInt(3, numEq);
	    stmt.setString(4, nomClub);
	    stmt.setString(5, cat);
	    stmt.setInt(6, numEq);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    n = rs.getInt(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return n;
    }

    public static int nbDefaites(String nomClub, String cat, int numEq) {
	String EQUIPE_ID = "(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";

	String PERDU_QD_A = "select r.RENCONTRE_ID "
	    + "from RENCONTRE r "
	    + "where r.EQUIPE_A_ID = " + EQUIPE_ID 
	    + "and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B ";

	String PERDU_QD_B = "select r.RENCONTRE_ID "
	    + "from RENCONTRE r "
	    + "where r.EQUIPE_B_ID = " + EQUIPE_ID 
	    + "and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B ";

	String SQL = "select count(*) nb_de_victoires from "
	    + "(" + PERDU_QD_A + " union " + PERDU_QD_B +") " 
	    + "as result ;";

	int n = 0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setString(1, nomClub);
	    stmt.setString(2, cat);
	    stmt.setInt(3, numEq);
	    stmt.setString(4, nomClub);
	    stmt.setString(5, cat);
	    stmt.setInt(6, numEq);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    n = rs.getInt(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return n;
    }

    public static int nbNuls(String nomClub, String cat, int numEq) {
	String EQUIPE_ID = "(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";

	String NUL_A = "select r.RENCONTRE_ID "
	    + "from RENCONTRE r "
	    + "where r.EQUIPE_A_ID = " + EQUIPE_ID 
	    + "and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B ";

	String NUL_B = "select r.RENCONTRE_ID "
	    + "from RENCONTRE r "
	    + "where r.EQUIPE_B_ID = " + EQUIPE_ID 
	    + "and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B ";

	String SQL = "select count(*) nb_de_victoires from "
	    + "(" + NUL_A + " union " + NUL_B +") " 
	    + "as result ;";
	int n=0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setString(1, nomClub);
	    stmt.setString(2, cat);
	    stmt.setInt(3, numEq);
	    stmt.setString(4, nomClub);
	    stmt.setString(5, cat);
	    stmt.setInt(6, numEq);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    n = rs.getInt(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return n;
    }

    public static ResultSet feuilleDeMatch(String clubA, int numEqA, String catA, String clubB, int numEqB, String catB, int numSaison, int numJournee) throws SQLException {
        String EQUIPE_A_ID ="(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";

	String EQUIPE_B_ID ="(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";
	
	String SQL = "select p.PERSONNE_NOM, p.PERSONNE_PRENOM, j.EQUIPE_ID, s.STATS_POINTS, s.STATS_FAUTES "
	    + "from RENCONTRE r, PERSONNE p, JOUEUR j, STATS s "
	    + "where ( (r.EQUIPE_A_ID = " + EQUIPE_A_ID + "and r.EQUIPE_B_ID = " + EQUIPE_B_ID + ") " //1,2,3,4,5,6
	         + "or (r.EQUIPE_A_ID = " + EQUIPE_B_ID + "and r.EQUIPE_B_ID = " + EQUIPE_A_ID + ") ) " //7,8,9,10,11,12
	    + "and r.NUMERO_SAISON = ? " // 13
	    + "and r.NUMERO_JOURNEE = ? " // 14
	    + "and p.PERSONNE_ID = j.JOUEUR_ID "
	    + "and r.RENCONTRE_ID = s.RENCONTRE_ID "
	    + "and j.JOUEUR_ID = s.JOUEUR_ID "
	    + "order by j.EQUIPE_ID asc, s.STATS_POINTS desc;";

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, clubA);
	stmt.setString(2, catA);
	stmt.setInt(3, numEqA);
	stmt.setString(4, clubB);
	stmt.setString(5, catB);
	stmt.setInt(6, numEqB);
	stmt.setString(7, clubB);
	stmt.setString(8, catB);
	stmt.setInt(9, numEqB);
	stmt.setString(10, clubA);
	stmt.setString(11, catA);
	stmt.setInt(12, numEqA);
	stmt.setInt(13, numSaison);
	stmt.setInt(14, numJournee);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet feuilleDeMatch(String clubA, int numEqA, String catA, String clubB, int numEqB, String catB, String date) throws SQLException {
        String EQUIPE_A_ID ="(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";

	String EQUIPE_B_ID ="(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";
	
	String SQL = "select p.PERSONNE_NOM, p.PERSONNE_PRENOM, j.EQUIPE_ID, s.STATS_POINTS, s.STATS_FAUTES "
	    + "from RENCONTRE r, PERSONNE p, JOUEUR j, STATS s "
	    + "where ( (r.EQUIPE_A_ID = " + EQUIPE_A_ID + "and r.EQUIPE_B_ID = " + EQUIPE_B_ID + ") " //1,2,3,4,5,6
	         + "or (r.EQUIPE_A_ID = " + EQUIPE_B_ID + "and r.EQUIPE_B_ID = " + EQUIPE_A_ID + ") ) " //7,8,9,10,11,12
	    + "and r.RENCONTRE_DATE = ? " //13
	    + "and p.PERSONNE_ID = j.JOUEUR_ID "
	    + "and r.RENCONTRE_ID = s.RENCONTRE_ID "
	    + "and j.JOUEUR_ID = s.JOUEUR_ID "
	    + "order by j.EQUIPE_ID asc, s.STATS_POINTS desc;";

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, clubA);
	stmt.setString(2, catA);
	stmt.setInt(3, numEqA);
	stmt.setString(4, clubB);
	stmt.setString(5, catB);
	stmt.setInt(6, numEqB);
	stmt.setString(7, clubB);
	stmt.setString(8, catB);
	stmt.setInt(9, numEqB);
	stmt.setString(10, clubA);
	stmt.setString(11, catA);
	stmt.setInt(12, numEqA);
	stmt.setString(13, date);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

}
