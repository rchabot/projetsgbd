package federationbasket.src;

import java.sql.*;

public class ConsultationJoueur {
    
    public static ResultSet infoJoueur(String nom, String prenom) throws SQLException{
	Connection conn = ConnectionDB.getInstance();
	String SQL = "SELECT j.* "
	    + "FROM JOUEUR j, PERSONNE p "
	    + "WHERE p.PERSONNE_NOM = ? " 
	    + "AND p.PERSONNE_PRENOM = ? "
	    + "AND p.PERSONNE_ID = j.JOUEUR_ID "
	    + "ORDER BY j.JOUEUR_ID; ";
	
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nom);
	stmt.setString(2, prenom);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }
    
    public static ResultSet infoJoueur(String numLicence) throws SQLException{
	String SQL = "SELECT * "
	    + "FROM JOUEUR j "
	    + "WHERE j.JOUEUR_NUM_LICENCE = ? ;";
	
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, numLicence);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet infoJoueur(int joueurId) throws SQLException{
	String SQL = "SELECT j.JOUEUR_ID, p.PERSONNE_NOM, p.PERSONNE_PRENOM, j.JOUEUR_NUM_LICENCE, g.CLUB_NOM, g.CATEGORIE_NOM, g.EQUIPE_NUMERO, p.PERSONNE_DATE_DE_NAISSANCE, p.PERSONNE_ADRESSE, p.PERSONNE_DATE_ENTREE "
	    + "FROM JOUEUR j, PERSONNE p, GetEquipe g "
	    + "WHERE j.JOUEUR_ID = p.PERSONNE_ID "
	    + "AND j.JOUEUR_ID = ? "
	    + "AND g.EQUIPE_ID = j.EQUIPE_ID ;";
	
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1, joueurId);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet listeJoueurs(String date) throws SQLException{
	String SQL = "";
	
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }
    
    public static int cumulPointsJoueur(String numLicence, int numSaison) {
	String JOUEUR_ID = "(SELECT PERSONNE_ID "
	    + "FROM PERSONNE p, JOUEUR j "
	    + "WHERE j.JOUEUR_NUM_LICENCE = ? "
	    + "AND j.JOUEUR_ID = p.PERSONNE_ID) ";

	String SQL = "SELECT SUM(POINTS) AS cumul_points "
	    + "FROM (SELECT STATS_POINTS AS POINTS "
	    + "FROM STATS s, RENCONTRE r "
	    + "WHERE s.JOUEUR_ID = " + JOUEUR_ID 
	    + "AND s.RENCONTRE_ID = r.RENCONTRE_ID "
	    + "AND r.NUMERO_SAISON = ? ) "
	    + "AS RESULT ;";
	int total = 0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setString(1, numLicence);
	    stmt.setInt(2, numSaison);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    total = rs.getInt(1);
	    rs.close();
	} catch(SQLException e){
	    e.printStackTrace();
	}
	return total;
    }

    public static int cumulStatsJoueur(int joueurId, String TYPE) {
	String SQL = "SELECT SUM("+TYPE+") AS cumul_points "
	    + "FROM (SELECT STATS_"+TYPE+ " AS "+TYPE+" "
	    + "FROM STATS s, RENCONTRE r "
	    + "WHERE s.JOUEUR_ID = ? " 
	    + "AND s.RENCONTRE_ID = r.RENCONTRE_ID) "
	    + "AS RESULT ;";

	int total = 0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setInt(1, joueurId);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    total = rs.getInt(1);
	    rs.close();
	} catch(SQLException e){
	    e.printStackTrace();
	}
	return total;
    }

    public static ResultSet cumulPointsJoueur(String numLicence) throws SQLException {
	String JOUEUR_ID = "(SELECT PERSONNE_ID "
	    + "FROM PERSONNE p, JOUEUR j "
	    + "WHERE j.JOUEUR_NUM_LICENCE = ? "
	    + "AND j.JOUEUR_ID = p.PERSONNE_ID) ";

	String SQL = "SELECT SUM(POINTS) AS cumul_points "
	    + "FROM (SELECT STATS_POINTS AS POINTS "
	    + "FROM STATS s, RENCONTRE r "
	    + "WHERE s.JOUEUR_ID = " + JOUEUR_ID
	    + "AND s.RENCONTRE_ID = r.RENCONTRE_ID) "
	    + "AS RESULT ;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, numLicence);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet cumulFautesJoueur(String numLicence, int numSaison) throws SQLException {
	String JOUEUR_ID = "(SELECT PERSONNE_ID "
	    + "FROM PERSONNE p, JOUEUR j "
	    + "WHERE j.JOUEUR_NUM_LICENCE = ? "
	    + "AND j.JOUEUR_ID = p.PERSONNE_ID) ";

	String SQL = "SELECT SUM(FAUTES) AS cumul_points "
	    + "FROM (SELECT STATS_FAUTES AS FAUTES "
	    + "FROM STATS s, RENCONTRE r "
	    + "WHERE s.JOUEUR_ID = " + JOUEUR_ID 
	    + "AND s.RENCONTRE_ID = r.RENCONTRE_ID "
	    + "AND r.NUMERO_SAISON = ? ) "
	    + "AS RESULT ;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, numLicence);
	stmt.setInt(2, numSaison);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet cumulFautesJoueur(String numLicence) throws SQLException {
	String JOUEUR_ID = "(SELECT PERSONNE_ID "
	    + "FROM PERSONNE p, JOUEUR j "
	    + "WHERE j.JOUEUR_NUM_LICENCE = ? "
	    + "AND j.JOUEUR_ID = p.PERSONNE_ID) ";

	String SQL = "SELECT SUM(FAUTES) AS cumul_points "
	    + "FROM (SELECT STATS_FAUTES AS FAUTES "
	    + "FROM STATS s, RENCONTRE r "
	    + "WHERE s.JOUEUR_ID = " + JOUEUR_ID
	    + "AND s.RENCONTRE_ID = r.RENCONTRE_ID) "
	    + "AS RESULT ;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, numLicence);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet statsParSaison(int joueurId) throws SQLException {
	String SQL = "SELECT NUMERO_SAISON AS SAISON, COUNT(*) AS NB_MATCHS_JOUES, SUM(POINTS) AS POINTS_MARQUES, SUM(FAUTES) AS FAUTES_COMMISES "
	    + "FROM ( "
	    + "SELECT r.NUMERO_SAISON, r.RENCONTRE_ID, STATS_POINTS AS POINTS, STATS_FAUTES AS FAUTES "
	    + "FROM STATS s, RENCONTRE r "
	    + "WHERE s.JOUEUR_ID = ? "
	    + "AND s.RENCONTRE_ID = r.RENCONTRE_ID "
	    + ") AS RESULT "
	    + "GROUP BY NUMERO_SAISON ;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1, joueurId);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet statsCarriere(int joueurId) throws SQLException {
	String SQL = "SELECT COUNT(NUMERO_SAISON) AS NB_SAISON, SUM(MATCHS_JOUES) AS NB_MATCH, SUM(CUMUL_POINTS) AS PTS_MARQUES, SUM(CUMUL_FAUTES) AS FAUTES_COMMISES  "
	    + "from  "
	    + "( "
	    + "SELECT NUMERO_SAISON, COUNT(*) AS MATCHS_JOUES, SUM(POINTS) AS CUMUL_POINTS, SUM(FAUTES) AS CUMUL_FAUTES "
	    + "FROM ( "
	    + "SELECT r.NUMERO_SAISON, r.RENCONTRE_ID, STATS_POINTS AS POINTS, STATS_FAUTES AS FAUTES "
	    + "FROM STATS s, RENCONTRE r "
	    + "WHERE s.JOUEUR_ID = ? "
	    + "AND s.RENCONTRE_ID = r.RENCONTRE_ID "
	    + ") AS RESULT "
	    + "GROUP BY NUMERO_SAISON "
	    + ") as saison; ";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1, joueurId);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

}
