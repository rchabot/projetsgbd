package federationbasket.src;
import java.sql.*;

public class Table {
    public static void launchCreation() throws SQLException{
	Statement stmt = null;
	Connection conn = null;
	conn = ConnectionDB.getInstance();
	    
	stmt = conn.createStatement();
	
	System.out.println("Creating tables ...");
	
	Table.dropTables(stmt);
	
	Table.createClub(stmt);
	Table.createPersonne(stmt);
	Table.createPersonneMorale(stmt);
	Table.createCategorie(stmt);
	Table.createEquipe(stmt);
	Table.createJoueur(stmt);
	Table.createEntraine(stmt);
	Table.createRencontre(stmt);
	Table.createStats(stmt);
	
	System.out.println("Tables created successfully");
    }

    public static void launchDropping() throws SQLException{
	Connection conn = null;
	Statement stmt = null;
	conn = ConnectionDB.getInstance();
	stmt = conn.createStatement();
	System.out.println("Dropping tables...");
	Table.dropTables(stmt);
	System.out.println("Tables dropped successfully");
    }

        
    public static void dropTables(Statement stmt) throws SQLException{
	String SET_FK_CHECK_0 = "SET FOREIGN_KEY_CHECKS = 0;";
	String DROP_CLUB  = "drop table if exists CLUB;";
	String DROP_CATEGORIE ="drop table if exists CATEGORIE;";
	String DROP_EQUIPE = "drop table if exists EQUIPE;";
	String DROP_ENTRAINE = "drop table if exists ENTRAINE";
	String DROP_JOUEUR = "drop table if exists JOUEUR;";
	String DROP_PERSONNE = "drop table if exists PERSONNE;";
	String DROP_PERSONNE_MORALE ="drop table if exists PERSONNE_MORALE;";
	String DROP_RENCONTRE = "drop table if exists RENCONTRE;";
	String DROP_STATS = "drop table if exists STATS;";
	String SET_FK_CHECK_1 = "SET FOREIGN_KEY_CHECKS = 1";

	stmt.executeUpdate(SET_FK_CHECK_0);
	stmt.executeUpdate(DROP_CATEGORIE);
	stmt.executeUpdate(DROP_CLUB);
	stmt.executeUpdate(DROP_EQUIPE);
	stmt.executeUpdate(DROP_ENTRAINE);
	stmt.executeUpdate(DROP_JOUEUR);
	stmt.executeUpdate(DROP_PERSONNE);
	stmt.executeUpdate(DROP_PERSONNE_MORALE);
	stmt.executeUpdate(DROP_RENCONTRE);
	stmt.executeUpdate(DROP_STATS);
	stmt.executeUpdate(SET_FK_CHECK_1);
					   
    }

    
    public static void createClub(Statement stmt) throws SQLException{
	String CREATE_CLUB = "create table CLUB("
	    + "CLUB_ID INT UNSIGNED AUTO_INCREMENT not null,"
	    + "CLUB_NOM VARCHAR(255) not null,"
	    + "CLUB_ADRESSE VARCHAR(255),"
	    + "CLUB_TELEPHONE CHAR(10),"
	    + "constraint pk_club primary key (CLUB_ID));";

	stmt.executeUpdate(CREATE_CLUB);
    }
    
    public static void createPersonne(Statement stmt) throws SQLException {
	String CREATE_PERSONNE = "create table PERSONNE("
	    + "PERSONNE_ID INT UNSIGNED AUTO_INCREMENT not null,"
	    + "PERSONNE_NOM CHAR(20) not null,"
	    + "PERSONNE_PRENOM CHAR(20) not null,"
	    + "PERSONNE_DATE_DE_NAISSANCE DATE,"
	    + "PERSONNE_ADRESSE VARCHAR(255),"
	    + "PERSONNE_DATE_ENTREE DATE,"
	    + "CLUB_ID INT UNSIGNED not null,"
	    + "constraint pk_personne primary key (PERSONNE_ID),"
	    + "constraint fk1_personne foreign key (CLUB_ID)"
	    + "references CLUB (CLUB_ID));";
	
	stmt.executeUpdate(CREATE_PERSONNE);
    }
    
    public static void createPersonneMorale(Statement stmt) throws SQLException {
	String CREATE_PERSONNE_MORALE = "create table PERSONNE_MORALE("
	    + "PERSONNE_ID INT UNSIGNED not null,"
	    + "FONCTION VARCHAR(255) not null,"
	    + "constraint pk_morale primary key (PERSONNE_ID),"
	    + "constraint fk1_morale foreign key (PERSONNE_ID)"
	    + "references PERSONNE (PERSONNE_ID));";

	stmt.executeUpdate(CREATE_PERSONNE_MORALE);
    }

    public static void createCategorie(Statement stmt) throws SQLException {
	String CREATE_CATEGORIE = "create table CATEGORIE("
	    + "CATEGORIE_ID INT UNSIGNED AUTO_INCREMENT not null,"
	    + "CATEGORIE_NOM CHAR(20) not null,"
	    + "constraint pk_categorie primary key (CATEGORIE_ID));";

	stmt.executeUpdate(CREATE_CATEGORIE);
    }

    public static void createEquipe(Statement stmt) throws SQLException {
	String CREATE_EQUIPE = "create table EQUIPE("
	    + "EQUIPE_ID INT UNSIGNED AUTO_INCREMENT not null,"
	    + "EQUIPE_NUMERO INT UNSIGNED,"
	    + "CLUB_ID INT UNSIGNED not null,"
	    + "CATEGORIE_ID INT UNSIGNED not null,"
	    + "constraint pk_equipe primary key (EQUIPE_ID),"
	    + "constraint fk1_equipe foreign key (CLUB_ID)"
	    + "references CLUB (CLUB_ID),"
	    + "constraint fk2_equipe foreign key (CATEGORIE_ID)"
	    + "references CATEGORIE (CATEGORIE_ID));";


	stmt.executeUpdate(CREATE_EQUIPE);
    }

    public static void createJoueur(Statement stmt) throws SQLException {
	String CREATE_JOUEUR = "create table JOUEUR("
	    + "JOUEUR_ID INT UNSIGNED not null,"
	    + "JOUEUR_NUM_LICENCE CHAR(8) not null,"
	    + "EQUIPE_ID INT UNSIGNED,"
	    + "constraint pk_joueur primary key (JOUEUR_ID),"
	    + "constraint fk1_joueur foreign key (JOUEUR_ID)"
	    + "references PERSONNE (PERSONNE_ID),"
	    + "constraint fk2_joueur foreign key (EQUIPE_ID)"
	    + "references EQUIPE (EQUIPE_ID));";
	
	stmt.executeUpdate(CREATE_JOUEUR);
    }
    
    public static void createEntraine(Statement stmt) throws SQLException {
	String CREATE_ENTRAINE = "create table ENTRAINE("
	    + "ENTRAINEUR_ID INT UNSIGNED AUTO_INCREMENT not null,"
	    + "EQUIPE_ID INT UNSIGNED not null,"
	    + "constraint pk_entraine  primary key (ENTRAINEUR_ID, EQUIPE_ID),"
	    + "constraint fk1_entraine foreign key (ENTRAINEUR_ID)"
	    + "references PERSONNE (PERSONNE_ID),"
	    + "constraint fk2_entraine foreign key (EQUIPE_ID)"
	    + "references EQUIPE (EQUIPE_ID));";

	stmt.executeUpdate(CREATE_ENTRAINE);
    }
    
    public static void createRencontre(Statement stmt) throws SQLException {
	String CREATE_RENCONTRE = "create table RENCONTRE("
	    + "RENCONTRE_ID INT UNSIGNED AUTO_INCREMENT not null,"
	    + "EQUIPE_A_ID INT UNSIGNED not null,"
	    + "EQUIPE_B_ID INT UNSIGNED not null,"
	    + "RENCONTRE_DATE DATE not null,"
	    + "NUMERO_JOURNEE INT UNSIGNED not null,"
	    + "NUMERO_SAISON INT UNSIGNED not null,"
	    + "SCORE_EQUIPE_A INT UNSIGNED,"
	    + "SCORE_EQUIPE_B INT UNSIGNED,"
	    + "constraint pk_rencontre primary key (RENCONTRE_ID),"
	    + "constraint fk1_rencontre foreign key (EQUIPE_A_ID)"
	    + "references EQUIPE (EQUIPE_ID),"
	    + "constraint fk2_rencontre foreign key (EQUIPE_B_ID)"
	    + "references EQUIPE (EQUIPE_ID));";

	stmt.executeUpdate(CREATE_RENCONTRE);
    }
    
    public static void createStats(Statement stmt) throws SQLException {
	String CREATE_STATS = "create table STATS("
	    + "JOUEUR_ID INT UNSIGNED not null,"
	    + "RENCONTRE_ID INT UNSIGNED not null,"
	    + "STATS_POINTS INT UNSIGNED,"
	    + "STATS_FAUTES INT UNSIGNED,"
	    + "constraint pk_stats primary key (JOUEUR_ID,RENCONTRE_ID),"
	    + "constraint fk1_stats foreign key (JOUEUR_ID)"
	    + "references PERSONNE (PERSONNE_ID),"
	    + "constraint fk2_stats foreign key (RENCONTRE_ID)"
	    + "references RENCONTRE(RENCONTRE_ID));";
	
	stmt.executeUpdate(CREATE_STATS);
    }
}
