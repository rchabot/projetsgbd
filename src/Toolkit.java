package federationbasket.src;

import java.sql.*;

public class Toolkit {

    public static boolean isNumeric(String Chaine) {
	return Chaine.matches("[1-9]\\d*");
    }

    public static int getEquipeId(String nomClub, String nomCategorie,int numEquipe){
	String SQL="SELECT EQUIPE_ID "
	    + "FROM GetEquipe "
	    + "WHERE CLUB_NOM = ? "
	    + "AND CATEGORIE_NOM = ?"
	    + "AND EQUIPE_NUMERO= ? ;";
	int id = 0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setString(1, nomClub);
	    stmt.setString(2, nomCategorie);
	    stmt.setInt(3,numEquipe);
	    ResultSet rs = stmt.executeQuery();
	    if (rs != null)
		rs.next();
	    id = rs.getInt(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return id;
    }

    public static int getRencontreId(int equipeAId,int equipeBId,String date,int numJournee,int numSaison)throws SQLException{
	String SQL="SELECT R.RENCONTRE_ID "
	    + "FROM RENCONTRE R "
	    + "WHERE R.EQUIPE_A_ID = ? "
	    + "AND R.EQUIPE_B_ID = ? "
	    + "AND R.RENCONTRE_DATE = ? "
	    + "AND R.NUMERO_JOURNEE = ? "
	    + "AND R.NUMERO_SAISON = ?;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1,equipeAId);
	stmt.setInt(2,equipeBId);
	stmt.setString(3,date);
	stmt.setInt(4,numJournee);
	stmt.setInt(5,numSaison);
	ResultSet rs = stmt.executeQuery();
	rs.next();
	return rs.getInt(1);
    }


    public static int getJoueurId(String nom,String prenom, int equipeId)throws SQLException{
	String SQL = "SELECT J.JOUEUR_ID FROM JOUEUR J,PERSONNE P WHERE J.JOUEUR_ID=P.PERSONNE_ID AND J.EQUIPE_ID=? AND P.PERSONNE_NOM=? AND P.PERSONNE_PRENOM=?;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1,equipeId);
	stmt.setString(2,nom);
	stmt.setString(3,prenom);
	ResultSet rs = stmt.executeQuery();
	rs.next();
	return rs.getInt(1);
    }

    public static boolean isEquipeA(int Equipe1Id,String dateMatch,int numJournee)throws SQLException{
	String SQL="SELECT  r.EQUIPE_B_ID FROM RENCONTRE r WHERE r.EQUIPE_A_ID=? and r.RENCONTRE_DATE=? and r.NUMERO_JOURNEE=?;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1, Equipe1Id);
	stmt.setString(2, dateMatch);
	stmt.setInt(3,numJournee);
	ResultSet rs = stmt.executeQuery();
	if(!rs.isBeforeFirst())
	    return false;
	return true;
    }
    
    public static String getNomClub(int equipeId)throws SQLException{
	String SQL="SELECT CLUB_NOM" +
	    " FROM GetEquipe"
	    + " WHERE EQUIPE_ID=?;"; 
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1, equipeId);
	ResultSet rs = stmt.executeQuery();
	    rs.next();
	return rs.getString(1);

    }

    
        public static ResultSet getEquipesId(String nomClub1,String nomCategorie1, int numEquipe1,String dateMatch,int numJournee)throws SQLException{
	int equipe1Id=Toolkit.getEquipeId(nomClub1,nomCategorie1,numEquipe1);

	String SQL="SELECT  r.EQUIPE_B_ID FROM RENCONTRE r WHERE r.EQUIPE_A_ID=? and r.RENCONTRE_DATE=? and r.NUMERO_JOURNEE=? "+
	    "UNION " +
	    "SELECT  r.EQUIPE_A_ID FROM RENCONTRE r WHERE r.EQUIPE_B_ID=? and r.RENCONTRE_DATE=? and r.NUMERO_JOURNEE=?;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1, equipe1Id);
	stmt.setString(2, dateMatch);
	stmt.setInt(3,numJournee);
	stmt.setInt(4, equipe1Id);
	stmt.setString(5, dateMatch);
	stmt.setInt(6,numJournee);
	ResultSet rs = stmt.executeQuery();
	return rs;

	}

    public static ResultSet getNumEquipes(int equipeId,String nomClub,String nomCategorie) throws SQLException{
	String SQL="SELECT EQUIPE_NUMERO FROM GetEquipe WHERE CLUB_NOM=? AND EQUIPE_ID=? AND CATEGORIE_NOM=? ; ";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	stmt.setInt(2,equipeId);
	stmt.setString(3,nomCategorie);
	
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet getNumsEquipe(String nomClub,String nomCategorie)throws SQLException{
	String SQL="SELECT EQUIPE_NUMERO "
	    + "FROM GetEquipe "
	    + "WHERE CLUB_NOM = ? "
	    + "AND CATEGORIE_NOM = ? ;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	stmt.setString(2, nomCategorie);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static int nbEquipes(String nomCategorie) throws SQLException{
	String CAT_ID = "(select CATEGORIE_ID "
	    + "FROM CATEGORIE c "
	    + "WHERE c.CATEGORIE_NOM = ?) ";

	String SQL = "SELECT count(*) "
	    + "FROM EQUIPE e "
	    + "WHERE e.CATEGORIE_ID = " + CAT_ID + ";";
	
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomCategorie);
	ResultSet rs = stmt.executeQuery();
	rs.next();
	int nbEq = rs.getInt(1);
	rs.close();
	return nbEq;
    }


    public static int nbEquipesClub(String nomClub) {
	String SQL = "SELECT count(*) "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = ?;";
	int nbEq = 0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	    stmt.setString(1, nomClub);
	    ResultSet rs = stmt.executeQuery();
	    rs.next();
	    nbEq = rs.getInt(1);
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return nbEq;
    }

    public static ResultSet getNomsCategorie(String nomClub)throws SQLException{
	String SQL="SELECT DISTINCT CATEGORIE_NOM "
	    + "FROM GetEquipe "
	    + "WHERE CLUB_NOM = ? "
	    + "GROUP BY CATEGORIE_NOM;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	ResultSet rs = stmt.executeQuery();
	return rs;
    }


    public static ResultSet getNomsClub() throws SQLException{
	String SQL ="SELECT c.CLUB_NOM FROM CLUB c "
	    + "GROUP BY CLUB_NOM";
	Connection conn = ConnectionDB.getInstance();
	Statement stmt=conn.createStatement();
	ResultSet rs=stmt.executeQuery(SQL);
	return rs;

    }

    
    public static void printResultSet(ResultSet rs) throws SQLException{
	ResultSetMetaData rsmd=rs.getMetaData();
	while(rs.next()){
	    for(int i=1;i<=rsmd.getColumnCount();i++)
		System.out.print(rs.getString(i)+" | ");
	    System.out.println("");
		
	}
	System.out.println("");
    }

    
    public static int getIdFromNom(String TABLE, String NOM) {
	String SQL = "SELECT "+ TABLE+"_ID "
	    + "FROM "+TABLE+" t "
	    + "WHERE t."+TABLE+"_NOM = ? ;";
	int id = 0;
	try {
	    Connection conn = ConnectionDB.getInstance();
	    PreparedStatement stmt = conn.prepareStatement(SQL);
	
	    stmt.setString(1, NOM);

	    ResultSet rs = stmt.executeQuery();
	    rs.next(); 
	    id = rs.getInt(TABLE+"_ID");
	    rs.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
	return id;
    }

    public static String getNomFromId(String TABLE, int ID) throws SQLException{
	
	String SQL = "SELECT " + TABLE + "_NOM "
	    + "FROM " + TABLE + " t "
	    + "WHERE " + "t." + TABLE +"_ID = ? ;";

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	
	stmt.setInt(1, ID);

	ResultSet rs = stmt.executeQuery();
	rs.next();
	String nom = rs.getString(TABLE+"_NOM");
	rs.close();
	return nom;
    }

    public static ResultSet getNomsCategorie() throws SQLException{
	String SQL="SELECT CATEGORIE_NOM "
	    + "FROM CATEGORIE ;";
	Connection conn = ConnectionDB.getInstance();
	Statement stmt = conn.createStatement();
	ResultSet rs = stmt.executeQuery(SQL);
	return rs;
    }


    public static ResultSet getNomsCategorie(int equipeId,String nomClub) throws SQLException{
	String SQL="SELECT CATEGORIE_NOM FROM GetEquipe WHERE CLUB_NOM=? AND EQUIPE_ID=? ; ";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);	
	stmt.setString(1, nomClub);
	stmt.setInt(2,equipeId);
	
	ResultSet rs = stmt.executeQuery();
	return rs;

    }


    public static int nbMaxSaison() throws SQLException {
	String SQL="SELECT MAX(NUMERO_SAISON) "
	    + "FROM RENCONTRE ;";
	Connection conn = ConnectionDB.getInstance();
	Statement stmt = conn.createStatement();
	ResultSet rs = stmt.executeQuery(SQL);
	rs.next();
	int nbMax = rs.getInt(1);
	rs.close();
	return nbMax;
    }
    
    public static int nbMaxJournee() throws SQLException {
	String SQL="SELECT MAX(NUMERO_JOURNEE) "
	    + "FROM RENCONTRE; ";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	//stmt.setInt(1, numSaison);
	ResultSet rs = stmt.executeQuery();
	rs.next();
	int nbMax = rs.getInt(1);
	rs.close();
	return nbMax;
    }
    
    public static int nbRow(ResultSet rs) throws SQLException {
	rs.last(); // on va à la fin
	int n = rs.getRow();
	rs.beforeFirst(); // on remet au début
	return n;
    }

    public static ResultSet getId(String TABLE) throws SQLException{
	String SQL="SELECT JOUEUR_ID FROM JOUEUR";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	ResultSet rs = stmt.executeQuery();   
	return rs;
    } 
	
    public static String getNomJoueurFromId(int id) throws SQLException{
	String SQL="select P.PERSONNE_NOM, P.PERSONNE_PRENOM FROM PERSONNE P,JOUEUR J WHERE J.JOUEUR_ID=P.PERSONNE_ID AND J.JOUEUR_ID=?;";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	
	stmt.setInt(1, id);

	ResultSet rs = stmt.executeQuery();
	rs.next();
	String joueur=rs.getString(1)+" "+rs.getString(2);
	return joueur;

    }
}
