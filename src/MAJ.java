package federationbasket.src;

import java.sql.*;
import java.util.Vector;

public class MAJ {
    private static final String INSERT = "insert into ";
    private static final String VALUES = "values ";

    /*
     * @args String args au format "(col1, col2, col3)"
     */
    public static void insertClub(String nom,String adresse,String telephone)throws SQLException{
	String SQL="insert into CLUB"
	    + "(CLUB_NOM,CLUB_ADRESSE,CLUB_TELEPHONE)"
	    + " values(?,?,?)";   
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nom);
	stmt.setString(2, adresse);
	stmt.setString(3, telephone);
	stmt.executeUpdate();
	stmt.close();
    }

    public static void insertEquipe(int numEquipe,String nomClub,String nomCategorie) throws SQLException{
	int ClubId=Toolkit.getIdFromNom("CLUB",nomClub);
	int CategorieId=Toolkit.getIdFromNom("CATEGORIE",nomCategorie);
	String SQL="insert into EQUIPE"
	    + "(EQUIPE_NUMERO,CLUB_ID, CATEGORIE_ID)"
	    + " values(?,?,?)";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1,numEquipe);
	stmt.setInt(2,ClubId);
	stmt.setInt(3,CategorieId);
	stmt.executeUpdate();
	stmt.close();

    }

    public static void insertRencontre(String nomClubA,String nomClubB,String nomCategorieA,String nomCategorieB,int numEquipeA,int numEquipeB,String date,int numJournee,int numSaison,int scoreA,int scoreB) throws SQLException{
	int equipeAId=Toolkit.getEquipeId(nomClubA,nomCategorieA,numEquipeA);
	int equipeBId=Toolkit.getEquipeId(nomClubB,nomCategorieB,numEquipeB);
	String SQL="insert into RENCONTRE"
	    +"(EQUIPE_A_ID,EQUIPE_B_ID,RENCONTRE_DATE,NUMERO_JOURNEE,NUMERO_SAISON,SCORE_EQUIPE_A,SCORE_EQUIPE_B)"
	    + " VALUES( ?,?,?,?,?,?,?)";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1,equipeAId);
	stmt.setInt(2,equipeBId);
	stmt.setString(3,date);
	stmt.setInt(4,numJournee);
	stmt.setInt(5,numSaison); 
	if(scoreA==0)
	    stmt.setNull(6,java.sql.Types.INTEGER);
	else
	    stmt.setInt(6,scoreA);
	if (scoreB==0)
	    stmt.setNull(7,java.sql.Types.INTEGER);
	else
	    stmt.setInt(7,scoreB);
	stmt.executeUpdate();
	stmt.close();   

    } 
    
    public static void insertPersonne(String nom, String prenom, String naissance, String adresse, String entree, String nomClub) throws SQLException{
	int club_id=Toolkit.getIdFromNom("CLUB",nomClub);
	String SQL="insert into PERSONNE"
	    + "(PERSONNE_NOM, PERSONNE_PRENOM, PERSONNE_DATE_DE_NAISSANCE, PERSONNE_ADRESSE, PERSONNE_DATE_ENTREE, CLUB_ID)"
	    + " values( ?, ?, ?, ?, ?, ?);";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nom);
	stmt.setString(2, prenom);
	stmt.setString(3, naissance);
	stmt.setString(4, adresse);
	stmt.setString(5, entree);
	stmt.setInt(6, club_id);
	stmt.executeUpdate();
	stmt.close();
    }

    
    public static void insertStatistique(int joueurId,int rencontreId,int statsPoints,int statsFautes) throws SQLException{
	String SQL="insert into STATS"
	    + "(JOUEUR_ID,RENCONTRE_ID, STATS_POINTS,STATS_FAUTES)"
	    + " values (?,?,?,?)";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1,joueurId);
	stmt.setInt(2,rencontreId);
	if(statsPoints==-1)
	    stmt.setNull(3,java.sql.Types.INTEGER);
	else
	    stmt.setInt(3,statsPoints);
	if(statsFautes==-1)
	    stmt.setNull(4,java.sql.Types.INTEGER);
	else
	    stmt.setInt(4,statsFautes);
	stmt.executeUpdate();
	stmt.close();
    }


    public static void insertPersonneMorale(String nom,String prenom,String naissance,String adresse,String entree,String nomClub, String fonction) throws SQLException {
	MAJ.insertPersonne(nom,prenom,naissance,adresse,entree,nomClub);
	int personneId=Toolkit.getIdFromNom("PERSONNE",nom);
	String SQL="insert into  PERSONNE_MORALE(PERSONNE_ID,FONCTION)"+ " values("+personneId+','+'\''+fonction+'\''+')';
	Connection conn = ConnectionDB.getInstance();
	Statement stmt=conn.createStatement();
	stmt.executeUpdate(SQL);
    }



    public static void insertJoueur(String nom,String prenom,String naissance,String adresse,String entree,String numLicence,String nomClub, String nomCategorie,int numEquipe) throws SQLException {
	MAJ.insertPersonne(nom,prenom,naissance,adresse,entree,nomClub);
	int equipeId=Toolkit.getEquipeId(nomClub,nomCategorie,numEquipe);
	//Recuperation de l'identifiant de la personne
	int joueurId=Toolkit.getIdFromNom("PERSONNE",nom);
	String SQL="insert into JOUEUR(JOUEUR_ID,JOUEUR_NUM_LICENCE,EQUIPE_ID) "
	    + "values(?, ?, ?);";
	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1, joueurId);
	stmt.setString(2, numLicence);
	stmt.setInt(3, equipeId);
	stmt.executeUpdate();
	stmt.close();
    }

    

    public static void insertEntraine(String nom,String prenom,String naissance,String adresse,String entree,String nomClub, String nomCategorie,int numEquipe) throws SQLException {
   	MAJ.insertPersonne(nom,prenom,naissance,adresse,entree,nomClub);
	int equipeId=Toolkit.getEquipeId(nomClub,nomCategorie,numEquipe);
	int entraineurId=Toolkit.getIdFromNom("PERSONNE",nom);
	String SQL="insert into ENTRAINE"
	    + "(ENTRAINEUR_ID,EQUIPE_ID)"
	    + " values(?,?)";
    	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setInt(1,entraineurId);
	stmt.setInt(2,equipeId);
	stmt.executeUpdate();
	stmt.close();    
    }


    public static void insert(String table, String args, String[] columnNames, Statement stmt) throws SQLException{
	
	int length = columnNames.length;
	StringBuffer buffer = new StringBuffer();
	
	buffer.append(INSERT);
	buffer.append(table+"(");
	
	int i;
	for (i = 0; i < length - 1; i++)
	    buffer.append(columnNames[i]+", ");
	buffer.append(columnNames[i++]);

	buffer.append(") " + VALUES + args +";");
	
	String SQL = buffer.toString();
	//System.out.println(SQL);
	stmt.executeUpdate(SQL);
    }

    public static void update(String table,int id,Vector<String> columnNames,Vector<String> values)throws SQLException{
	int length=columnNames.size();
	System.out.println(length);
	StringBuffer buffer = new StringBuffer();
	buffer.append("UPDATE "+table+" SET " );
	int i;
	for( i=0;i<length-1;i++)
	    //  buffer.append(columnNames.get(i)+" = ?, ");
	    buffer.append(columnNames.get(i)+" = "+values.get(i)+" , ");

	buffer.append(columnNames.get(i)+" = "+values.get(i));
	//buffer.append(columnNames.get(i)+" = ?");
	//buffer.append(" WHERE "+ table+"_ID"+" = ?;");
	buffer.append(" WHERE "+ table+"_ID"+" ="+id+';');
	String SQL = buffer.toString();
	//String adresse="54",Id="1";
	//String SQL="update CLUB SET CLUB_ADRESSE= "+adresse+", CLUB_TELEPHONE = '4' WHERE CLUB_ID="+Id+';';
	System.out.println(SQL);
	Connection conn = ConnectionDB.getInstance();
	
	PreparedStatement stmt = conn.prepareStatement(SQL);
	/*stmt.setString(1,"45");
	  stmt.setString(2,"50");
	  stmt.setInt(3,1);*/
	/*for(int j=0;j<length;j++){
	      if (Toolkit.isNumeric(values.get(j)))
	      stmt.setInt(j+1,Integer.parseInt(values.get(j)));
	          else
		  stmt.setString(j+1,values.get(j));
		  }
		  stmt.setInt(length+1,id);*/

	//  System.out.println(SQL);
	stmt.executeUpdate(SQL);
    }

    
}
