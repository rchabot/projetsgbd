package federationbasket.src;

import java.sql.*;

public class Database {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
    static final String DB_NAME = "FederationBasket";
    static final String DB_URL = "jdbc:mysql://localhost/" + DB_NAME;
    static final String USER = "root";
    static final String PASS = "rchabot";

    private static void createDatabase() throws SQLException {
	Statement stmt = null;
	Connection conn = null;
	conn = ConnectionDB.getInstance();
	    
	stmt = conn.createStatement();
	
	System.out.println("Creating "+DB_NAME+" database ...");

	stmt.executeUpdate("create database if not exists "+DB_NAME);
	Table.launchCreation();
	Donnees.launchCreation();
	Vues.launchCreation();
	
	System.out.println(DB_NAME+" created");
    }

    private static void dropDatabase() throws SQLException {
	Statement stmt = null;
	Connection conn = null;
	conn = ConnectionDB.getInstance();
	    
	stmt = conn.createStatement();
	
	System.out.println("Dropping "+DB_NAME+" database ...");

	Table.launchDropping();
	Vues.launchDropping();
	stmt.executeUpdate("drop database if exists "+DB_NAME);

	System.out.println(DB_NAME+" created");
    }

    public static void main(String... args) throws Exception{
	try{
	    if (args[1] == "oui")
		createDatabase();
	    dropDatabase();

	}catch(SQLException se){
	    se.printStackTrace();
	}catch(Exception e){
	    e.printStackTrace();
	}
	
    }
    
}
