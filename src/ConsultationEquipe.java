package federationbasket.src;

import java.sql.*;

public class ConsultationEquipe {

    public static ResultSet joueursEquipe(String nomClub, String catEquipe, int numEquipe) throws SQLException {
	Connection conn = ConnectionDB.getInstance();
	String EQUIPE_ID = "(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";
	    
	String SQL = "SELECT PERSONNE_NOM, PERSONNE_PRENOM "
	    + "FROM JOUEUR j, PERSONNE p "
	    + "WHERE j.EQUIPE_ID = " + EQUIPE_ID
	    + "AND j.JOUEUR_ID = p.PERSONNE_ID "
	    + "ORDER BY PERSONNE_ID;";

	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	stmt.setString(2, catEquipe);
	stmt.setInt(3, numEquipe);
	
	ResultSet rs = stmt.executeQuery();
	return rs;
    }


    public static ResultSet entraineursEquipe(String nomClub, String catEquipe, int numEquipe) throws SQLException {
	Connection conn = ConnectionDB.getInstance();
	String EQUIPE_ID = "(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";

	String SQL = "SELECT PERSONNE_NOM, PERSONNE_PRENOM "
	    + "FROM PERSONNE p, ENTRAINE ent "
	    + "WHERE p.PERSONNE_ID = ent.ENTRAINEUR_ID "
	    + "AND ent.EQUIPE_ID = " + EQUIPE_ID 
	    + "ORDER BY PERSONNE_ID;";
	
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	stmt.setString(2, catEquipe);
	stmt.setInt(3, numEquipe);

	ResultSet rs = stmt.executeQuery();
	return rs;
    }


    public static ResultSet infoEquipe(String nomClub, String catEquipe, int numEquipe) throws SQLException {
	String EQUIPE_ID = "(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";

	String JOUEURS = "SELECT PERSONNE_NOM, PERSONNE_PRENOM "
	    + "FROM PERSONNE p, EQUIPE e, JOUEUR j "
	    + "WHERE j.EQUIPE_ID = " + EQUIPE_ID
	    + "AND j.JOUEUR_ID = p.PERSONNE_ID ";
	
	String ENTRAINEURS = "SELECT PERSONNE_NOM, PERSONNE_PRENOM "
	    + "FROM PERSONNE p, ENTRAINE ent "
	    + "WHERE p.PERSONNE_ID = ent.ENTRAINEUR_ID "
	    + "AND ent.EQUIPE_ID = " + EQUIPE_ID ;

	String SQL = ENTRAINEURS + "UNION " + JOUEURS + ";";

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	stmt.setString(2, catEquipe);
	stmt.setInt(3, numEquipe);
	stmt.setString(4, nomClub);
	stmt.setString(5, catEquipe);
	stmt.setInt(6, numEquipe);

	ResultSet rs = stmt.executeQuery();

	return rs;
    }

        public static ResultSet resultatsEquipe(String nomClub, String nomCat, int numEq, int numSaison, int numJournee) throws SQLException{
	String EQUIPE_ID = "(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";

	String SQL = "SELECT eqA.CLUB_NOM AS EQUIPE_A, eqA.EQUIPE_NUMERO AS NUM, SCORE_EQUIPE_A, SCORE_EQUIPE_B, eqB.CLUB_NOM AS EQUIPE_B, eqB.EQUIPE_NUMERO AS NUM, r.RENCONTRE_DATE AS DATE, r.NUMERO_SAISON AS SAISON, r.NUMERO_JOURNEE AS JOURNEE "
	    + "FROM RENCONTRE r, GetEquipe eqA, GetEquipe eqB "
	    + "WHERE (r.EQUIPE_A_ID = " + EQUIPE_ID //1,2,3
	    + "OR r.EQUIPE_B_ID = " + EQUIPE_ID + ") " //4,5,6
	    + "AND r.EQUIPE_A_ID = eqA.EQUIPE_ID "
	    + "AND r.EQUIPE_B_ID = eqB.EQUIPE_ID "
	    + "AND r.NUMERO_SAISON = ? " //7
	    + "AND r.NUMERO_JOURNEE = ? " //8
	    + "ORDER BY RENCONTRE_DATE; ";

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	stmt.setString(2, nomCat);
	stmt.setInt(3, numEq);
	stmt.setString(4, nomClub);
	stmt.setString(5, nomCat);
	stmt.setInt(6, numEq);
	stmt.setInt(7, numSaison);
	stmt.setInt(8, numJournee);

	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet resultatsEquipe(String nomClub, String nomCat, int numEq, String date) throws SQLException{
	String EQUIPE_ID = "(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";

	String SQL = "SELECT eqA.CLUB_NOM AS EQUIPE_A, eqA.EQUIPE_NUMERO AS NUM, SCORE_EQUIPE_A, SCORE_EQUIPE_B, eqB.CLUB_NOM AS EQUIPE_B, eqB.EQUIPE_NUMERO AS NUM, r.RENCONTRE_DATE AS DATE, r.NUMERO_SAISON AS SAISON, r.NUMERO_JOURNEE AS JOURNEE "
	    + "FROM RENCONTRE r, GetEquipe eqA, GetEquipe eqB "
	    + "WHERE (r.EQUIPE_A_ID = " + EQUIPE_ID //1,2,3
	    + "OR r.EQUIPE_B_ID = " + EQUIPE_ID + ") " //4,5,6
	    + "AND r.EQUIPE_A_ID = eqA.EQUIPE_ID "
	    + "AND r.EQUIPE_B_ID = eqB.EQUIPE_ID "
	    + "AND r.RENCONTRE_DATE <= ? "//7
	    + "ORDER BY RENCONTRE_DATE; ";
	    

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	stmt.setString(2, nomCat);
	stmt.setInt(3, numEq);
	stmt.setString(4, nomClub);
	stmt.setString(5, nomCat);
	stmt.setInt(6, numEq);
	stmt.setString(7, date);

	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    public static ResultSet resultatsEquipe(String nomClub, String nomCat, int numEq, int numSaison) throws SQLException{
	String EQUIPE_ID = "(SELECT EQUIPE_ID "
	    + "FROM GetEquipe e "
	    + "WHERE e.CLUB_NOM = UPPER(?) "
	    + "AND e.CATEGORIE_NOM = UPPER(?) "
	    + "AND e.EQUIPE_NUMERO = ? ) ";

	String SQL = "SELECT eqA.CLUB_NOM AS EQUIPE_A, eqA.EQUIPE_NUMERO AS NUM, SCORE_EQUIPE_A, SCORE_EQUIPE_B, eqB.CLUB_NOM AS EQUIPE_B, eqB.EQUIPE_NUMERO AS NUM, r.RENCONTRE_DATE AS DATE, r.NUMERO_SAISON AS SAISON, r.NUMERO_JOURNEE AS JOURNEE "
	    + "FROM RENCONTRE r, GetEquipe eqA, GetEquipe eqB "
	    + "WHERE (r.EQUIPE_A_ID = " + EQUIPE_ID //1,2,3
	    + "OR r.EQUIPE_B_ID = " + EQUIPE_ID + ") " //4,5,6
	    + "AND r.EQUIPE_A_ID = eqA.EQUIPE_ID "
	    + "AND r.EQUIPE_B_ID = eqB.EQUIPE_ID "
	    + "AND r.NUMERO_SAISON = ? "//7
	    + "ORDER BY RENCONTRE_DATE; ";
	    

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(SQL);
	stmt.setString(1, nomClub);
	stmt.setString(2, nomCat);
	stmt.setInt(3, numEq);
	stmt.setString(4, nomClub);
	stmt.setString(5, nomCat);
	stmt.setInt(6, numEq);
	stmt.setInt(7, numSaison);

	ResultSet rs = stmt.executeQuery();
	return rs;
    }

    
}
