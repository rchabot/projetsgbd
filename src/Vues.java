package federationbasket.src;

import java.sql.*;

public class Vues{

    public static void launchCreation() throws SQLException{
	Connection conn = ConnectionDB.getInstance();
	Statement stmt = conn.createStatement();
	System.out.println("Creating views...");
	vueGetEquipe(stmt);
	System.out.println("Views created");
	stmt.close();
    }

    public static void launchDropping() throws SQLException{
	Connection conn = ConnectionDB.getInstance();
	Statement stmt = conn.createStatement();
	System.out.println("Deleting views...");
	dropViews(stmt);
	System.out.println("Views deleted");
	stmt.close();
    }

    public static void dropViews(Statement stmt) throws SQLException {
	String DROP_GET_EQUIPE = "drop view if exists GetEquipe;";
	stmt.executeUpdate(DROP_GET_EQUIPE);
    }

    public static void vueGetEquipe(Statement stmt) throws SQLException{
	String SQL = "create or replace view GetEquipe as "
	    + "select e.EQUIPE_ID, c.CLUB_NOM, cat.CATEGORIE_NOM, e.EQUIPE_NUMERO "
	    + "from EQUIPE e, CLUB c, CATEGORIE cat "
	    + "where e.CLUB_ID = c.CLUB_ID "
	    + "and e.CATEGORIE_ID = cat.CATEGORIE_ID;";
	
	stmt.executeUpdate(SQL);
    }

}
