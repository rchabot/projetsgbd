package federationbasket.src;

import java.sql.*;

public class Suppression {

    public static void supprimerJoueur(int joueurID) throws SQLException{
	String req = "DELETE FROM STATS WHERE joueur_id=?";
	String req2 = "DELETE FROM JOUEUR WHERE joueur_id=?";
	String req3 = "DELETE FROM PERSONNE WHERE personne_id=?";

	Connection conn = ConnectionDB.getInstance();

	PreparedStatement stmt = conn.prepareStatement(req);
	PreparedStatement stmt2 = conn.prepareStatement(req2);
	PreparedStatement stmt3 = conn.prepareStatement(req3);

	stmt.setInt(1, joueurID);
	stmt2.setInt(1, joueurID);
	stmt3.setInt(1, joueurID);

	stmt.executeUpdate();
	stmt2.executeUpdate();
	stmt3.executeUpdate();

	stmt.close();
	stmt2.close();
	stmt3.close();
    }

    public static void supprimerEntraineur(int entraineurID) throws SQLException{

	String req = "DELETE FROM ENTRAINE WHERE entraineur_id=?";
	String req2 = "DELETE FROM PERSONNE WHERE personne_id=?";
	Connection conn = ConnectionDB.getInstance();

	PreparedStatement stmt = conn.prepareStatement(req);
	PreparedStatement stmt2 = conn.prepareStatement(req2);

	stmt.setInt(1, entraineurID);
	stmt2.setInt(1, entraineurID);

	stmt.executeUpdate();
	stmt2.executeUpdate();
    }

    public static void supprimerRencontre(int rencontreID) throws SQLException{
	String req="DELETE FROM STATS WHERE RENCONTRE_ID=?";
	String req2="DELETE FROM RENCONTRE WHERE RENCONTRE_ID=?";

	Connection conn = ConnectionDB.getInstance();
	PreparedStatement stmt = conn.prepareStatement(req);
	PreparedStatement stmt2 = conn.prepareStatement(req2);
	
	stmt.setInt(1, rencontreID);
	stmt2.setInt(1, rencontreID);
	
	stmt.executeUpdate();
	stmt2.executeUpdate();
	

    }

    public static void supprimerEquipe(int equipeID) throws SQLException{
	String req = "DELETE FROM STATS WHERE rencontre_id IN ( "
	    + "SELECT rencontre_id FROM RENCONTRE "
	    + "WHERE equipe_a_id=? OR equipe_b_id=?); ";
	String req2 = "DELETE FROM RENCONTRE WHERE equipe_a_id=? OR equipe_b_id=?; ";
	String req3 = "DELETE FROM JOUEUR WHERE equipe_id=? ;";
	String req4 = "DELETE FROM ENTRAINE WHERE EQUIPE_ID = ?; ";
	String req5 = "DELETE FROM EQUIPE WHERE equipe_id=? ;";

	Connection conn = ConnectionDB.getInstance();

	PreparedStatement stmt = conn.prepareStatement(req);
	PreparedStatement stmt2 = conn.prepareStatement(req2);
	PreparedStatement stmt3 = conn.prepareStatement(req3);
	PreparedStatement stmt4 = conn.prepareStatement(req4);
	PreparedStatement stmt5 = conn.prepareStatement(req5);
		
	stmt.setInt(1, equipeID);
	stmt.setInt(2, equipeID);
	stmt2.setInt(1, equipeID);
	stmt2.setInt(2, equipeID);
	stmt3.setInt(1, equipeID);
	stmt4.setInt(1, equipeID);
	stmt5.setInt(1, equipeID);
	stmt.executeUpdate();
	stmt2.executeUpdate();
	stmt3.executeUpdate();
	stmt4.executeUpdate();
	stmt5.executeUpdate();
    }

    public static void supprimerClub(int clubID) throws SQLException{
	String req1 = "DELETE FROM STATS WHERE joueur_id IN "
	    + "(SELECT personne_id FROM PERSONNE "
	    + "WHERE club_id=?);";
	
	String req2 = "DELETE FROM STATS WHERE rencontre_id IN "
	    + "(SELECT rencontre_id FROM RENCONTRE "
	    + "WHERE equipe_a_id IN "
	    + "(SELECT equipe_id FROM EQUIPE "
	    + "WHERE club_id=?) "
	    + "UNION "
	    + "SELECT rencontre_id FROM RENCONTRE "
	    + "WHERE equipe_b_id IN "
	    + "(SELECT equipe_id FROM EQUIPE "
	    + "WHERE club_id=?)); ";

	String req3 = "DELETE FROM JOUEUR WHERE joueur_id IN "
	    + "(SELECT personne_id FROM PERSONNE "
	    + "WHERE club_id=?); ";

	String req4 = "DELETE FROM PERSONNE_MORALE WHERE personne_id IN  "
	    + "(SELECT personne_id FROM PERSONNE "
	    + "WHERE club_id=?); ";

	String req5 ="DELETE FROM ENTRAINE WHERE equipe_id IN "
	    + "(SELECT personne_id FROM PERSONNE "
	    + "WHERE club_id=?); ";

	String req6 = "DELETE FROM PERSONNE WHERE club_id=?; ";

	String req7 = "DELETE FROM RENCONTRE WHERE equipe_a_id  IN (SELECT equipe_id FROM EQUIPE "
	    + "WHERE club_id=?)    OR equipe_b_id IN  "
	    + "(SELECT equipe_id FROM EQUIPE "
	    + "WHERE club_id=?); ";

	String req8 = "DELETE FROM EQUIPE WHERE club_id=?; ";

	String req9 = "DELETE FROM CLUB where club_id=?;";
	    
	    


	Connection conn = ConnectionDB.getInstance();

	PreparedStatement stmt = conn.prepareStatement(req1);
	PreparedStatement stmt2 = conn.prepareStatement(req2);
	PreparedStatement stmt3 = conn.prepareStatement(req3);
	PreparedStatement stmt4 = conn.prepareStatement(req4);
	PreparedStatement stmt5 = conn.prepareStatement(req5);
	PreparedStatement stmt6 = conn.prepareStatement(req6);
	PreparedStatement stmt7 = conn.prepareStatement(req7);
	PreparedStatement stmt8 = conn.prepareStatement(req8);
	PreparedStatement stmt9 = conn.prepareStatement(req9);

	stmt.setInt(1, clubID);
	stmt2.setInt(1, clubID);
	stmt2.setInt(2, clubID);
	stmt3.setInt(1, clubID);
	stmt4.setInt(1, clubID);
	stmt5.setInt(1, clubID);
	stmt6.setInt(1, clubID);
	stmt7.setInt(1, clubID);
	stmt7.setInt(2, clubID);
	stmt8.setInt(1, clubID);
	stmt9.setInt(1, clubID);

	stmt.executeUpdate();
	stmt2.executeUpdate();
	stmt3.executeUpdate();
	stmt4.executeUpdate();
	stmt5.executeUpdate();
	stmt6.executeUpdate();
	stmt7.executeUpdate();
	stmt8.executeUpdate();
	stmt9.executeUpdate();
    }
}
