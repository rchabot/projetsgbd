package federationbasket.tst;

import java.sql.*;
import federationbasket.src.*;

public class TestStatistiques {
    void testClassementJoueursPoints() throws SQLException{
	ResultSet rs = Statistiques.classementJoueurs("SENIOR", 1, 2, "FAUTES");
	//Toolkit.printResultSet(rs);
	rs.close();
    }

    void testClassementJoueursFautes() throws SQLException{
	ResultSet rs = Statistiques.classementJoueurs("SENIOR", 1, 2, "POINTS");
	//Toolkit.printResultSet(rs);
	rs.close();
    }

    void testMoyenneParMatchSaison() throws SQLException{
	double rs = Statistiques.moyenneParMatch(1);
	assert Math.abs(rs - 144.5714) < 0.0001 : "moyenne points";
	double rs1 = Statistiques.moyenneParMatch(1, "junior");
	assert Math.abs(rs1 - 114) < 0.0001 : "moyenne points";
    }

    void testMoyenneParMatchDate() throws SQLException{
	double rs = Statistiques.moyenneParMatch("2015-01-13");
	assert Math.abs(rs - 143.5) < 0.0001 : "moyenne points";
    }

    void testClassement() throws SQLException{
	ResultSet rs = Statistiques.classementEquipes(1,"senior");
	Toolkit.printResultSet(rs);
	rs.close();
    }

}
