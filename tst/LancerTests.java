package federationbasket.tst;

import java.lang.reflect.*;
import java.sql.*;
import federationbasket.src.*;

class LancerTests {

    public static void main(String... args) throws Exception{
	boolean estMisAssertion = false;
	assert estMisAssertion = true;

	if (!estMisAssertion)
	    System.out.println("Execution impossible sans l'option -ea");
	
	try{
	    CreateDatabase.createDatabase();
	    
	    lancer(TestToolkit.class);
	    lancer(TestConsultationJoueur.class);
	    lancer(TestConsultationEquipe.class);
	    lancer(TestConsultationClub.class);
	    lancer(TestStatistiques.class);
	    //lancer(TestConsultationResultats.class);
	    
	    DropDatabase.dropDatabase();
	}catch(SQLException se){
	    se.printStackTrace();
	}catch(Exception e){
	    e.printStackTrace();
	}
	
    }
    
    private static void lancer(Class c) throws SQLException, Exception{
	int nbTest = 0;
	
	Object o =c.newInstance();
	Method[] m=c.getDeclaredMethods();
	System.out.print(c.getName()+":");
	for(int i=0;i<m.length;i++){
	    if (m[i].getName().startsWith("test")){
		System.out.print(".");
		m[i].invoke(o);
		nbTest++;
	    }
	}
	System.out.println("(" + nbTest + ")");
    }



}
