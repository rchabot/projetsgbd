package federationbasket.tst;

import java.sql.*;
import federationbasket.src.*;

public class TestToolkit {
    void testNumsEquipe() throws SQLException {
	ResultSet rs = Toolkit.getNumsEquipe("CSP", "JUNIOR");
	rs.next();
	assert rs.getInt(1) == 1 : "eq1";
	rs.next();
	assert rs.getInt(1) == 2 : "eq2";
	rs.close();
    }

    void testNomsCategorie() throws SQLException {
	ResultSet rs = Toolkit.getNomsCategorie("CSP");
	//Toolkit.printResultSet(rs);
	rs.next();
	assert rs.getString(1).equals("JUNIOR") : "cat cadet";
	rs.next();
	assert rs.getString(1).equals("SENIOR") : "cat senior";
	rs.close();
    }

    void testNomsClubs() throws SQLException{
	ResultSet rs = Toolkit.getNomsClub();
	//Toolkit.printResultSet(rs);
	rs.close();
    }

    void testNbEquipes() throws SQLException{
	int nbEq = Toolkit.nbEquipes("SENIOR");
	assert nbEq == 4;
    }

}
