
package federationbasket.tst;

import java.sql.*;
import federationbasket.src.*;

public class TestConsultationResultats{
    
    void testListeScoreToDate1() throws SQLException{
	ResultSet rs = ConsultationResultats.listeScoreToDate("2015-01-13");
	rs.next();
	assert rs.getInt(1) == 1 : "id eqA";
	assert rs.getInt(2) == 64: "score eqA";
	assert rs.getInt(3) == 63: "score eqB";
	assert rs.getInt(4) == 2 : "id eqB";
	rs.next();
	assert rs.getInt(1) == 3 : "id eqA";
	assert rs.getInt(2) == 75: "score eqA";
	assert rs.getInt(3) == 97: "score eqB";
	assert rs.getInt(4) == 4 : "id eqB";
	rs.next();
	assert rs.getInt(1) == 1 : "id eqA";
	assert rs.getInt(2) == 63: "score eqA";
	assert rs.getInt(3) == 72: "score eqB";
	assert rs.getInt(4) == 4 : "id eqB";
	rs.close();
    }

    void testListeScoreToDate2() throws SQLException{
	ResultSet rs = ConsultationResultats.listeScoreToDate(1);
	rs.next();
	assert rs.getInt(1) == 1 : "id eqA";
	assert rs.getInt(2) == 64: "score eqA";
	assert rs.getInt(3) == 63: "score eqB";
	assert rs.getInt(4) == 2 : "id eqB";
	rs.next();
	assert rs.getInt(1) == 3 : "id eqA";
	assert rs.getInt(2) == 75: "score eqA";
	assert rs.getInt(3) == 97: "score eqB";
	assert rs.getInt(4) == 4 : "id eqB";
	rs.next();
	assert rs.getInt(1) == 1 : "id eqA";
	assert rs.getInt(2) == 63: "score eqA";
	assert rs.getInt(3) == 72: "score eqB";
	assert rs.getInt(4) == 4 : "id eqB";
	rs.next();
	assert rs.getInt(1) == 2 : "id eqA";
	assert rs.getInt(2) == 80: "score eqA";
	assert rs.getInt(3) == 59: "score eqB";
	assert rs.getInt(4) == 3 : "id eqB";
	rs.close();
    }

    void testListeScoreAtDate1() throws SQLException{
	ResultSet rs = ConsultationResultats.listeScoreAtDate("2015-01-10");
	rs.next();
	assert rs.getInt(1) == 1 : "id eqA";
	assert rs.getInt(2) == 64: "score eqA";
	assert rs.getInt(3) == 63: "score eqB";
	assert rs.getInt(4) == 2 : "id eqB";
	rs.next();
	assert rs.getInt(1) == 3 : "id eqA";
	assert rs.getInt(2) == 75: "score eqA";
	assert rs.getInt(3) == 97: "score eqB";
	assert rs.getInt(4) == 4 : "id eqB";
	rs.close();
    }
    
    void testListeScoreAtDate2() throws SQLException{
	ResultSet rs = ConsultationResultats.listeScoreAtDate(1,1);
	rs.next();
	assert rs.getInt(1) == 1 : "id eqA";
	assert rs.getInt(2) == 64: "score eqA";
	assert rs.getInt(3) == 63: "score eqB";
	assert rs.getInt(4) == 2 : "id eqB";
	rs.next();
	assert rs.getInt(1) == 3 : "id eqA";
	assert rs.getInt(2) == 75: "score eqA";
	assert rs.getInt(3) == 97: "score eqB";
	assert rs.getInt(4) == 4 : "id eqB";
	rs.close();
    }

    void testNbVictoireClub() {
	int rs1 = ConsultationResultats.nbVictoires("csp");
	assert rs1 == 1 : "nb victoires 1";
	int rs2 = ConsultationResultats.nbVictoires("sa");
	assert rs2 == 3 : "nb victoires 2";
    }

    void testNbDefaiteClub() throws SQLException{
	int rs1 = ConsultationResultats.nbDefaites("csp");
	assert rs1 == 3 : "nb defaites 1";
	int rs2 = ConsultationResultats.nbDefaites("lakers");
	assert rs2 == 2 : "nb defaite 2";
    }

    void testNbNulClub() throws SQLException{
	int rs1 = ConsultationResultats.nbNuls("csp");
	assert rs1 == 1 : "nb nul 1";
	int rs2 = ConsultationResultats.nbNuls("all stars");
	assert rs2 == 0 : "nb nul 1";
    }

    void testNbVictoireEquipe() throws SQLException{
	int rs1 = ConsultationResultats.nbVictoires("all stars", "senior",1);
	assert rs1 == 3 : "nb victoires 1";
	int rs2 = ConsultationResultats.nbVictoires("csp", "junior",2);
	assert rs2 == 0 : "nb victoires 2";
    }

    void testNbDefaiteEquipe() throws SQLException{
	int rs1 = ConsultationResultats.nbDefaites("all stars", "senior",1);
	assert rs1 == 0 : "nb defaites 1";
	int rs2 = ConsultationResultats.nbDefaites("sa", "senior",1);
	assert rs2 == 2 : "nb defaites 2";
    }

    void testNbNulEquipe() throws SQLException{
	int rs1 = ConsultationResultats.nbNuls("all stars", "senior",1);
	assert rs1 == 0 : "nb nul 1";
	int rs2 = ConsultationResultats.nbNuls("csp", "senior",1);
	assert rs2 == 1 : "nb nuls 2";
    }

    void testFeuilleDeMatch() throws SQLException{
	ResultSet rs = ConsultationResultats.feuilleDeMatch("all stars", 1, "SENIOR", "csp", 1, "SENIOR", 1, 2);
	rs.next();
	assert rs.getString(1).equals("GOMIS") : "nom";
	assert rs.getString(2).equals("JOSEPH") : "prenom";
	assert rs.getInt(3) == 1 : "eq id";
	assert rs.getInt(4) == 23 : "pts";
	assert rs.getInt(5) == 2 : "fautes";
	rs.next();
	assert rs.getString(1).equals("MOERMAN") : "nom";
	assert rs.getString(2).equals("ADRIEN") : "prenom";
	assert rs.getInt(3) == 1 : "eq id";
	assert rs.getInt(4) == 20 : "pts";
	assert rs.getInt(5) == 3 : "fautes";
	rs.next();
	assert rs.getString(1).equals("WESTERMAN") : "nom";
	assert rs.getString(2).equals("LEO") : "prenom";
	assert rs.getInt(3) == 1 : "eq id";
	assert rs.getInt(4) == 17 : "pts";
	assert rs.getInt(5) == 2 : "fautes";
	rs.next();
	assert rs.getString(1).equals("REMPLACANT") : "nom";
	assert rs.getString(2).equals("CSP") : "prenom";
	assert rs.getInt(3) == 1 : "eq id";
	assert rs.getInt(4) == 0 : "pts";
	assert rs.getInt(5) == 0 : "fautes";
	rs.next();
	assert rs.getString(1).equals("JORDAN") : "nom";
	assert rs.getString(2).equals("MICHAEL") : "prenom";
	assert rs.getInt(3) == 4 : "eq id";
	assert rs.getInt(4) == 39 : "pts";
	assert rs.getInt(5) == 2 : "fautes";
	rs.next();
	assert rs.getString(1).equals("JOHNSON") : "nom";
	assert rs.getString(2).equals("MAGIC") : "prenom";
	assert rs.getInt(3) == 4 : "eq id";
	assert rs.getInt(4) == 23 : "pts";
	assert rs.getInt(5) == 3 : "fautes";
	rs.next();
	assert rs.getString(1).equals("LEBRON") : "nom";
	assert rs.getString(2).equals("JAMES") : "prenom";
	assert rs.getInt(3) == 4 : "eq id";
	assert rs.getInt(4) == 20 : "pts";
	assert rs.getInt(5) == 3 : "fautes";
	rs.close();
	//ResultSet rs1 = ConsultationResultats.feuilleDeMatch("all stars", 1, "csp", 1, "SENIOR", "2015-01-12");
	//Toolkit.printResultSet(rs1);
    }
}
