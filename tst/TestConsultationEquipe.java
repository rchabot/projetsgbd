package federationbasket.tst;

import java.sql.*;
import federationbasket.src.*;

public class TestConsultationEquipe{

    void testJoueursEquipe() throws SQLException {
	ResultSet rs = ConsultationEquipe.joueursEquipe("CSP", "SENIOR",1);
	//Toolkit.printResultSet(rs);
	rs.close();
    }   

    void testEntraineursEquipe() throws SQLException {
	ResultSet rs = ConsultationEquipe.entraineursEquipe("CSP", "SENIOR",1);
	//Toolkit.printResultSet(rs);
	rs.close();
    }   

    void testInfoEquipe() throws SQLException {
	ResultSet rs = ConsultationEquipe.infoEquipe("CSP", "SENIOR",1);
	//Toolkit.printResultSet(rs);
	rs.close();
    }   

    void testResultatsEquipe1() throws SQLException{
	ResultSet rs = ConsultationEquipe.resultatsEquipe("All Stars", "SENIOR", 1, 1, 1);
	rs.next();
	assert rs.getString(1).equals("LAKERS") : "id eqA";
	assert rs.getInt(3) == 75: "score eqA";
	assert rs.getInt(4) == 97: "score eqB";
	assert rs.getString(5).equals("ALL STARS") : "id eqB";
	rs.close();
    }

    void testResultatsEquipe2() throws SQLException{
	ResultSet rs = ConsultationEquipe.resultatsEquipe("LAkERS", "SENIOR", 1, "2015-01-13");
	rs.next();
	/*
	assert rs.getString(1).equals("LAKERS") : "id eqA";
	assert rs.getInt(3) == 70: "score eqA";
	assert rs.getInt(4) == 70: "score eqB";
	assert rs.getString(5).equals("CSP") : "id eqB";
	rs.close();
	*/
    }

    void testResultatsEquipe3() throws SQLException{
	ResultSet rs = ConsultationEquipe.resultatsEquipe("LAkERS", "SENIOR", 1, 1);
	/*
	rs.next();
	assert rs.getStrin(1).equals("LAKERS") == 3 : "id eqA";
	assert rs.getInt(2) == 75: "score eqA";
	assert rs.getInt(3) == 97: "score eqB";
	assert rs.getInt(4) == 4 : "id eqB";
	rs.next();
	assert rs.getInt(1) == 3 : "id eqA";
	assert rs.getInt(2) == 70: "score eqA";
	assert rs.getInt(3) == 70: "score eqB";
	assert rs.getInt(4) == 1 : "id eqB";
	rs.next();
	assert rs.getInt(1) == 2 : "id eqA";
	assert rs.getInt(2) == 80: "score eqA";
	assert rs.getInt(3) == 59: "score eqB";
	assert rs.getInt(4) == 3 : "id eqB";
	rs.close();
	*/
    }

}
