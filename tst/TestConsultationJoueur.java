package federationbasket.tst;

import java.sql.*;
import federationbasket.src.*;

public class TestConsultationJoueur {

    void testInfoJoueur1() throws SQLException{
	ResultSet rs = ConsultationJoueur.infoJoueur("AAAA0001");
	rs.next();
	assert rs.getInt(1) == 4 : "Mauvais id";
	assert rs.getString(2).equals("AAAA0001") : "Mauvais num licence";
	assert rs.getInt(3) == 1 : "Mauvais num équipe";
	rs.close();
    }

    void testInfoJoueur2() throws SQLException{
	ResultSet rs = ConsultationJoueur.infoJoueur("PARKER", "TONY");
	rs.next();
	assert rs.getInt(1) == 10 : "ID";
	assert rs.getString(2).equals("BBBB0001") : "NUM_LICENCE";
	assert rs.getInt(3) == 2 : "EQUIPE";
	rs.close();
    }

    void testCumulPointsSaison() throws SQLException{
	
    }

    void testCumulPointsCarriere() throws SQLException{

    }

    void testCumulFautes() throws SQLException{
    
    }
}
