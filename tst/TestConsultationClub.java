package federationbasket.tst;

import java.sql.*;
import federationbasket.src.*;

public class TestConsultationClub {
    
    void testInfoClub() throws SQLException{
	ResultSet rs = ConsultationClub.infoClub("CSP");
	rs.next();
	assert rs.getInt(1) == 1 : "Mauvais id";
	assert rs.getString(2).equals("CSP") : "Mauvais nom";
	assert rs.getString(3).equals("BEAUBLANC") : "Mauvaise adresse";
	assert rs.getString(4).equals("0678985867") : "Mauvais tel";
	rs.close();
    }    
	
    void testBureauClub() throws SQLException{
	ResultSet rs = ConsultationClub.bureauClub("CSP");
	rs.next();
	assert rs.getString(1).equals("FRED") : "fonction";
	assert rs.getString(2).equals("FORTE") : "NOM";
	assert rs.getString(3).equals("PRESIDENT") : "prenom";
	rs.close();
    }
	
    
}
