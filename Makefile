compile-src:
	javac -d build src/*.java

compile-GUI:
	javac -d build/ -cp build GUI/*.java

compile-tst:
	javac -d build-tst -cp build tst/*.java

create-database:
	java -cp build:lib/mysql-connector-java-5.1.34-bin.jar federationbasket.src.CreateDatabase

drop-database:
	java -cp build:lib/mysql-connector-java-5.1.34-bin.jar federationbasket.src.DropDatabase

exe-tst:
	java -ea -cp build-tst:build:lib/mysql-connector-java-5.1.34-bin.jar federationbasket.tst.LancerTests

exe-GUI:
	java -cp build:lib/mysql-connector-java-5.1.34-bin.jar federationbasket.GUI.Fenetre

clean:
	rm -r build/*
	rm -r build-tst/*
