/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package federationbasket.GUI;

import javax.swing.*;
import java.awt.event.*;

public class Accueil extends JPanel{
    private Fenetre fenetre;
    private JLabel Titre;
    private JButton consultation;
    private JButton jButton1;
    private JLabel jLabel1;
    private JButton maj;
    
    public Accueil(Fenetre fenetre) {
	this.fenetre=fenetre;
        initComponents();
    }

  
    private void initComponents() {

        Titre = new JLabel();
        consultation = new JButton();
        jButton1 = new JButton();
        maj = new JButton();
        jLabel1 = new JLabel();

       

        Titre.setText("Federation sportive de basket-ball");

        consultation.setText("consultation");
        consultation.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
		    consultationActionPerformed(evt);
		}
	    });

        jButton1.setText("statistiques");
        jButton1.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
		    jButton1ActionPerformed(evt);
		}
	    });

        maj.setText("Mise à jour");
        maj.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
		    majActionPerformed(evt);
		}
	    });

        jLabel1.setIcon(new ImageIcon("basket.jpg")); 
        //jLabel1.setText("jLabel1");

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
				  layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				  .addGroup(layout.createSequentialGroup()
					    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						      .addGroup(layout.createSequentialGroup()
								.addGap(40, 40, 40)
								.addComponent(consultation)
								.addGap(81, 81, 81)
								.addComponent(jButton1)
								.addGap(77, 77, 77)
								.addComponent(maj))
						      .addGroup(layout.createSequentialGroup()
								.addGap(169, 169, 169)
								.addComponent(Titre, GroupLayout.PREFERRED_SIZE, 261, GroupLayout.PREFERRED_SIZE))
						      .addGroup(layout.createSequentialGroup()
								.addGap(143, 143, 143)
								.addComponent(jLabel1, GroupLayout.PREFERRED_SIZE, 319, GroupLayout.PREFERRED_SIZE)))
					    .addContainerGap(113, Short.MAX_VALUE))
				  );
        layout.setVerticalGroup(
				layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					  .addGap(22, 22, 22)
					  .addComponent(Titre, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
					  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					  .addComponent(jLabel1)
					  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
					  .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						    .addComponent(consultation)
						    .addComponent(jButton1)
						    .addComponent(maj))
					  .addGap(85, 85, 85))
				);

      
    }                      

    private void consultationActionPerformed(ActionEvent evt) { 
	fenetre.setConsultation();                                            
       
    }                                            

    private void jButton1ActionPerformed(ActionEvent evt) {                   
        fenetre.setStatistiques();	
    }                                        

    private void majActionPerformed(ActionEvent evt) {
        fenetre.setMaj(); 
    }
}
