package federationbasket.GUI;

import federationbasket.src.*;

import java.sql.*;

public class Club extends javax.swing.JDialog {

    public Club(java.awt.Frame parent, String title,boolean modal) {
        super(parent,title, modal);
        initComponents();
    }

  
 
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        Nom = new javax.swing.JLabel();
        EditNom = new javax.swing.JTextField();
        Adresse = new javax.swing.JLabel();
        EditAdresse = new javax.swing.JTextField();
        Telephone = new javax.swing.JLabel();
        EditTelephone = new javax.swing.JTextField();
        Valider = new javax.swing.JButton();
        Annuler = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Veuillez remplir les champs ci-dessous:");

        Nom.setText("Nom:");

        Adresse.setText("Adresse(*):");

        Telephone.setText("Telephone(*):");

        Valider.setText("Valider");
        Valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ValiderActionPerformed(evt);
            }
        });

        Annuler.setText("Annuler");
        Annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnnulerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Nom)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(EditNom, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(56, 56, 56)
                        .addComponent(Adresse)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(EditAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Telephone)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Valider)
                                .addGap(64, 64, 64)
                                .addComponent(Annuler))
                            .addComponent(EditTelephone, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(183, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Nom)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(EditNom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Adresse)
                        .addComponent(EditAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Telephone)
                    .addComponent(EditTelephone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 119, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Valider)
                    .addComponent(Annuler))
                .addGap(46, 46, 46))
        );

        pack();
    }


    private void AnnulerActionPerformed(java.awt.event.ActionEvent evt) {                                        
       this.setVisible(false);
    }                                       

    private void ValiderActionPerformed(java.awt.event.ActionEvent evt) {   
	String nom=EditNom.getText(), adresse=EditAdresse.getText(), telephone=EditTelephone.getText();
	if (adresse.isEmpty())
	    adresse=null;
	if(telephone.isEmpty())
	    telephone=null;
	try{
	    MAJ.insertClub(nom,adresse,telephone); }
	catch(SQLException e) {e.printStackTrace(); } 
	this.setVisible(false);
      
        
    }                                       



    // Variables declaration - do not modify                     
    private javax.swing.JLabel Adresse;
    private javax.swing.JButton Annuler;
    private javax.swing.JTextField EditAdresse;
    private javax.swing.JTextField EditNom;
    private javax.swing.JTextField EditTelephone;
    private javax.swing.JLabel Nom;
    private javax.swing.JLabel Telephone;
    private javax.swing.JButton Valider;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration                   
}
