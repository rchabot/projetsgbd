package federationbasket.GUI;

import federationbasket.src.*;

import javax.swing.*;
import java.util.*;
import java.sql.*;

public class Equipe extends javax.swing.JDialog {

   
    public Equipe(java.awt.Frame parent,String title, boolean modal) {
        super(parent,title, modal);
        initComponents();
    }

                       
     private void initComponents() {

        champ = new javax.swing.JLabel();
	num = new javax.swing.JLabel();
	EditNum = new javax.swing.JTextField();
        Club = new javax.swing.JLabel();
        ListeClubs = new javax.swing.JComboBox();
        Categorie = new javax.swing.JLabel();
        ListeCategories = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        champ.setText("Veuillez remplir les champs ci-dessous:");

       

       

        Club.setText("Club:");

	//  ListeClubs.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeClubs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

		try{
		    ListeClubsActionPerformed(evt);}
		catch(SQLException e){}
            }
        });

        Categorie.setText("Categorie:");

	//   ListeCategories.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeCategories.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeCategoriesActionPerformed(evt);}
catch(SQLException e){}
            }
        });

        jButton1.setText("Valider");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Annuler");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

	num.setText("numero:");

   javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(champ)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Club)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ListeClubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(Categorie)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ListeCategories, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)
                                .addComponent(num)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(EditNum, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(jButton1)
                        .addGap(52, 52, 52)
                        .addComponent(jButton2)))
                .addContainerGap(200, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(champ)
                .addGap(86, 86, 86)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Club)
                    .addComponent(ListeClubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Categorie)
                    .addComponent(ListeCategories, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(num)
                    .addComponent(EditNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 112, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addGap(53, 53, 53))
        );


        pack();
    }
   
    
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        
	String nomCategorie=ListeCategories.getSelectedItem().toString(), nomClub=ListeClubs.getSelectedItem().toString();
int  numEquipe=Integer.parseInt(EditNum.getText());
    try{
	MAJ.insertEquipe(numEquipe,nomClub,nomCategorie);
}  catch(SQLException e){e.printStackTrace();}
    this.setVisible(false);

    }      

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {                                         
	this.setVisible(false);
    }    


    private void ListeClubsActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {            /*	String club=ListeClubs.getSelectedItem().toString();
	Vector<String> ListCategories=new Vector<String>();
	ResultSet rs=Toolkit.getNomsCategorie(club);
	while(rs.next()){
	    ListCategories.add(rs.getString(1));
	}
	changeListeCategories(ListCategories);*/

    }  


    private void ListeCategoriesActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {                                                
	/*	String categorie=ListeCategories.getSelectedItem().toString();
	String club=ListeClubs.getSelectedItem().toString();
	Vector<String> ListNums=new Vector<String>();
	ResultSet rs=Toolkit.getNumsEquipe(club,categorie);    
	while(rs.next()){
	    ListNums.add(rs.getString(1));
	}
	changeListeNums(ListNums);*/  
    }
    

 public void changeListeClubs(Vector<String> v){
	ListeClubs.setModel(new DefaultComboBoxModel(v));

    }   

 public void changeListeCategories(Vector<String> v){
	ListeCategories.setModel(new DefaultComboBoxModel(v));

    }   

 



    // Variables declaration - do not modify                     
    private javax.swing.JLabel Categorie;
    private javax.swing.JLabel Club;
    private javax.swing.JComboBox ListeCategories;
    private javax.swing.JComboBox ListeClubs;
    private javax.swing.JTextField EditNum;
    private javax.swing.JLabel champ;
    private javax.swing.JLabel num;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    // End of variables declaration                   
}
