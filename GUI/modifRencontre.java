package federationbasket.GUI;

import federationbasket.src.*;

import javax.swing.*;
import java.util.*;
import java.sql.*;



public class modifRencontre extends javax.swing.JDialog {

 
    public modifRencontre(java.awt.Frame parent,String title, boolean modal) {
        super(parent,title, modal);
        initComponents();
    }

                             
    private void initComponents() {

        champs = new javax.swing.JLabel();
        EquipeA = new javax.swing.JLabel();
        CLubA = new javax.swing.JLabel();
        ListeCLubsA = new javax.swing.JComboBox();
        CategorieA = new javax.swing.JLabel();
        ListeCategoriesA = new javax.swing.JComboBox();
        NumeroA = new javax.swing.JLabel();
        ListeNumsA = new javax.swing.JComboBox();
        EquipeB = new javax.swing.JLabel();
        ClubB = new javax.swing.JLabel();
        ListeCLubsB = new javax.swing.JComboBox();
        CategorieB = new javax.swing.JLabel();
        ListeCategoriesB = new javax.swing.JComboBox();
        NumeroB = new javax.swing.JLabel();
        ListeNumsB = new javax.swing.JComboBox();
        Date = new javax.swing.JLabel();
        EditDate = new javax.swing.JTextField();
        Valider = new javax.swing.JButton();
        Annuler = new javax.swing.JButton();
        journee = new javax.swing.JLabel();
        EditJournee = new javax.swing.JTextField();
        saison = new javax.swing.JLabel();
        EditSaison = new javax.swing.JTextField();
        scoreA = new javax.swing.JLabel();
        EditScoreA = new javax.swing.JTextField();
        scoreB = new javax.swing.JLabel();
        EditScoreB = new javax.swing.JTextField();
        choix = new javax.swing.JLabel();
        modifScore = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        champs.setText("Veuillez modifier les champs souhaités:");

        EquipeA.setText("Equipe A");

        CLubA.setText("Club:");

        ListeCLubsA.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeCLubsA.addActionListener(new java.awt.event.ActionListener() {
           public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeCLubsAActionPerformed(evt);}
		catch(SQLException e){}
            }
        });

        CategorieA.setText("Categorie:");

        ListeCategoriesA.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeCategoriesA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeCategoriesAActionPerformed(evt);} catch(SQLException e){}
            }
	    });

        NumeroA.setText("Numero(*):");

        ListeNumsA.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
       

        EquipeB.setText("Equipe B");

        ClubB.setText("Club:");

        ListeCLubsB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeCLubsB.addActionListener(new java.awt.event.ActionListener() {
           public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeCLubsBActionPerformed(evt);}
		catch(SQLException e){}
            }
        });

        CategorieB.setText("Categorie:");

        ListeCategoriesB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeCategoriesB.addActionListener(new java.awt.event.ActionListener() {
           public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeCategoriesBActionPerformed(evt);}
		catch(SQLException e){}
            }
        });

        NumeroB.setText("Numero(*):");

        ListeNumsB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        

        Date.setText("Date de la rencontre:");

       

        Valider.setText("Valider");
        Valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ValiderActionPerformed(evt);
            }
        });

        Annuler.setText("Annuler");
        Annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnnulerActionPerformed(evt);
            }
        });

        journee.setText("No de journee:");

        saison.setText("No de saison:");

        scoreA.setText("Score equipe A(*):");

        scoreB.setText("Score equipe B(*):");

        choix.setText("choix de la rencontre");

        modifScore.setText("Modification des scores ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(champs)
                            .addComponent(EquipeA)
                            .addComponent(EquipeB)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(ClubB)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(ListeCLubsB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(32, 32, 32)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(CategorieB)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(ListeCategoriesB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(CategorieA)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(ListeCategoriesA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(choix)))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(CLubA)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(ListeCLubsA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(42, 42, 42)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(NumeroB)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(ListeNumsB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(NumeroA)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(ListeNumsA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(saison)
                                                    .addGap(4, 4, 4)
                                                    .addComponent(EditSaison, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(Date))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(EditDate, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(27, 27, 27))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(scoreA)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(EditScoreA, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(62, 62, 62)))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(journee)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(EditJournee, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(0, 143, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(scoreB)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(EditScoreB, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(101, 101, 101)))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(123, 123, 123)
                        .addComponent(Valider)
                        .addGap(59, 59, 59)
                        .addComponent(Annuler))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(modifScore)))
                .addContainerGap(37, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(champs)
                .addGap(18, 18, 18)
                .addComponent(choix)
                .addGap(22, 22, 22)
                .addComponent(EquipeA)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(CLubA)
                        .addComponent(ListeCLubsA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(CategorieA)
                        .addComponent(ListeCategoriesA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(NumeroA)
                        .addComponent(ListeNumsA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(24, 24, 24)
                .addComponent(EquipeB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ClubB)
                    .addComponent(ListeCLubsB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CategorieB)
                    .addComponent(ListeCategoriesB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(NumeroB)
                    .addComponent(ListeNumsB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Date)
                    .addComponent(EditDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(journee)
                    .addComponent(EditJournee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saison)
                    .addComponent(EditSaison, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                .addComponent(modifScore)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(EditScoreB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(scoreB)
                    .addComponent(EditScoreA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(scoreA))
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Valider)
                    .addComponent(Annuler))
                .addGap(29, 29, 29))
        );

        pack();
    }                  

                                   

    private void ListeCategoriesAActionPerformed(java.awt.event.ActionEvent evt)  throws SQLException    {                                            
       	String categorie=ListeCategoriesA.getSelectedItem().toString();
	String club=ListeCLubsA.getSelectedItem().toString();
	Vector<String> ListNums=new Vector<String>();
	ResultSet rs=Toolkit.getNumsEquipe(club,categorie);    
	while(rs.next()){
	    ListNums.add(rs.getString(1));
	}
	changeListeNumsA(ListNums);  
	}                                                

    private void ValiderActionPerformed(java.awt.event.ActionEvent evt) { 
	String date=EditDate.getText(), clubA=ListeCLubsA.getSelectedItem().toString(), categorieA=ListeCategoriesA.getSelectedItem().toString(),clubB=ListeCLubsB.getSelectedItem().toString(), categorieB=ListeCategoriesB.getSelectedItem().toString(), scoreA=EditScoreA.getText(),scoreB=EditScoreB.getText();
	int numEquipeA=Integer.parseInt(ListeNumsA.getSelectedItem().toString()), numEquipeB=Integer.parseInt(ListeNumsB.getSelectedItem().toString()), numJournee=Integer.parseInt(EditJournee.getText()),numSaison=Integer.parseInt(EditSaison.getText());
	try{
	int equipeAId=Toolkit.getEquipeId(clubA,categorieA,numEquipeA);
	int equipeBId=Toolkit.getEquipeId(clubB,categorieB,numEquipeB);                      int rencontreId=Toolkit.getRencontreId(equipeAId,equipeBId,date,numJournee,numSaison);
	
	Vector<String> values=new Vector<String>();
	Vector<String> column=new Vector<String>();    
	if(!scoreA.isEmpty()){
	    values.add(scoreA);
	column.add("SCORE_EQUIPE_A");
	}

	if(!scoreB.isEmpty()){
	    values.add(scoreB);
	column.add("SCORE_EQUIPE_B");
	}

	MAJ.update("RENCONTRE",rencontreId,column,values);

	}catch(SQLException e){ e.printStackTrace();}

	this.setVisible(false);
      
    }                                       

    private void AnnulerActionPerformed(java.awt.event.ActionEvent evt) {                                        
        this.setVisible(false);
    }                                       

    private void ListeCLubsAActionPerformed(java.awt.event.ActionEvent evt) 
       throws SQLException {
	String club=ListeCLubsA.getSelectedItem().toString();
	Vector<String> ListCategories=new Vector<String>();
	ResultSet rs=Toolkit.getNomsCategorie(club);
	while(rs.next()){
	    ListCategories.add(rs.getString(1));
	}
	changeListeCategoriesA(ListCategories);                       
       
    }                                           

                                          

    private void ListeCLubsBActionPerformed(java.awt.event.ActionEvent evt) throws SQLException {
	String club=ListeCLubsB.getSelectedItem().toString();
	Vector<String> ListCategories=new Vector<String>();
	ResultSet rs=Toolkit.getNomsCategorie(club);
	while(rs.next()){
	    ListCategories.add(rs.getString(1));
	}
	changeListeCategoriesB(ListCategories);         
    }                                           

    private void ListeCategoriesBActionPerformed(java.awt.event.ActionEvent evt) throws SQLException       {                           
       	String categorie=ListeCategoriesB.getSelectedItem().toString();
	String club=ListeCLubsB.getSelectedItem().toString();
	Vector<String> ListNums=new Vector<String>();
	ResultSet rs=Toolkit.getNumsEquipe(club,categorie);    
	while(rs.next()){
	    ListNums.add(rs.getString(1));
	}
	changeListeNumsB(ListNums);
    }                                                
                  


public void changeListeClubs(Vector<String> v){
	ListeCLubsA.setModel(new DefaultComboBoxModel(v));
        ListeCLubsB.setModel(new DefaultComboBoxModel(v));

    }   



 public void changeListeCategoriesA(Vector<String> v){
     ListeCategoriesA.setModel(new DefaultComboBoxModel(v));
 }   

 public void changeListeNumsA(Vector<String> v){
     ListeNumsA.setModel(new DefaultComboBoxModel(v));
 }   


 public void changeListeCategoriesB(Vector<String> v){
     ListeCategoriesB.setModel(new DefaultComboBoxModel(v));
 }   

 public void changeListeNumsB(Vector<String> v){
     ListeNumsB.setModel(new DefaultComboBoxModel(v));
 }   





    // Variables declaration - do not modify                     
    private javax.swing.JButton Annuler;
    private javax.swing.JLabel CLubA;
    private javax.swing.JLabel CategorieA;
    private javax.swing.JLabel CategorieB;
    private javax.swing.JLabel ClubB;
    private javax.swing.JLabel Date;
    private javax.swing.JTextField EditDate;
    private javax.swing.JTextField EditJournee;
    private javax.swing.JTextField EditSaison;
    private javax.swing.JTextField EditScoreA;
    private javax.swing.JTextField EditScoreB;
    private javax.swing.JLabel EquipeA;
    private javax.swing.JLabel EquipeB;
    private javax.swing.JComboBox ListeCLubsA;
    private javax.swing.JComboBox ListeCLubsB;
    private javax.swing.JComboBox ListeCategoriesA;
    private javax.swing.JComboBox ListeCategoriesB;
    private javax.swing.JComboBox ListeNumsA;
    private javax.swing.JComboBox ListeNumsB;
    private javax.swing.JLabel NumeroA;
    private javax.swing.JLabel NumeroB;
    private javax.swing.JButton Valider;
    private javax.swing.JLabel champs;
    private javax.swing.JLabel choix;
    private javax.swing.JLabel journee;
    private javax.swing.JLabel modifScore;
    private javax.swing.JLabel saison;
    private javax.swing.JLabel scoreA;
    private javax.swing.JLabel scoreB;
    // End of variables declaration                   
}
