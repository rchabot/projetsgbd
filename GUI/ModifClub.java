package federationbasket.GUI;

import federationbasket.src.*;

import javax.swing.*;
import java.util.*;
import java.sql.*;


public class ModifClub extends javax.swing.JDialog {

    
    public ModifClub(java.awt.Frame parent,String title, boolean modal) {
        super(parent,title, modal);
        initComponents();
    }

       
    private void initComponents() {

        champs = new javax.swing.JLabel();
        Nom = new javax.swing.JLabel();
        Adresse = new javax.swing.JLabel();
        EditAdresse = new javax.swing.JTextField();
        Telephone = new javax.swing.JLabel();
        EditTelephone = new javax.swing.JTextField();
        Valider = new javax.swing.JButton();
        Annuler = new javax.swing.JButton();
        listClub = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        champs.setText("Veuillez modifier les champs souhaités");

        Nom.setText("Nom:");

        Adresse.setText("Adresse:");

        Telephone.setText("Telephone:");

        Valider.setText("Valider");
        Valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ValiderActionPerformed(evt);
            }
        });

        Annuler.setText("Annuler");
        Annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnnulerActionPerformed(evt);
            }
        });

        listClub.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(champs)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Nom)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(listClub, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(72, 72, 72)
                        .addComponent(Adresse)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(EditAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Telephone)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Valider)
                                .addGap(64, 64, 64)
                                .addComponent(Annuler))
                            .addComponent(EditTelephone, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(183, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(champs)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Nom)
                        .addComponent(listClub, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Adresse)
                        .addComponent(EditAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Telephone)
                    .addComponent(EditTelephone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 119, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Valider)
                    .addComponent(Annuler))
                .addGap(46, 46, 46))
        );

        pack();
    }    



    private void AnnulerActionPerformed(java.awt.event.ActionEvent evt) {                                        
      this.setVisible(false);
    }                                       

    private void ValiderActionPerformed(java.awt.event.ActionEvent evt) {             
      try{      int clubId=Toolkit.getIdFromNom("CLUB",listClub.getSelectedItem().toString());   
	String adresse=EditAdresse.getText(),telephone=EditTelephone.getText();
	Vector<String> values=new Vector<String>();
	Vector<String> column=new Vector<String>();

	if (!adresse.isEmpty()){
	    values.add("'"+adresse+"'");
	    column.add("CLUB_ADRESSE");
	}
	if(!telephone.isEmpty()){
	    values.add("'"+telephone+"'");
	    column.add("CLUB_TELEPHONE");
	}
	//	System.out.println(values.get(0));
	//	System.out.println(values.get(1));
		MAJ.update("CLUB",clubId,column,values);
		this.setVisible(false);
	} catch(SQLException e) {e.printStackTrace();}
 
    }                                       


 public void changeListeClubs(Vector<String> v){
	listClub.setModel(new DefaultComboBoxModel(v));

    }   


  

    // Variables declaration - do not modify                     
    private javax.swing.JLabel Adresse;
    private javax.swing.JButton Annuler;
    private javax.swing.JTextField EditAdresse;
    private javax.swing.JTextField EditTelephone;
    private javax.swing.JLabel Nom;
    private javax.swing.JLabel Telephone;
    private javax.swing.JButton Valider;
    private javax.swing.JLabel champs;
    private javax.swing.JComboBox listClub;
    // End of variables declaration                   
}
