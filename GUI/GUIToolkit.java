package federationbasket.GUI;

import federationbasket.src.*;
import java.sql.*;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class GUIToolkit {
    public static void setBoxCategorie(JComboBox boxCat){
	try {
	    ResultSet rs = Toolkit.getNomsCategorie();
	    Vector<String> items = new Vector<String>();
	    int i = 0;
	    while (rs.next()){
		items.add(rs.getString(1));
	    }
	    boxCat.setModel(new DefaultComboBoxModel(items));
	}catch(SQLException e){
	    e.printStackTrace();
	}
    }

    public static void setBoxSaison(JComboBox boxSaison){
	try {
	    int nbSaison = Toolkit.nbMaxSaison();
	    Vector<String> items = new Vector<String>();
	    for (int i = 1; i <= nbSaison; i++)
		items.add(String.valueOf(i));
	    boxSaison.setModel(new DefaultComboBoxModel(items));
	}catch(SQLException e){
	    e.printStackTrace();
	}
    }

    public static void setBoxJournee(JComboBox boxSaison){
	try {
	    int nbSaison = Toolkit.nbMaxJournee();
	    Vector<String> items = new Vector<String>();
	    for (int i = 1; i <= nbSaison; i++)
		items.add(String.valueOf(i));
	    boxSaison.setModel(new DefaultComboBoxModel(items));
	}catch(SQLException e){
	    e.printStackTrace();
	}
    }
    
    
    public static void setBoxClub(JComboBox boxClub){
        try {
            ResultSet rs = Toolkit.getNomsClub();
            Vector<String> items = new Vector<String>();
            int i = 0;
            while(rs.next())
                items.add(rs.getString(1));
            boxClub.setModel(new DefaultComboBoxModel(items));
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    
    public static void changerComboBox(JComboBox box, Vector<String> vect){
        box.setModel(new DefaultComboBoxModel(vect));
    }

    public static void setBoxJoueursClub(JComboBox boxJoueurs, String nomClub){
	try {
	    ResultSet rs = federationbasket.src.ConsultationClub.listeJoueurs(nomClub);
	    Vector<String> items = new Vector<String>();
            int i = 0;
            while(rs.next())
                items.add(rs.getString(3) + " " + rs.getString(2));
            boxJoueurs.setModel(new DefaultComboBoxModel(items));
	    rs.close();
	}catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static int getJoueurId(ResultSet rsJoueur, int numLigne){
	int id = 0;
	try {
	    int i = 0;
	    while(rsJoueur.next() && i != numLigne)
		i++;
	    id = rsJoueur.getInt(1);
	    rsJoueur.close();
	} catch (SQLException e){
	    e.printStackTrace();
	}
        return id;
    }
    
    public static void remplirTable(JTable table, ResultSet rs)throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
	int nbCol = rsmd.getColumnCount();
	int nbRow = Toolkit.nbRow(rs);
	String[] colNames = new String[nbCol];
	for (int i=0; i < nbCol; i++){
            colNames[i] = rsmd.getColumnLabel(i+1);
	}
	String[][] data = new String[nbRow][nbCol];
	int j=0;
	while(rs.next()){
            for (int i=0; i < nbCol; i++){
                data[j][i] = rs.getString(i+1);
            }
            j++;
	}
	table.setModel(new DefaultTableModel(data, colNames));
    }
    
}
