package federationbasket.GUI;

import federationbasket.src.*;

import javax.swing.*;
import java.util.*;
import java.sql.*;


public class PersonneMorale extends javax.swing.JDialog {

    public PersonneMorale(java.awt.Frame parent,String title, boolean modal) {
        super(parent,title, modal);
        initComponents();
    }

                           
    private void initComponents() {

        Nom = new javax.swing.JLabel();
        EditNom = new javax.swing.JTextField();
        Prenom = new javax.swing.JLabel();
        EditPrenom = new javax.swing.JTextField();
	Club = new javax.swing.JLabel();
        ListeClubs = new javax.swing.JComboBox();
        Adresse = new javax.swing.JLabel();
        EditAdresse = new javax.swing.JTextField();
        Naissance = new javax.swing.JLabel();
        EditNaissance = new javax.swing.JTextField();
        entree = new javax.swing.JLabel();
        EditEntree = new javax.swing.JTextField();
        valider = new javax.swing.JButton();
        annuler = new javax.swing.JButton();
        champs = new javax.swing.JLabel();
        fonction = new javax.swing.JLabel();
        editFonction = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        Nom.setText("Nom:");

        EditNom.setText(" ");
       
        Prenom.setText("Prenom:");

       

        Adresse.setText("Adresse(*):");

       

        Naissance.setText("date de naissance(*):");

        entree.setText("date d'entree(*):");

        valider.setText("Valider");
        valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validerActionPerformed(evt);
            }
        });

        annuler.setText("Annuler");
        annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerActionPerformed(evt);
            }
        });

        champs.setText("Veuillez remplir les champs ci-dessous:");

        fonction.setText("Fonction:");
	Club.setText("Club:");
       
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(Nom)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(EditNom, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(39, 39, 39)
                                .addComponent(Prenom)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(EditPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(Adresse)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(EditAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(entree))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Club)
                                .addGap(4, 4, 4)
                                .addComponent(ListeClubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(fonction)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addComponent(Naissance)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(EditNaissance, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(editFonction, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(EditEntree, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(127, 127, 127)
                        .addComponent(valider)
                        .addGap(47, 47, 47)
                        .addComponent(annuler))
                    .addComponent(champs))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(champs)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Nom)
                    .addComponent(EditNom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Prenom)
                    .addComponent(EditPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Naissance)
                    .addComponent(EditNaissance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Adresse)
                    .addComponent(EditAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(entree)
                    .addComponent(EditEntree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fonction)
                    .addComponent(editFonction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Club)
                    .addComponent(ListeClubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 96, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valider)
                    .addComponent(annuler))
                .addGap(22, 22, 22))
        );

        pack();
    }            



    private void annulerActionPerformed(java.awt.event.ActionEvent evt) {                       this.setVisible(false);                 
       
    }                                       

    private void validerActionPerformed(java.awt.event.ActionEvent evt) {                    String nom=EditNom.getText(),prenom=EditPrenom.getText(),adresse=EditAdresse.getText(),entree=EditEntree.getText(),naissance=EditNaissance.getText(), fonction=editFonction.getText(),club=ListeClubs.getSelectedItem().toString();                    
        if (entree.isEmpty())
        entree=null;
	if (naissance.isEmpty())
	    naissance=null;
	if (adresse.isEmpty())
	    adresse=null;
	try{	MAJ.insertPersonneMorale(nom,prenom,naissance,adresse,entree,club,fonction);
	}
	catch(SQLException e){e.printStackTrace();
}
	
	this.setVisible(false);

    }                                       

    

    public void changeListeClubs(Vector<String> v){
	ListeClubs.setModel(new DefaultComboBoxModel(v));

    }   

   
    // Variables declaration - do not modify                     
    private javax.swing.JLabel Adresse;
    private JLabel Club;
    private javax.swing.JTextField EditAdresse;
    private javax.swing.JTextField EditEntree;
    private javax.swing.JTextField EditNaissance;
    private javax.swing.JTextField EditNom;
    private javax.swing.JTextField EditPrenom;
    private javax.swing.JLabel Naissance;
    private javax.swing.JLabel Nom;
    private javax.swing.JLabel Prenom;
    private javax.swing.JButton annuler;
    private javax.swing.JLabel champs;
    private JComboBox ListeClubs;
    private javax.swing.JTextField editFonction;
    private javax.swing.JLabel entree;
    private javax.swing.JLabel fonction;
    private javax.swing.JButton valider;
    // End of variables declaration                   
}
