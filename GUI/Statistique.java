package federationbasket.GUI;

import federationbasket.src.*;

import java.util.*;
import java.sql.*;
import javax.swing.*;


public class Statistique extends javax.swing.JDialog {

   
    public Statistique(java.awt.Frame parent, String title,boolean modal) {
        super(parent, title,modal);
        initComponents();
    }

   
   
    private void initComponents() {

        jCheckBox1 = new javax.swing.JCheckBox();
        jComboBox7 = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        points = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        fautes = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        Club1 = new javax.swing.JLabel();
        ListeClub1 = new javax.swing.JComboBox();
        Categorie1 = new javax.swing.JLabel();
        ListeCategorie1 = new javax.swing.JComboBox();
        num1 = new javax.swing.JLabel();
        ListeNum1 = new javax.swing.JComboBox();
        date = new javax.swing.JLabel();
        EditDate = new javax.swing.JTextField();
        journee = new javax.swing.JLabel();
        EditJournee = new javax.swing.JTextField();
        joueur = new javax.swing.JLabel();
        ListeJoueur = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        club2 = new javax.swing.JLabel();
        ListeClub2 = new javax.swing.JComboBox();
        Categorie2 = new javax.swing.JLabel();
        ListeCategorie2 = new javax.swing.JComboBox();
        num2 = new javax.swing.JLabel();
        ListNum2 = new javax.swing.JComboBox();
        valider = new javax.swing.JButton();
        annuler = new javax.swing.JButton();

        jCheckBox1.setText("jCheckBox1");

        jComboBox7.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Veuillez remplir les champs ci-dessous:");

        jLabel4.setText("Nombre de points:");

        jLabel5.setText("Nombre de fautes:");

        jLabel6.setText("Choix de l'equipe");

        Club1.setText("Club:");

        ListeClub1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeClub1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try {
		    ListeClub1ActionPerformed(evt);} catch(SQLException e){}
            }
        });

        Categorie1.setText("Categorie:");

        ListeCategorie1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeCategorie1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeCategorie1ActionPerformed(evt);} catch(SQLException e){}
            }
        });

        num1.setText("numero:");

        ListeNum1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeNum1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeNum1ActionPerformed(evt);} catch(SQLException e){}
            }
        });

        date.setText("Date du match:");

        journee.setText("No de journee:");

        joueur.setText("Joueur:");

        ListeJoueur.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeJoueur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeJoueurActionPerformed(evt);} catch(SQLException e){}
            }
        });

        jLabel13.setText("Equipe adverse");

        club2.setText("Club:");

        ListeClub2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeClub2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try {
		    ListeClub2ActionPerformed(evt);}
		catch(SQLException e){}
            }
        });

        Categorie2.setText("Categorie:");

        ListeCategorie2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeCategorie2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeCategorie2ActionPerformed(evt);}
		catch(SQLException e){}
            }
        });

        num2.setText("numero:");

        ListNum2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListNum2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ListNum2ActionPerformed(evt);
            }
        });

        valider.setText("Valider");
valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    validerActionPerformed(evt);}
		catch(SQLException e){}
            }
        });

        annuler.setText("Annuler");
annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel13))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Club1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ListeClub1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(Categorie1)
                        .addGap(1, 1, 1)
                        .addComponent(ListeCategorie1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(num1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ListeNum1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(club2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ListeClub2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(Categorie2)
                                        .addGap(4, 4, 4)
                                        .addComponent(ListeCategorie2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(num2))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(points, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(44, 44, 44)
                                        .addComponent(jLabel5))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(valider)
                                .addGap(36, 36, 36)
                                .addComponent(annuler)
                                .addGap(46, 46, 46)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(38, 38, 38)
                                .addComponent(fautes, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(ListNum2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(date)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(EditDate, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23)
                        .addComponent(journee)
                        .addGap(4, 4, 4)
                        .addComponent(EditJournee, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(joueur)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ListeJoueur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(65, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(30, 30, 30)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Club1)
                    .addComponent(ListeClub1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Categorie1)
                    .addComponent(ListeCategorie1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(num1)
                    .addComponent(ListeNum1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(date)
                    .addComponent(EditDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(journee)
                    .addComponent(EditJournee, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(joueur)
                    .addComponent(ListeJoueur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(club2)
                    .addComponent(ListeClub2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Categorie2)
                    .addComponent(ListeCategorie2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(num2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ListNum2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(points, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(fautes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valider)
                    .addComponent(annuler))
                .addGap(25, 25, 25))
        );

        pack();
    }

    private void ListeClub1ActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {                           
	String club=ListeClub1.getSelectedItem().toString();
	Vector<String> ListCategories=new Vector<String>();
	ResultSet rs=Toolkit.getNomsCategorie(club);
	while(rs.next()){
	    ListCategories.add(rs.getString(1));
	}
	changeListeCategories1(ListCategories);                       
                       
   
    }                                          

    private void ListeCategorie1ActionPerformed(java.awt.event.ActionEvent evt)throws SQLException { 
	String categorie=ListeCategorie1.getSelectedItem().toString();
	String club=ListeClub1.getSelectedItem().toString();
	Vector<String> ListNums=new Vector<String>();
	ResultSet rs=Toolkit.getNumsEquipe(club,categorie);    
	while(rs.next()){
	    ListNums.add(rs.getString(1));
	}
	changeListeNums1(ListNums); 
        
    }                                               

    private void ListeNum1ActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {  
	String categorie=ListeCategorie1.getSelectedItem().toString();
	String club=ListeClub1.getSelectedItem().toString();
	int numEquipe1=Integer.parseInt(ListeNum1.getSelectedItem().toString());
	Vector<String> ListJoueurs=new Vector<String>();
	    ResultSet rs = federationbasket.src.ConsultationEquipe.joueursEquipe(club,categorie,numEquipe1);

	    while(rs.next()){
		ListJoueurs.add((rs.getString(1)+" "+rs.getString(2)));
		changeListeJoueurs(ListJoueurs);
	

    }
    }
                                        
      
				                                            









    private void ListeClub2ActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {                         
	String  nomCLub1=ListeClub1.getSelectedItem().toString(),categorie1=ListeCategorie1.getSelectedItem().toString(),dateMatch=EditDate.getText();
	int numJournee=Integer.parseInt(EditJournee.getText()),numEquipe1=Integer.parseInt(ListeNum1.getSelectedItem().toString());  	
	String club=ListeClub2.getSelectedItem().toString();
	Vector<String> ListCategories=new Vector<String>();
	try{

	ResultSet rs=Toolkit.getEquipesId(nomCLub1,categorie1,numEquipe1,dateMatch,numJournee);	 
	while(rs.next()){
	    ResultSet rs2=Toolkit.getNomsCategorie(rs.getInt(1),club);
	    while(rs2.next())
	    ListCategories.add(rs2.getString(1));
	}
	}
	catch(SQLException e){ 
	    e.printStackTrace();}
    


	changeListeCategories2(ListCategories);                       
    }
       
                                          

    private void ListeCategorie2ActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {
	String  nomCLub1=ListeClub1.getSelectedItem().toString(),categorie1=ListeCategorie1.getSelectedItem().toString(),dateMatch=EditDate.getText();
	int numJournee=Integer.parseInt(EditJournee.getText()),numEquipe1=Integer.parseInt(ListeNum1.getSelectedItem().toString());  	
	String club=ListeClub2.getSelectedItem().toString();
	String categorie=ListeCategorie2.getSelectedItem().toString();
	Vector<String> ListNums=new Vector<String>();

	ResultSet rs=Toolkit.getEquipesId(nomCLub1,categorie1,numEquipe1,dateMatch,numJournee);	    
	while(rs.next()){
	    ResultSet rs2=Toolkit.getNumEquipes(rs.getInt(1),club,categorie);
	    while(rs2.next())
		ListNums.add(rs2.getString(1));
		    }
	    
	  
	
	changeListeNums2(ListNums);                                
       
    }                                               

    private void ListNum2ActionPerformed(java.awt.event.ActionEvent evt) {                                         
       
    }                                        
    private void ListeJoueurActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {
		String  nomCLub1=ListeClub1.getSelectedItem().toString(),categorie1=ListeCategorie1.getSelectedItem().toString(),dateMatch=EditDate.getText();
		int numJournee=Integer.parseInt(EditJournee.getText()),numEquipe1=Integer.parseInt(ListeNum1.getSelectedItem().toString());
	Vector<String> ListeClubs=new Vector<String>();
	try{	ResultSet rs=Toolkit.getEquipesId(nomCLub1,categorie1,numEquipe1,dateMatch,numJournee);	   
	     while(rs.next()){
			 String nom=Toolkit.getNomClub(rs.getInt(1));
				ListeClubs.add(nom);				
			  }
            
		    } catch(SQLException e){ e.printStackTrace();}
		    changeListeClubs2(ListeClubs);
    }
                                     

private void validerActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {      
  String  nomClub1=ListeClub1.getSelectedItem().toString(),categorie1=ListeCategorie1.getSelectedItem().toString(),dateMatch=EditDate.getText(), nomClub2=ListeClub2.getSelectedItem().toString(),categorie2=ListeCategorie2.getSelectedItem().toString();
  int numJournee=Integer.parseInt(EditJournee.getText()),numEquipe1=Integer.parseInt(ListeNum1.getSelectedItem().toString()),numEquipe2=Integer.parseInt(ListNum2.getSelectedItem().toString()),statsPoints=-1,statsFautes=-1;
    String joueur=ListeJoueur.getSelectedItem().toString(), nom=joueur.substring(0,joueur.indexOf(' ')), prenom=joueur.substring(joueur.indexOf(' ')+1); int rencontreId=0;
    if (!points.getText().isEmpty())
	statsPoints=Integer.parseInt(points.getText());
    if (!fautes.getText().isEmpty())
	statsFautes=Integer.parseInt(fautes.getText());

     

    int equipe1Id=Toolkit.getEquipeId(nomClub1,categorie1,numEquipe1),equipe2Id=Toolkit.getEquipeId(nomClub2,categorie2,numEquipe2),joueurId=Toolkit.getJoueurId(nom,prenom,equipe1Id);

    if(Toolkit.isEquipeA(equipe1Id,dateMatch,numJournee))
	rencontreId=Toolkit.getRencontreId(equipe1Id,equipe2Id,dateMatch,numJournee,1);
    else
	rencontreId=Toolkit.getRencontreId(equipe2Id,equipe1Id,dateMatch,numJournee,1);
    try{	MAJ.insertStatistique(joueurId,rencontreId,statsPoints,statsFautes)     ;
	this.setVisible(false);}  catch(SQLException e){e.printStackTrace();}

                            

        
    }                                       

    private void annulerActionPerformed(java.awt.event.ActionEvent evt) {  
	
	this.setVisible(false);          
        
    }      


public void changeListeClubs(Vector<String> v){
	ListeClub1.setModel(new DefaultComboBoxModel(v));
        ListeClub2.setModel(new DefaultComboBoxModel(v));

    }   

public void changeListeClubs2(Vector<String> v){
	ListeClub2.setModel(new DefaultComboBoxModel(v));
}

public void changeListeCategories1(Vector<String> v){
     ListeCategorie1.setModel(new DefaultComboBoxModel(v));
 }   

 public void changeListeNums1(Vector<String> v){
     ListeNum1.setModel(new DefaultComboBoxModel(v));
 }   


 public void changeListeCategories2(Vector<String> v){
     ListeCategorie2.setModel(new DefaultComboBoxModel(v));
 }   

 public void changeListeNums2(Vector<String> v){
     ListNum2.setModel(new DefaultComboBoxModel(v));
 }   

    public void changeListeJoueurs(Vector<String> v){
ListeJoueur.setModel(new DefaultComboBoxModel(v));
    }

 


    // Variables declaration - do not modify                     
    private javax.swing.JLabel Categorie1;
    private javax.swing.JLabel Categorie2;
    private javax.swing.JLabel Club1;
    private javax.swing.JTextField EditDate;
    private javax.swing.JTextField EditJournee;
    private javax.swing.JComboBox ListNum2;
    private javax.swing.JComboBox ListeCategorie1;
    private javax.swing.JComboBox ListeCategorie2;
    private javax.swing.JComboBox ListeClub1;
    private javax.swing.JComboBox ListeClub2;
    private javax.swing.JComboBox ListeJoueur;
    private javax.swing.JComboBox ListeNum1;
    private javax.swing.JButton annuler;
    private javax.swing.JLabel club2;
    private javax.swing.JLabel date;
    private javax.swing.JTextField fautes;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JComboBox jComboBox7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel joueur;
    private javax.swing.JLabel journee;
    private javax.swing.JLabel num1;
    private javax.swing.JLabel num2;
    private javax.swing.JTextField points;
    private javax.swing.JButton valider;
    // End of variables declaration                   
}
