package federationbasket.GUI;

import federationbasket.src.*;

import javax.swing.*;
import java.util.*;
import java.sql.*;


public class delEquipe extends javax.swing.JDialog {

 
    public delEquipe(java.awt.Frame parent,String title, boolean modal) {
        super(parent,title, modal);
        initComponents();
    }

  
                
    private void initComponents() {

        champ = new javax.swing.JLabel();
        club = new javax.swing.JLabel();
        Valider = new javax.swing.JButton();
        Annuler = new javax.swing.JButton();
        listClub = new javax.swing.JComboBox();
        categorie = new javax.swing.JLabel();
        listCategorie = new javax.swing.JComboBox();
        num = new javax.swing.JLabel();
        listNum = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        champ.setText("Choisissez l'équipe  à supprimer");

        club.setText("Club:");

        Valider.setText("Valider");
        Valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ValiderActionPerformed(evt);
            }
        });

        Annuler.setText("Annuler");
        Annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnnulerActionPerformed(evt);
            }
        });

        listClub.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        listClub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
                listClubActionPerformed(evt);
		} catch(SQLException e) {}
            }
        });

        categorie.setText("Categorie:");

        listCategorie.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        listCategorie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
                listCategorieActionPerformed(evt);
		} catch(SQLException e){}
            }
        });

        num.setText("Numero:");

        listNum.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(champ)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(club)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(listClub, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addComponent(categorie)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(listCategorie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(num)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(listNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(Valider)
                        .addGap(64, 64, 64)
                        .addComponent(Annuler)))
                .addContainerGap(57, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(champ)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(club)
                    .addComponent(listClub, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(categorie)
                    .addComponent(listCategorie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(num)
                    .addComponent(listNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 176, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Valider)
                    .addComponent(Annuler))
                .addGap(46, 46, 46))
        );

        pack();
    }         

    private void AnnulerActionPerformed(java.awt.event.ActionEvent evt) {                                        
      this.setVisible(false);
    }                                       

    private void ValiderActionPerformed(java.awt.event.ActionEvent evt) {                       
	String club=listClub.getSelectedItem().toString(),categorie=listCategorie.getSelectedItem().toString();
	int numEquipe=Integer.parseInt(listNum.getSelectedItem().toString());
               
    try{
	int equipeId=Toolkit.getEquipeId(club,categorie,numEquipe);
	Suppression.supprimerEquipe(equipeId);
	this.setVisible(false);
   
    } catch(SQLException e){e.printStackTrace();}
    }                                       

    private void listClubActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {  
	String club=listClub.getSelectedItem().toString();
	Vector<String> ListCategories=new Vector<String>();
	ResultSet rs=Toolkit.getNomsCategorie(club);
	ListCategories.add("Choix");
	while(rs.next()){
	    ListCategories.add(rs.getString(1));
	}
	changeListeCategories(ListCategories);             
       
    }                                        

    private void listCategorieActionPerformed(java.awt.event.ActionEvent evt)throws SQLException { 	String categorie=listCategorie.getSelectedItem().toString();
	String club=listClub.getSelectedItem().toString();
	Vector<String> ListNums=new Vector<String>();
	ResultSet rs=Toolkit.getNumsEquipe(club,categorie);    
	while(rs.next()){
	    ListNums.add(rs.getString(1));
	}
	changeListeNums(ListNums);                             
                  
    }                                             

                                 
    public void changeListeClubs(Vector<String> v){
	listClub.setModel(new DefaultComboBoxModel(v));
	
    }              


    public void changeListeCategories(Vector<String> v){
	listCategorie.setModel(new DefaultComboBoxModel(v));

    }   
    
    public void changeListeNums(Vector<String> v){
	listNum.setModel(new DefaultComboBoxModel(v));

    }   

   

    // Variables declaration - do not modify                     
    private javax.swing.JButton Annuler;
    private javax.swing.JButton Valider;
    private javax.swing.JLabel categorie;
    private javax.swing.JLabel champ;
    private javax.swing.JLabel club;
    private javax.swing.JComboBox listCategorie;
    private javax.swing.JComboBox listClub;
    private javax.swing.JComboBox listNum;
    private javax.swing.JLabel num;
    // End of variables declaration                   
}
