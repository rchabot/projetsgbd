package federationbasket.GUI;

import federationbasket.src.*;

import javax.swing.*;
import java.util.*;
import java.sql.*;


public class delClub extends javax.swing.JDialog {

    
    public delClub(java.awt.Frame parent, String title,boolean modal) {
        super(parent,title, modal);
        initComponents();
    }
            
    private void initComponents() {

        champ = new javax.swing.JLabel();
        club = new javax.swing.JLabel();
        Valider = new javax.swing.JButton();
        Annuler = new javax.swing.JButton();
        ListClub = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        champ.setText("Choisissez le club à supprimer");

        club.setText("Club:");

        Valider.setText("Valider");
        Valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ValiderActionPerformed(evt);
            }
        });

        Annuler.setText("Annuler");
        Annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnnulerActionPerformed(evt);
            }
        });

        ListClub.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
   

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(champ)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(club)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(ListClub, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(Valider)
                        .addGap(64, 64, 64)
                        .addComponent(Annuler)))
                .addContainerGap(226, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(champ)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(club)
                    .addComponent(ListClub, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 176, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Valider)
                    .addComponent(Annuler))
                .addGap(46, 46, 46))
        );

        pack();
    }       


public void changeListeClubs(Vector<String> v){
	ListClub.setModel(new DefaultComboBoxModel(v));

    }   


    private void AnnulerActionPerformed(java.awt.event.ActionEvent evt) {                                        
        this.setVisible(false);
    }                                       

    private void ValiderActionPerformed(java.awt.event.ActionEvent evt) {    
	String club=ListClub.getSelectedItem().toString();
		try{
	int clubId=Toolkit.getIdFromNom("CLUB",club);
	Suppression.supprimerClub(clubId);

		}
		catch(SQLException e){e.printStackTrace();}

	this.setVisible(false);                                    
       
    }                                       

  
 

    // Variables declaration - do not modify                     
    private javax.swing.JButton Annuler;
    private javax.swing.JComboBox ListClub;
    private javax.swing.JButton Valider;
    private javax.swing.JLabel champ;
    private javax.swing.JLabel club;
    // End of variables declaration                   
}
