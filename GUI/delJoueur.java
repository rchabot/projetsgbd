package federationbasket.GUI;

import federationbasket.src.*;

import javax.swing.*;
import java.util.*;
import java.sql.*;



public class delJoueur extends javax.swing.JDialog {

 
    public delJoueur(java.awt.Frame parent,String title, boolean modal) {
        super(parent, modal);
        initComponents();
    }

      
    private void initComponents() {

        champ = new javax.swing.JLabel();
        valider = new javax.swing.JButton();
        annuler = new javax.swing.JButton();
        choix = new javax.swing.JLabel();
        identifiant = new javax.swing.JLabel();
        listId = new javax.swing.JComboBox();
        nomJoueur = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        champ.setText("Veuillez choisir le joueur à supprimer:");

        valider.setText("Valider");
        valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validerActionPerformed(evt);
            }
        });

        annuler.setText("Annuler");
        annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerActionPerformed(evt);
            }
        });

        choix.setText("Choix du joueur");

        identifiant.setText("ID:");

        listId.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        listId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listIdActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(143, 143, 143)
                        .addComponent(valider)
                        .addGap(88, 88, 88)
                        .addComponent(annuler))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(champ))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(identifiant)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(listId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addComponent(nomJoueur, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(choix)))
                .addContainerGap(228, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(champ)
                .addGap(55, 55, 55)
                .addComponent(choix)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(listId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(identifiant)
                    .addComponent(nomJoueur, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 102, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valider)
                    .addComponent(annuler))
                .addGap(53, 53, 53))
        );

        pack();
    }               

    private void listIdActionPerformed(java.awt.event.ActionEvent evt) {                                       
       int JoueurId=Integer.parseInt(listId.getSelectedItem().toString());

	try{  
	    nomJoueur.setText(Toolkit.getNomJoueurFromId(JoueurId)); 

	}
	catch(SQLException e){e.printStackTrace();}  
    }                                      

    private void annulerActionPerformed(java.awt.event.ActionEvent evt) {                                        
        this.setVisible(false); 
   }                                       

    private void validerActionPerformed(java.awt.event.ActionEvent evt) {                                        
       int JoueurId=Integer.parseInt(listId.getSelectedItem().toString());
       try{
	   Suppression.supprimerJoueur(JoueurId);
	   this.setVisible(false);
       }
	   catch(SQLException e){
	       e.printStackTrace();
	   }
       
    }                                       


  public void changeListId(Vector<String> v){
           listId.setModel(new DefaultComboBoxModel(v));
    }

 

    // Variables declaration - do not modify                     
    private javax.swing.JButton annuler;
    private javax.swing.JLabel champ;
    private javax.swing.JLabel choix;
    private javax.swing.JLabel identifiant;
    private javax.swing.JComboBox listId;
    private javax.swing.JLabel nomJoueur;
    private javax.swing.JButton valider;
    // End of variables declaration                   
}
