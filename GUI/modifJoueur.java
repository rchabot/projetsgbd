package federationbasket.GUI;

import federationbasket.src.*;

import javax.swing.*;
import java.util.*;
import java.sql.*;

public class modifJoueur extends javax.swing.JDialog {

   
    public modifJoueur(java.awt.Frame parent,String title, boolean modal) {
        super(parent,title, modal);
        initComponents();
    }

                        
    private void initComponents() {

        champ = new javax.swing.JLabel();
        Club = new javax.swing.JLabel();
        ListeClubs = new javax.swing.JComboBox();
        Categorie = new javax.swing.JLabel();
        ListeCategories = new javax.swing.JComboBox();
        valider = new javax.swing.JButton();
        annuler = new javax.swing.JButton();
        num = new javax.swing.JLabel();
        listNum = new javax.swing.JComboBox();
        choix = new javax.swing.JLabel();
        identifiant = new javax.swing.JLabel();
        listId = new javax.swing.JComboBox();
        nomJoueur = new javax.swing.JLabel();
        modification = new javax.swing.JLabel();
        rq = new javax.swing.JLabel();
        license = new javax.swing.JLabel();
        editLicense = new javax.swing.JTextField();
        rq2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        champ.setText("Veuillez modifier les champs souhaités:");

        Club.setText("Club:");

        
        ListeClubs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeClubsActionPerformed(evt);}
		catch(SQLException e){}
            }
        });

        Categorie.setText("Categorie:");
ListeCategories.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Choix" }));

        ListeCategories.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeCategoriesActionPerformed(evt);}
		catch(SQLException e){}
            }
        });

        valider.setText("Valider");
        valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validerActionPerformed(evt);
            }
        });

        annuler.setText("Annuler");
        annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerActionPerformed(evt);
            }
        });

        num.setText("numero:");

       

        choix.setText("Choix du joueur");

        identifiant.setText("ID:");

        
        listId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try {
		    listIdActionPerformed(evt);} catch(SQLException e){}
            }
        });

        modification.setText("Modifications possibles");

        rq.setText("(1)");

        license.setText("Numéro de licence: (*)");

	// editLicense.setText("jTextField1");

        rq2.setText("(1):  Renseignez les 3 champs pour modifier l'équipe du joueur");

       javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(115, 115, 115)
                        .addComponent(valider)
                        .addGap(52, 52, 52)
                        .addComponent(annuler))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(champ))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(identifiant)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(listId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addComponent(nomJoueur, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(choix))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(modification))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Club)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ListeClubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(license))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Categorie)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ListeCategories, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(33, 33, 33)
                                .addComponent(num)
                                .addGap(4, 4, 4)
                                .addComponent(listNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(40, 40, 40)
                                .addComponent(rq))
                            .addComponent(editLicense, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(rq2)))
                .addContainerGap(84, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(champ)
                .addGap(55, 55, 55)
                .addComponent(choix)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(listId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(identifiant)
                    .addComponent(nomJoueur, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(60, 60, 60)
                .addComponent(modification)
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Club)
                    .addComponent(ListeClubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Categorie)
                    .addComponent(ListeCategories, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(num)
                    .addComponent(listNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rq))
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(license)
                    .addComponent(editLicense, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addComponent(rq2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(valider)
                    .addComponent(annuler))
                .addGap(53, 53, 53))
        );

        pack();
    }// </editor-fold>                        

    private void validerActionPerformed(java.awt.event.ActionEvent evt) {                                        String club=ListeClubs.getSelectedItem().toString(),categorie=ListeCategories.getSelectedItem().toString(),licence=editLicense.getText(); int equipeId=0,numEquipe=0,joueurId=Integer.parseInt(listId.getSelectedItem().toString());
	try{
	Vector<String> values=new Vector<String>();
	Vector<String> column=new Vector<String>(); 
	if(!licence.isEmpty()){
	    values.add(licence);
	    column.add("JOUEUR_NUM_LICENCE");
	}
	if(!club.equals("Choix")){
		numEquipe=Integer.parseInt(listNum.getSelectedItem().toString());
		
		equipeId=Toolkit.getEquipeId(club,categorie,numEquipe);
		column.add("EQUIPE_ID");
		values.add(String.valueOf(equipeId));
		System.out.println(column.toString());
		System.out.println(values.toString());
	    }
	    MAJ.update("JOUEUR",joueurId,column,values);
	    this.setVisible(false);
	} catch(SQLException e){e.printStackTrace();}
      
    }                                       

    private void annulerActionPerformed(java.awt.event.ActionEvent evt) {                                        
	this.setVisible(false);
    }                                       

    private void ListeClubsActionPerformed(java.awt.event.ActionEvent evt) throws SQLException {            
	String club=ListeClubs.getSelectedItem().toString();
	Vector<String> ListCategories=new Vector<String>();
	ResultSet rs=Toolkit.getNomsCategorie(club);
	ListCategories.add("Choix");
	while(rs.next()){
	    ListCategories.add(rs.getString(1));
	}
	changeListeCategories(ListCategories);                                                             
      
    }                                          

    private void ListeCategoriesActionPerformed(java.awt.event.ActionEvent evt)	throws SQLException    {                                            
       	String categorie=ListeCategories.getSelectedItem().toString();
	String club=ListeClubs.getSelectedItem().toString();
	Vector<String> ListNums=new Vector<String>();
	ResultSet rs=Toolkit.getNumsEquipe(club,categorie);    
	while(rs.next()){
	    ListNums.add(rs.getString(1));
	}
	changeListeNums(ListNums);  
    }                                               

    private void listIdActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {    
	int JoueurId=Integer.parseInt(listId.getSelectedItem().toString());

	try{  
	    nomJoueur.setText(Toolkit.getNomJoueurFromId(JoueurId)); }
	catch(SQLException e){e.printStackTrace();}  

    }    

    public void changeListId(Vector<String> v){
           listId.setModel(new DefaultComboBoxModel(v));
    }

                                  
    public void changeListeClubs(Vector<String> v){
	ListeClubs.setModel(new DefaultComboBoxModel(v));
	
    }              


    public void changeListeCategories(Vector<String> v){
	ListeCategories.setModel(new DefaultComboBoxModel(v));

    }   
    
    public void changeListeNums(Vector<String> v){
	listNum.setModel(new DefaultComboBoxModel(v));

    }   

    
   


    // Variables declaration - do not modify                     
    private javax.swing.JLabel Categorie;
    private javax.swing.JLabel Club;
    private javax.swing.JComboBox ListeCategories;
    private javax.swing.JComboBox ListeClubs;
    private javax.swing.JButton annuler;
    private javax.swing.JLabel champ;
    private javax.swing.JLabel choix;
    private javax.swing.JTextField editLicense;
    private javax.swing.JLabel identifiant;
    private javax.swing.JComboBox listNum;
    private javax.swing.JLabel license;
    private javax.swing.JComboBox listId;
    private javax.swing.JLabel modification;
    private javax.swing.JLabel nomJoueur;
    private javax.swing.JLabel num;
    private javax.swing.JLabel rq;
    private javax.swing.JLabel rq2;
    private javax.swing.JButton valider;
    // End of variables declaration                   
}
