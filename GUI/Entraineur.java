package federationbasket.GUI;

import federationbasket.src.*;

import javax.swing.*;
import java.util.*;
import java.sql.*;

public class Entraineur extends javax.swing.JDialog {

 
    public Entraineur(java.awt.Frame parent, String title,boolean modal) {
        super(parent,title, modal);
        initComponents();
    }

                  
    private void initComponents() {

        champs = new javax.swing.JLabel();
        Nom = new javax.swing.JLabel();
	num = new javax.swing.JLabel();
        EditNom = new javax.swing.JTextField();
	EditNum = new javax.swing.JComboBox();
        Prenom = new javax.swing.JLabel();
        EditPrenom = new javax.swing.JTextField();
        date = new javax.swing.JLabel();
        EditDate = new javax.swing.JTextField();
        Adresse = new javax.swing.JLabel();
        EditAdresse = new javax.swing.JTextField();
        Entree = new javax.swing.JLabel();
        EditEntree = new javax.swing.JTextField();
        Club = new javax.swing.JLabel();
        ListeClubs = new javax.swing.JComboBox();
        Categorie = new javax.swing.JLabel();
        ListeCategories = new javax.swing.JComboBox();
        Valider = new javax.swing.JButton();
        Annuler = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        champs.setText("Veuillez remplir les champs ci-dessous:");

        Nom.setText("Nom:");

        Prenom.setText("Prenom:");

        date.setText("Date de naissance(*):");

        //EditDate.setText(" ");

        Adresse.setText("Adresse(*):");

        Entree.setText("Date d'entree(*):");

        Club.setText("Club:");

        ListeClubs.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeClubs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeClubsActionPerformed(evt);}
		catch(SQLException e) {}
            }
        });

        Categorie.setText("Categorie:");

        ListeCategories.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeCategories.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeCategoriesActionPerformed(evt); }
		catch(SQLException e) {}
            }

        });

        Valider.setText("Valider");
        Valider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ValiderActionPerformed(evt);} catch(SQLException e){}
            }
        });

        Annuler.setText("Annuler");
        Annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnnulerActionPerformed(evt);
            }
        });
num.setText("numero:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Nom)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(EditNom, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
			      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Prenom, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(EditPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(date)
                        .addGap(4, 4, 4)
                        .addComponent(EditDate, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(champs)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(Valider)
                                        .addGap(59, 59, 59)
                                        .addComponent(Annuler))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(Adresse)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(EditAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(54, 54, 54)
                                        .addComponent(Entree)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(EditEntree, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Club)
			      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
			      .addComponent(ListeClubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
			      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
			      .addComponent(Categorie)
			      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
			      .addComponent(ListeCategories, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
			      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
			      .addComponent(num)
			      .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
			      .addComponent(EditNum)
                        .addGap(211, 211, 211))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(champs)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Nom)
                    .addComponent(EditNom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Prenom)
                    .addComponent(EditPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(date)
                    .addComponent(EditDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Adresse)
                    .addComponent(EditAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Entree)
                    .addComponent(EditEntree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Club)
                    .addComponent(ListeClubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Categorie)
                    .addComponent(ListeCategories, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
		      .addComponent(num)
			  .addComponent(EditNum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Valider)
                    .addComponent(Annuler))
	    .addGap(35, 35, 35))
        );

        pack();
    }



    private void ValiderActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {         
	String nom=EditNom.getText(),prenom=EditPrenom.getText(),adresse=EditAdresse.getText(),entree=EditEntree.getText(),naissance=EditDate.getText(),club=ListeClubs.getSelectedItem().toString(), categorie=ListeCategories.getSelectedItem().toString();

	int numEquipe=Integer.parseInt(EditNum.getSelectedItem().toString());  
	if (entree.isEmpty())
	    entree=null;
	if (naissance.isEmpty())	   
	    naissance=null;
	if (adresse.isEmpty())
	    adresse=null;

	    
	try{  MAJ.insertEntraine(nom,prenom,naissance,adresse,entree,club,categorie,numEquipe);} catch(SQLException e) {e.printStackTrace();}

	this.setVisible(false);   
    }                                      

    private void AnnulerActionPerformed(java.awt.event.ActionEvent evt) {                                        
        this.setVisible(false);
    }                                       

    private void ListeClubsActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {            
	String club=ListeClubs.getSelectedItem().toString();
	Vector<String> ListCategories=new Vector<String>();
	ResultSet rs=Toolkit.getNomsCategorie(club);
	while(rs.next()){
	    ListCategories.add(rs.getString(1));
	}
	changeListeCategories(ListCategories);                                           
      
    }                                          

    private void ListeCategoriesActionPerformed(java.awt.event.ActionEvent evt)                                                
	throws SQLException    {                                            
       	String categorie=ListeCategories.getSelectedItem().toString();
	String club=ListeClubs.getSelectedItem().toString();
	Vector<String> ListNums=new Vector<String>();
	ResultSet rs=Toolkit.getNumsEquipe(club,categorie);    
	while(rs.next()){
	    ListNums.add(rs.getString(1));
	}
	changeListeNums(ListNums);  
    }  



 public void changeListeClubs(Vector<String> v){
	ListeClubs.setModel(new DefaultComboBoxModel(v));

    }              


public void changeListeCategories(Vector<String> v){
	ListeCategories.setModel(new DefaultComboBoxModel(v));

    }   

 public void changeListeNums(Vector<String> v){
	EditNum.setModel(new DefaultComboBoxModel(v));

    }   


  

    // Variables declaration - do not modify                     
    private javax.swing.JLabel Adresse;
    private javax.swing.JButton Annuler;
    private javax.swing.JLabel Categorie;
    private javax.swing.JLabel Club;
    private javax.swing.JTextField EditAdresse;
    private javax.swing.JTextField EditDate;
    private javax.swing.JTextField EditEntree;
    private javax.swing.JTextField EditNom;
    private javax.swing.JTextField EditPrenom;
    private javax.swing.JLabel Entree;
    private javax.swing.JComboBox ListeCategories;
    private javax.swing.JComboBox ListeClubs;
    private javax.swing.JLabel Nom;
    private javax.swing.JLabel Prenom;
    private javax.swing.JButton Valider;
    private javax.swing.JLabel champs;
    private javax.swing.JLabel date;
    private javax.swing.JComboBox EditNum;
    private javax.swing.JLabel num;
    // End of variables declaration                   
}
