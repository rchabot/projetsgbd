package federationbasket.GUI;

import federationbasket.src.*;

import javax.swing.*;
import java.util.*;
import java.sql.*;

public class Maj extends JFrame {
  private Fenetre fenetre;
   
    public Maj(Fenetre fenetre) {
	this.fenetre=fenetre;
        initComponents();
    }
                      
    private void initComponents() {
        Menu = new JButton();
	panier=new JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        addJoueur = new javax.swing.JMenuItem();
        addEquipe = new javax.swing.JMenuItem();
        addClub = new javax.swing.JMenuItem();
	addEntraineur = new javax.swing.JMenuItem();
        addRencontre = new javax.swing.JMenuItem();
        addStatistique = new javax.swing.JMenuItem();
	addPersonneMorale = new javax.swing.JMenuItem();
	delClub = new javax.swing.JMenuItem();
        delRencontre = new javax.swing.JMenuItem();
	delEquipe = new javax.swing.JMenuItem();
        delJoueur = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenu3 = new javax.swing.JMenu();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
	modifClub = new javax.swing.JMenuItem();
        modifRencontre = new javax.swing.JMenuItem();
        modifJoueur = new javax.swing.JMenuItem();
      
      	this.setSize(800, 600);
	this.setLocationRelativeTo(null);               
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jMenu1.setText("Insertion");
       Menu.setText("retour au menu principal");

       Menu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuprincipalActionPerformed(evt);
            }
        });
 
panier.setIcon(new javax.swing.ImageIcon("panier.jpg"));


        addJoueur.setText("Joueur");
        addJoueur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{ addJoueurActionPerformed(evt);}
		catch (SQLException e) {}

            }
        });
        jMenu1.add(addJoueur);

        addEquipe.setText("Equipe");
        addEquipe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

		try{
		    addEquipeActionPerformed(evt);}
	catch (SQLException e) {}

            }
        });
        jMenu1.add(addEquipe);

        addClub.setText("Club");
        addClub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addClubActionPerformed(evt);
            }
        });
        jMenu1.add(addClub);


	addEntraineur.setText("Entraineur");
	addEntraineur.addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(java.awt.event.ActionEvent evt) {
		    try{
			addEntraineurActionPerformed(evt);}
	catch (SQLException e) {}
		}
	    });
	




        jMenu1.add(addEntraineur);

        addRencontre.setText("Rencontre");
	addRencontre.addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(java.awt.event.ActionEvent evt) {
		    try{
			addRencontreActionPerformed(evt);}
	catch (SQLException e) {}
		}
	    });
        jMenu1.add(addRencontre);

        addStatistique.setText("Statistique");
	addStatistique.addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(java.awt.event.ActionEvent evt) {
		    try{	    addStatistiqueActionPerformed(evt);}
		    catch (SQLException e){}
		}
	    });
        jMenu1.add(addStatistique);

 addPersonneMorale.setText("Personne Morale");
      addPersonneMorale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
                PersonneMoraleActionPerformed(evt);}
		catch (SQLException e){}
            }
	  });

jMenu1.add(addPersonneMorale);



        jMenuBar1.add(jMenu1);

        jMenu2.setText("Suppression");
        jMenu2.add(jSeparator1);
	delClub.setText("Club");
        delClub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delClubActionPerformed(evt);
            }
        });
        jMenu2.add(delClub);

        delRencontre.setText("Rencontre");
        delRencontre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delRencontreActionPerformed(evt);
            }
        });
        jMenu2.add(delRencontre);

        delJoueur.setText("Joueur");
        delJoueur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    delJoueurActionPerformed(evt);} catch(SQLException e){}
            }
        });
        jMenu2.add(delJoueur);


        delEquipe.setText("Equipe");
        delEquipe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delEquipeActionPerformed(evt);
            }
        });
        jMenu2.add(delEquipe);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Modification");
        jMenu3.add(jSeparator2);


modifClub.setText("Club");
        modifClub.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifClubActionPerformed(evt);
            }
        });
        jMenu3.add(modifClub);

        modifRencontre.setText("Rencontre");
        modifRencontre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifRencontreActionPerformed(evt);
            }
        });
        jMenu3.add(modifRencontre);

        modifJoueur.setText("Joueur");
        modifJoueur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    modifJoueurActionPerformed(evt); }
		catch(SQLException e){}
            }
        });
        jMenu3.add(modifJoueur);

        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(207, 207, 207)
                .addComponent(Menu)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(191, Short.MAX_VALUE)
                .addComponent(panier, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(216, 216, 216))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(Menu)
                .addGap(45, 45, 45)
                .addComponent(panier)
                .addContainerGap(75, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>                        

    private void addEquipeActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {    

	Vector<String> ListClub=new Vector<String>();
	ResultSet rs=Toolkit.getNomsClub();
	ListClub.add("Choix");
	while(rs.next()){
	    ListClub.add(rs.getString(1)); 
	    }     
        
	Vector<String> ListCategorie=new Vector<String>();
	ResultSet rs2=Toolkit.getNomsCategorie();
	ListCategorie.add("Choix");
	while(rs2.next()){
	    ListCategorie.add(rs2.getString(1));
	   
	    }     
                              
     Equipe e=new  Equipe(null,"AJout d'une equipe",true);
     e.changeListeClubs(ListClub);
     e.changeListeCategories(ListCategorie);
     e.setVisible(true);
      
    }                                         

    private void addJoueurActionPerformed(java.awt.event.ActionEvent evt)throws SQLException { 
	Vector<String> ListClub=new Vector<String>();
	ResultSet rs=Toolkit.getNomsClub();
	ListClub.add("Choix");
	while(rs.next()){
	    ListClub.add(rs.getString(1)); 
	    }     

              
	Joueur j=new Joueur(null,"Ajout d'un joueur",true);
	j.changeListeClubs(ListClub);
	j.setVisible(true);

	    }                                         

    private void addClubActionPerformed(java.awt.event.ActionEvent evt) {                                        
        Club c=new Club(null,"AJout d'un club",true);
c.setVisible(true);

    }                      


    private void addEntraineurActionPerformed (java.awt.event.ActionEvent evt)throws SQLException {                                              
      	Vector<String> ListClub=new Vector<String>();
	ResultSet rs=Toolkit.getNomsClub();
	ListClub.add("Choix");
	while(rs.next()){
	    ListClub.add(rs.getString(1)); 
	}     


	Entraineur e=new Entraineur(null,"Ajout d'un entraineur", true);
	e.changeListeClubs(ListClub);
	e.setVisible(true);
    }   


    private void addRencontreActionPerformed(java.awt.event.ActionEvent evt) throws SQLException{                                             
	Vector<String> ListClub=new Vector<String>();
	ResultSet rs=Toolkit.getNomsClub();
	ListClub.add("Choix");
	while(rs.next()){
	    ListClub.add(rs.getString(1)); 
	    }           

	
	
	Rencontre r=new Rencontre(null,"Ajout d'une rencontre",true);
	r.changeListeClubs(ListClub);
	r.setVisible(true);
    }                                            

    private void addStatistiqueActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {         
  	Vector<String> ListClub=new Vector<String>();
	ResultSet rs=Toolkit.getNomsClub();
	ListClub.add("Choix");
	while(rs.next()){
	    ListClub.add(rs.getString(1)); 
	    }                                               
        Statistique s=new Statistique(null,"Ajout d'une statistique", true);
	s.changeListeClubs(ListClub);
	s.setVisible(true);
    }       

    private void modifClubActionPerformed(java.awt.event.ActionEvent evt) {           
	Vector<String> ListClub=new Vector<String>();
	try{
	    ResultSet rs=Toolkit.getNomsClub();
	    ListClub.add("Choix");
	    while(rs.next()){
		ListClub.add(rs.getString(1)); 
	    }
	} catch(SQLException e){}
	ModifClub mc=new ModifClub(null,"Modification d'un joueur",true);
	mc.changeListeClubs(ListClub);
	mc.setVisible(true);
                           
       
    }                                         

    private void modifRencontreActionPerformed(java.awt.event.ActionEvent evt) {    
			Vector<String> ListClub=new Vector<String>();
			try{
	ResultSet rs=Toolkit.getNomsClub();
	ListClub.add("Choix");
	while(rs.next()){
	ListClub.add(rs.getString(1)); 
	} } catch(SQLException e){}	
	modifRencontre mr=new modifRencontre(null,"Modification d'une rencontre",true);
	mr.changeListeClubs(ListClub);
			
	mr.setVisible(true);                                        
	
    }
                                          

    private void modifJoueurActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {                                            
     	Vector<String> ListClub=new Vector<String>();
	Vector<String> ListJoueur=new Vector<String>();
		        
			   
			    
	ResultSet rs=Toolkit.getNomsClub();
	ResultSet rs2=Toolkit.getId("JOUEUR");
	ListClub.add("Choix");
	ListJoueur.add("Choix");
	while(rs2.next()){			    
	    ListJoueur.add(rs2.getString(1));
	}
	
	while(rs.next()){
	    ListClub.add(rs.getString(1));
	} 

	modifJoueur mj=new modifJoueur(null,"Modification d'un joueur",true);
	mj.changeListeClubs(ListClub);
	mj.changeListId(ListJoueur);
			
	mj.setVisible(true);           
	                      
    }


    private void PersonneMoraleActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {    
    Vector<String> ListClub=new Vector<String>();
    ResultSet rs=Toolkit.getNomsClub();
    ListClub.add("Choix");
    while(rs.next()){
	ListClub.add(rs.getString(1)); 
    }                           
                                           
    PersonneMorale p=new PersonneMorale(null,"Ajout d'une personne morale",true);
    p.changeListeClubs(ListClub);
    p.setVisible(true);
    } 


 private void delClubActionPerformed(java.awt.event.ActionEvent evt) {       
	Vector<String> ListClub=new Vector<String>();
			try{
	ResultSet rs=Toolkit.getNomsClub();
	ListClub.add("Choix");
	while(rs.next()){
	ListClub.add(rs.getString(1)); 
	} } catch(SQLException e){e.printStackTrace();}	
	delClub dc=new delClub(null,"Suppression d'un club",true);
	dc.changeListeClubs(ListClub);
			
	dc.setVisible(true);        
                                 
       
    }                                       

    private void delRencontreActionPerformed(java.awt.event.ActionEvent evt) {        
      	Vector<String> ListClub=new Vector<String>();
			try{
	ResultSet rs=Toolkit.getNomsClub();
	ListClub.add("Choix");
	while(rs.next()){
	ListClub.add(rs.getString(1)); 
	} } catch(SQLException e){}	
	delRencontre dr=new delRencontre(null,"Suppression d'une rencontre",true);
	dr.changeListeClubs(ListClub);
			
	dr.setVisible(true);                                    
        
    }            


 private void delEquipeActionPerformed(java.awt.event.ActionEvent evt) {  
	Vector<String> ListClub=new Vector<String>();                                        
       	try{
	ResultSet rs=Toolkit.getNomsClub();
	ListClub.add("Choix");
	while(rs.next()){
	ListClub.add(rs.getString(1)); 
	} } catch(SQLException e){}	
	delEquipe de=new delEquipe(null,"Suppression d'une rencontre",true);
	de.changeListeClubs(ListClub);
			
	de.setVisible(true);        



    }                                

    private void delJoueurActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {                                          
                                    
	Vector<String> ListJoueur=new Vector<String>();
		        
			
			    
	
	ResultSet rs=Toolkit.getId("JOUEUR");
	
	ListJoueur.add("Choix");
	while(rs.next()){			    
	    ListJoueur.add(rs.getString(1));
	}
	
	

	delJoueur dj=new delJoueur(null,"Suppression d'un joueur",true);
	
	dj.changeListId(ListJoueur);
			
	dj.setVisible(true);           
	


    }                                         



 private void menuprincipalActionPerformed(java.awt.event.ActionEvent evt) {                                              
        this.setVisible(false);
	fenetre.setAccueil();
	fenetre.setVisible(true);

 }


                     

    
    

    // Variables declaration - do not modify                     
    private javax.swing.JMenuItem addClub;
    private javax.swing.JMenuItem addEntraineur;
    private javax.swing.JMenuItem addRencontre;
    private javax.swing.JMenuItem addStatistique;
    private javax.swing.JMenuItem addEquipe;
    private javax.swing.JMenuItem addJoueur;
    private javax.swing.JMenuItem addPersonneMorale;
    private javax.swing.JMenuItem delClub;
    private javax.swing.JMenuItem delJoueur;
    private javax.swing.JMenuItem delRencontre;
    private javax.swing.JMenuItem delEquipe;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JMenuItem modifClub;
    private javax.swing.JMenuItem modifRencontre;
    private javax.swing.JMenuItem modifJoueur;
    private javax.swing.JLabel panier;
    private JButton Menu;
    // End of variables declaration                   
}
