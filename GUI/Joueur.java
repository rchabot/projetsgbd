package federationbasket.GUI;

import federationbasket.src.*;

import javax.swing.*;
import java.util.*;
import java.sql.*;

public class Joueur extends JDialog {

    public Joueur(java.awt.Frame parent, String title,boolean modal) {
        super(parent,title, modal);
        initComponents();

    }

                  
    private void initComponents() {

	champs = new javax.swing.JLabel();
        Nom = new javax.swing.JLabel();
        EditNom = new javax.swing.JTextField();
        Prenom = new javax.swing.JLabel();
        EditPrenom = new javax.swing.JTextField();
        Club = new javax.swing.JLabel();
        ListeClubs = new javax.swing.JComboBox();
        License = new javax.swing.JLabel();
        EditLicense = new javax.swing.JTextField();
        Categorie = new javax.swing.JLabel();
        ListeCategories = new javax.swing.JComboBox();
        Adresse = new javax.swing.JLabel();
        EditAdresse = new javax.swing.JTextField();
        Naissance = new javax.swing.JLabel();
        EditNaissance = new javax.swing.JTextField();
        entree = new javax.swing.JLabel();
        EditEntree = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        num = new javax.swing.JLabel();
        ListeNums = new javax.swing.JComboBox();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        champs.setText("Veuillez remplir les champs ci-dessous:");

        Nom.setText("Nom:");

        EditNom.setText("");
       

        Prenom.setText("Prenom:");

      

        Club.setText("Club:");


	

	//  ListeClubs.setModel(new DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeClubs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeClubsActionPerformed(evt);}
		catch(SQLException e){}
            }
        });

        License.setText("Numéro de license:");

 Categorie.setText("Categorie:");

 //    ListeCategories.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
 
	
        ListeCategories.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{
		    ListeCategoriesActionPerformed(evt);}
		catch(SQLException e){}
            }
        });
       

        Adresse.setText("Adresse(*):");

       

        Naissance.setText("date de naissance(*):");

        entree.setText("date d'entree(*):");

        jButton1.setText("Valider");
	jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
		try{ jButton1ActionPerformed(evt);} catch(SQLException e){}
            }
        });




        jButton2.setText("Annuler"); 
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

	num.setText("numero:");

	ListeNums.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ListeNums.addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(java.awt.event.ActionEvent evt) {
		    ListeNumsActionPerformed(evt);
		}
	    });




        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(License)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(EditLicense, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(champs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(Adresse)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(EditAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(entree))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(Nom)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(EditNom, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(39, 39, 39)
                                .addComponent(Prenom)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(EditPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addComponent(Naissance)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(EditNaissance, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(EditEntree, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(Club)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ListeClubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(50, 50, 50)
                                .addComponent(Categorie))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(127, 127, 127)
                                .addComponent(jButton1)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addComponent(jButton2))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ListeCategories, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(41, 41, 41)
                                .addComponent(num)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ListeNums, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(champs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Nom)
                    .addComponent(EditNom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Prenom)
                    .addComponent(EditPrenom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Naissance)
                    .addComponent(EditNaissance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Adresse)
                    .addComponent(EditAdresse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(entree)
                    .addComponent(EditEntree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(License)
                    .addComponent(EditLicense, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Club)
                    .addComponent(ListeClubs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Categorie)
                    .addComponent(ListeCategories, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(num)
                    .addComponent(ListeNums, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addGap(22, 22, 22))
        );

        pack();
    }                      

  
    private void ListeClubsActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {                               
	String club=ListeClubs.getSelectedItem().toString();
	Vector<String> ListCategories=new Vector<String>();
	ResultSet rs=Toolkit.getNomsCategorie(club);
	while(rs.next()){
	    ListCategories.add(rs.getString(1));
	}
	changeListeCategories(ListCategories);
    }                                          

    private void ListeCategoriesActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {       
	String categorie=ListeCategories.getSelectedItem().toString();
	String club=ListeClubs.getSelectedItem().toString();
	Vector<String> ListNums=new Vector<String>();
	ResultSet rs=Toolkit.getNumsEquipe(club,categorie);    
	while(rs.next()){
	    ListNums.add(rs.getString(1));
	}
	changeListeNums(ListNums);                                 
       
    }

private void ListeNumsActionPerformed(java.awt.event.ActionEvent evt) {                                          
       
    }     



    
                                               
	//	String Club=ListeClubs.getSelectedItem().toString();

          

 private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)throws SQLException {
      String nom=EditNom.getText(),prenom=EditPrenom.getText(),adresse=EditAdresse.getText(), license=EditLicense.getText(),entree=EditEntree.getText(),naissance=EditNaissance.getText(), club=ListeClubs.getSelectedItem().toString(), categorie=ListeCategories.getSelectedItem().toString();
      int numEquipe=Integer.parseInt(ListeNums.getSelectedItem().toString());
        if (entree.isEmpty())
        entree=null;
	if (naissance.isEmpty())
	    naissance=null;
	if (adresse.isEmpty())
	    adresse=null;
       // System.out.println (nom+" " +prenom + " " +adresse+" "+license+" "+entree+" "+naissance+" "+club+" "+categorie+" "+numEquipe);
     //  MAJ.insertPersonne("zou","lotfi",null,"a",null,"CSP");
      // System.out.println("ajoutpersonne");
      // try{    MAJ.insertJoueur("zou","lotfi",null,"a",null,"010355","CSP","CADET",1);} catch(SQLException e){e.printStackTrace();}
	 try{	MAJ.insertJoueur(nom,prenom,naissance,adresse,entree,license,club,categorie,numEquipe);}  catch(SQLException e){e.printStackTrace();}
	//	System.out.println((entree.isEmpty()));

     this.setVisible(false);
 }  



 private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {   
                               
        this.setVisible(false);
    }                                       

 
    public void changeListeClubs(Vector<String> v){
	ListeClubs.setModel(new DefaultComboBoxModel(v));

    }   

    public void changeListeCategories(Vector<String> v){
	ListeCategories.setModel(new DefaultComboBoxModel(v));

    }   

 public void changeListeNums(Vector<String> v){
	ListeNums.setModel(new DefaultComboBoxModel(v));

    }   

   




    // Variables declaration - do not modify                     
    private JLabel Adresse;
    private JLabel Club;
    private JTextField EditAdresse;
    private JTextField EditEntree;
    private JTextField EditLicense;
    private JTextField EditNaissance;
    private JTextField EditNom;
    private JTextField EditPrenom;
    private JLabel champs;
    private javax.swing.JLabel Categorie;
    private javax.swing.JLabel num;
    private JLabel License;
    private JComboBox ListeClubs;
    private javax.swing.JComboBox ListeNums;
    private javax.swing.JComboBox ListeCategories;
    private JLabel Naissance;
    private JLabel Nom;
    private JLabel Prenom;
    private JLabel entree;
    private JButton jButton1;
    private JButton jButton2;
   
    // End of variables declaration                   
}
