-- fct: nbDéfaites 
-- @args nomClub

select count(*) nb_de_victoires
from (select r.RENCONTRE_ID
      from CLUB c, RENCONTRE r, EQUIPE e
      where c.CLUB_ID= (select CLUB_ID
       		    	from CLUB c
			where c.CLUB_NOM = UPPER('csp'))   
      and e.CLUB_ID = c.CLUB_ID 
      and r.EQUIPE_A_ID = e.EQUIPE_ID 
      and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B

      UNION

      select r.RENCONTRE_ID
      from CLUB c, RENCONTRE r,  EQUIPE e
      where c.CLUB_ID = (select CLUB_ID
       		    	 from CLUB c
			 where c.CLUB_NOM = UPPER('csp'))
      and e.CLUB_ID = c.CLUB_ID 
      and r.EQUIPE_B_ID = e.EQUIPE_ID 
      and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B) 
as result
