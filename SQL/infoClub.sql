-- infoClub(String nomClub)

select * from CLUB
where CLUB_NOM = UPPER(nomClub);

-- bureauClub(String nomClub)

select pm.FONCTION, p.PERSONNE_NOM, p.PERSONNE_PRENOM
from PERSONNE p, PERSONNE_MORALE pm, CLUB c
where p.PERSONNE_ID = pm.PERSONNE_ID
and p.CLUB_ID = p. CLUB_ID
and c.CLUB_NOM = UPPER(nomClub);
