-- fct MoyennePtsMatch
-- @args numSaison, categorie
-- Moyenne des points marqués par match pour la saison

select avg(pts_marques_match)
from (
     select sum(score) as pts_marques_match, RENCONTRE_ID
     from (select r.SCORE_EQUIPE_A as score, r.RENCONTRE_ID
     	  from RENCONTRE r, EQUIPE eqA	      
	  where r.NUMERO_SAISON = 1
	  and r.EQUIPE_A_ID = eqA.EQUIPE_ID
	  and eqA.CATEGORIE_ID = 8
     	  UNION ALL
     	  select r.SCORE_EQUIPE_B as score, r.RENCONTRE_ID
     	  from RENCONTRE r, EQUIPE eqB
     	  where r.NUMERO_SAISON = 1
	  and r.EQUIPE_B_ID = eqB.EQUIPE_ID
	  and eqB.CATEGORIE_ID = 8) as result
     group by RENCONTRE_ID
) as result


