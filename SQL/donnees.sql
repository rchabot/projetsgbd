-- ============================================================
--    suppression des donnees
-- ============================================================


delete from CATEGORIE;
delete from CLUB;
delete from EQUIPE;
delete from ENTRAINE;
delete from JOUEUR;
delete from PERSONNE;
delete from PERSONNE_MORALE;
delete from RENCONTRE;
delete from STATS;
commit ;

-- ============================================================
--    creation des donnees
-- ============================================================

-- CLUB
insert into CLUB(CLUB_NOM, CLUB_ADRESSE, CLUB_TELEPHONE)

values ('CSP', 'BEAUBLANC', '0678985867'),
('SA', 'At&T CENTER', '0001112223'),
('LAKERS', 'STAPLES CENTER', '1112223334'),
('ALL STARS', 'BERCY', '2223334445');



-- CATEGORIE
insert into CATEGORIE(CATEGORIE_NOM)

values ('BABY-BASKETTEUR'),
('MINI-POUSSIN'),
('POUSSIN'),
('BENJAMINS'),
('MINIMES'),
('CADETS'),
('JUNIOR'),
('SENIOR');


-- PERSONNE
insert into PERSONNE(PERSONNE_NOM, PERSONNE_PRENOM, PERSONNE_DATE_DE_NAISSANCE, PERSONNE_ADRESSE, PERSONNE_DATE_ENTREE, CLUB_ID)

-- CLUB CSP
values	('FORTE', 'FRED', null, null, '1990-10-10', 1),
		('DUPRAZ', 'JEAN-MARC', NULL, NULL, NULL, 1),
		('ADJOINT', 'ENTRAINEUR', '1960-10-01', 'Rue de la soif', '1985-01-01', 1),
		('MOERMAN', 'ADRIEN', NULL, NULL, NULL, 1),
		('GOMIS', 'JOSEPH', NULL, NULL, NULL, 1),
		('WESTERMAN', 'LEO', NULL, NULL, NULL, 1),
		('REMPLACANT', 'CSP', NULL, NULL, NULL, 1),
		('ENTRAINEUR', 'SANS EMPLOI', NULL, NULL, NULL, 1),
		('POPOVICH', 'GREGG', NULL, NULL, NULL, 2),
		('PARKER', 'TONY', NULL, NULL, NULL, 2),
		('DIAW', 'BORIS', NULL, NULL, NULL, 2),
		('GINOBILI', 'MARIO', NULL, NULL, NULL, 2),
		-- CLUB SA
		('REMPLACANT', 'SA', NULL, NULL, NULL, 2),
		-- CLUB LAKERS
		('ENTRAINEUR','LAKERS', NULL, NULL, NULL, 3),
		('BRYANT', 'KOBE', NULL, NULL, NULL, 3),
		('NASH', 'STEVE', NULL, NULL, NULL, 3),
		('YOUNG', 'NICK', NULL, NULL, NULL, 3),
		-- CLUB ALL STARS
		('ENTRAINEUR','ALL STARS', NULL, NULL, NULL, 4),
		('JORDAN','MICHAEL', NULL, NULL, NULL, 4),
		('JOHNSON', 'MAGIC', NULL, NULL, NULL, 4),
		('LEBRON', 'JAMES', NULL, NULL, NULL, 4),
		-- CLUB LIMOGES 2e CATEGORIE 1ere equipe
		('JUNIOR1', 'CSPUN', NULL, NULL, NULL, 1),
		('JUNIOR1', 'CSPDEUX', NULL, NULL, NULL, 1),
		('JUNIOR1', 'HOMONYME', NULL, NULL, NULL, 1),
		-- CLUB LIMOGES 2e CATEGORIE 1ere equipe
		('JUNIOR2', 'CSPUN', NULL, NULL, NULL, 1),
		('JUNIOR2', 'CSPDEUX', NULL, NULL, NULL, 1),
		('JUNIOR1', 'HOMONYME', NULL, NULL, NULL, 1),
		-- CLUB SA 2e CATEGORIE
		('JUNIOR', 'SAUN', NULL, NULL, NULL, 2),
		('JUNIOR', 'SADEUX', NULL, NULL, NULL, 2),
		('JUNIOR', 'SATROIS', NULL, NULL, NULL, 2);

-- PERSONNE_MORALE
insert into PERSONNE_MORALE(PERSONNE_ID, FONCTION)

values (1, 'PRESIDENT');

-- EQUIPE
insert into EQUIPE(EQUIPE_NUMERO, CLUB_ID, CATEGORIE_ID)

values	(1, 1, 8),
		(1, 2, 8),
		(1, 3, 8),
		(1, 4, 8),
		(1, 1, 7),
		(2, 1, 7),
		(1, 2, 7);

-- JOUEUR
insert into JOUEUR(JOUEUR_ID, JOUEUR_NUM_LICENSE, EQUIPE_ID)

value	(4, 'AAAA0001', 1),
		(5, 'AAAA0002', 1),
		(6, 'AAAA0003', 1),
		(7, 'AAAA0004', 1),
		(10,'BBBB0001', 2),
		(11,'BBBB0002', 2),
		(12,'BBBB0003', 2),
		(13,'BBBB0004', 2),
		(15,'CCCC0001', 3),
		(16,'CCCC0002', 3),
		(17,'CCCC0003', 3),
		(19,'DDDD0001', 4),
		(20,'DDDD0002', 4),
		(21,'DDDD0003', 4),
		(22,'AAAA0005', 5),
		(23,'AAAA0006', 5),
		(24,'AAAA0007', 5),
		(25,'AAAA0008', 6),
		(26,'AAAA0009', 6),
		(27,'AAAA0010', 6),
		(28,'BBBB0005', 7),
		(29,'BBBB0006', 7),
		(30,'BBBB0007', 7);


-- ENTRAINE
insert into ENTRAINE(ENTRAINEUR_ID, EQUIPE_ID)

values	(2,1),
		(3,1),
		(3,5),
		(3,6),
		(9,2),
		(14,3),
		(15,3),
		(18,4);


-- RENCONTRE
insert into RENCONTRE(EQUIPE_A_ID, EQUIPE_B_ID, RENCONTRE_DATE, NUMERO_JOURNEE, NUMERO_SAISON, SCORE_EQUIPE_A, SCORE_EQUIPE_B)

values	(1, 2, '2015-01-10', 1, 1, 64, 63),
		(3, 4, '2015-01-10', 1, 1, 75, 97),
		(1, 4, '2015-01-12', 2, 1, 63, 72),
		(3, 1, '2015-01-13', 3, 1, 70, 70),
		(2, 3, '2015-01-14', 2, 1, 80, 59),
		(4, 2, '2015-01-14', 3, 1, 100, 85),
		(5, 7, '2015-01-14', 1, 1, 55, 59),
		(1, 2, '2016-01-08', 1, 2, 77, 88);

-- STATS
insert into STATS(JOUEUR_ID, RENCONTRE_ID, STATS_POINTS, STATS_FAUTES)

-- Rencontre 1
values	(4 , 1, 20, 3),
		(5 , 1, 33, 2),
		(6 , 1, 7, 5),
		-- Remplaçant qui marque
		(7 , 1, 4, 1),
		(10, 1, 25, 2),
		(11, 1, 20, 3),
		(12, 1, 18, 4),
		-- Remplaçant absent de la feuille de match
		-- Rencontre 2
		(15, 2, 38, 1),
		(16, 2, 10, 4),
		(17, 2, 27, 4),
		(19, 2, 33, 2),
		(20, 2, 40, 3),
		(21, 2, 24, 4),
		-- Rencontre 3
		(4 , 3, 20, 3),
		(5 , 3, 23, 2),
		(6 , 3, 17, 2),
		-- Remplaçant sur la feuille de match
		(7 , 3, 0, 0),
		(19, 3, 39, 2),
		(20, 3, 23, 3),
		(21, 3, 20, 3),
		-- Rencontre 4
		(4 , 4, 20, 1),
		(5 , 4, 28, 4),
		(6 , 4, 22, 2),
		(15, 4, 29, 3),
		(16, 4, 11, 1),
		(17, 4, 30, 2),
		-- Rencontre 5
		(10, 5, 37, 2),
		(11, 5, 23, 4),
		(12, 5, 20, 3),
		(15, 5, 36, 3),
		(16, 5, 8, 0),
		(17, 5, 15, 4),
		-- Rencontre 6
		(19, 6, 39, 1),
		(20, 6, 21, 4),
		(21, 6, 40, 3),
		(10, 6, 30, 2),
		(11, 6, 35, 4),
		(12, 6, 20, 3),
		-- Rencontre junior
		(25, 7, 15, 3),
		(26, 7, 15, 2),
		(27, 7, 25, 1),
		(28, 7, 30, 5),
		(29, 7, 19, 4),
		(30, 7, 10, 3),
		-- Rencontre saison 2
		(4 , 8, 29, 3),
		(5 , 8, 33, 2),
		(6 , 8, 15, 5),
		(10, 8, 29, 2),
		(11, 8, 20, 3),
		(12, 8, 39, 4);
