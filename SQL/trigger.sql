
DELIMITER $$
CREATE TRIGGER after_delete_joueur
AFTER UPDATE ON JOUEUR
      FOR EACH ROW
      BEGIN
      DELETE FROM PERSONNE WHERE PERSONNE.PERSONNE_ID = OLD.JOUEUR_ID;
      END$$
DELIMITER;


DELIMITER //
CREATE TRIGGER after_insert_equipe 
AFTER UPDATE ON EQUIPE  
      FOR EACH ROW 
      BEGIN
      INSERT INTO GetEquipe(EQUIPE_ID,CLUB_NOM,CATEGORIE_NOM,EQUIPE_NUMERO) 
      select e.EQUIPE_ID, c.CLUB_NOM, cat.CATEGORIE_NOM, e.EQUIPE_NUMERO 
      from EQUIPE e, CLUB c, CATEGORIE cat 
      where e.CLUB_ID = c.CLUB_ID and e.CATEGORIE_ID = cat.CATEGORIE_ID  and e.EQUIPE_ID=NEW.EQUIPE_ID
END//
DELIMITER;

CREATE TRIGGER after_delete_equipe
AFTER DELETE ON EQUIPE
      FOR EACH ROW 
      DELETE FROM GetEquipe WHERE EQUIPE_ID = OLD.EQUIPE_ID;
