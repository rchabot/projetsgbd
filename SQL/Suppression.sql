-- Suppression joueur

DELETE FROM STATS WHERE joueur_id=4;
DELETE FROM JOUEUR WHERE joueur_id=4;
DELETE FROM PERSONNE WHERE personne_id=4;

-- Suppression Entraineur

DELETE FROM ENTRAINE WHERE entraineur_id=9;
DELETE FROM PERSONNE WHERE personne_id=9;

-- Suppression equipe

DELETE FROM STATS WHERE rencontre_id IN (
	SELECT rencontre_id FROM RENCONTRE
			WHERE equipe_a_id=1 OR equipe_b_id = 1);
	DELETE FROM JOUEUR WHERE equipe_id=1;
	DELETE FROM ENTRAINE WHERE EQUIPE_ID = 1;
	DELETE FROM RENCONTRE WHERE equipe_a_id=1 OR equipe_b_id=1;
	DELETE FROM EQUIPE WHERE equipe_id=1;
