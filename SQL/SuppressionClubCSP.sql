-- Suppression Club

DELETE FROM STATS WHERE joueur_id IN
	(SELECT personne_id FROM PERSONNE
		WHERE club_id=1);

DELETE FROM STATS WHERE rencontre_id IN
       (SELECT rencontre_id FROM RENCONTRE
       WHERE equipe_a_id IN
       	     (SELECT equipe_id FROM EQUIPE
	     WHERE club_id=1)
       UNION
       SELECT rencontre_id FROM RENCONTRE
	     WHERE equipe_b_id IN
	     (SELECT equipe_id FROM EQUIPE
		  WHERE club_id=1));

DELETE FROM JOUEUR WHERE joueur_id IN
	(SELECT personne_id FROM PERSONNE
		WHERE club_id=1);

DELETE FROM PERSONNE_MORALE WHERE personne_id IN 
       	(SELECT personne_id FROM PERSONNE
		WHERE club_id=1);

DELETE FROM ENTRAINE WHERE equipe_id IN
	(SELECT personne_id FROM PERSONNE
		WHERE club_id=1);
       	      
DELETE FROM PERSONNE WHERE club_id=1;


DELETE FROM RENCONTRE WHERE equipe_a_id  IN (SELECT equipe_id FROM EQUIPE
		WHERE club_id=1)    OR equipe_b_id IN 
	(SELECT equipe_id FROM EQUIPE
		WHERE club_id=1);

DELETE FROM EQUIPE WHERE club_id=1;

DELETE FROM CLUB where club_id=1;
