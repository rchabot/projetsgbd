-- ============================================================
--   Nom de la base   :  FEDERATION BASKET                                
--   Nom de SGBD      :  MySQL Version ??                    
--   Date de creation :  21/11/2014                       
-- ============================================================

SET FOREIGN_KEY_CHECKS = 0;

drop table if exists CATEGORIE;

drop table if exists CLUB;

drop table if exists EQUIPE;

drop table if exists ENTRAINE;

drop table if exists JOUEUR;

drop table if exists PERSONNE;

drop table if exists PERSONNE_MORALE;

drop table if exists RENCONTRE;

drop table if exists STATS;

SET FOREIGN_KEY_CHECKS = 1;

-- ============================================================
--   Table : CLUB                                            
-- ============================================================
create table CLUB
(
    CLUB_ID         INT	UNSIGNED    AUTO_INCREMENT	not null,
    CLUB_NOM        VARCHAR(255)    	          	not null,
    CLUB_ADRESSE    VARCHAR(255)  				,
    CLUB_TELEPHONE  CHAR(10)                                    ,
    constraint pk_club primary key (CLUB_ID)
);

-- ============================================================
--   Table : PERSONNE                                       
-- ============================================================
create table PERSONNE
(
    PERSONNE_ID                 INT UNSIGNED AUTO_INCREMENT not null,
    PERSONNE_NOM                CHAR(20)                    not null,
    PERSONNE_PRENOM             CHAR(20)                    not null,
    PERSONNE_DATE_DE_NAISSANCE  DATE                                ,
    PERSONNE_ADRESSE            VARCHAR(255)                        ,
    PERSONNE_DATE_ENTREE        DATE                                ,
    CLUB_ID                     INT UNSIGNED                not null, 
    constraint pk_personne primary key (PERSONNE_ID),
    constraint fk1_personne foreign key (CLUB_ID)  
    	       		    references CLUB (CLUB_ID)
);

-- ============================================================
--   Table : PERSONNE MORALE                                      
-- ============================================================
create table PERSONNE_MORALE
(
    PERSONNE_ID           INT UNSIGNED                   not null,
    FONCTION              VARCHAR(255)                   not null,
    constraint pk_morale primary key (PERSONNE_ID)               ,
    constraint fk1_morale foreign key (PERSONNE_ID)
    	       		  references PERSONNE (PERSONNE_ID)

);

-- ============================================================
--   Table : CATEGORIE                                
-- ============================================================
create table CATEGORIE
(
    CATEGORIE_ID       INT UNSIGNED AUTO_INCREMENT          not null,
    CATEGORIE_NOM      CHAR(20)                             not null,
    constraint pk_categorie primary key (CATEGORIE_ID)
);

-- ============================================================
--   Table : EQUIPE                                   
-- ============================================================
create table EQUIPE
(
    EQUIPE_ID      INT UNSIGNED AUTO_INCREMENT   not null,
    EQUIPE_NUMERO  INT UNSIGNED				 ,
    CLUB_ID        INT UNSIGNED           	 not null,
    CATEGORIE_ID   INT UNSIGNED           	 not null,
    ENTRAINEUR_ID  INT UNSIGNED           	 not null,          
    constraint pk_equipe  primary key (EQUIPE_ID),
    constraint fk1_equipe foreign key (CLUB_ID)  
    	       		  references CLUB (CLUB_ID),
    constraint fk2_equipe foreign key (CATEGORIE_ID)  
    	       		  references CATEGORIE (CATEGORIE_ID),
    constraint fk3_personne foreign key (ENTRAINEUR_ID) 
    	       		  references PERSONNE (PERSONNE_ID)
);

-- ============================================================
--   Table : JOUEUR                                             
-- ============================================================
create table JOUEUR
(
    JOUEUR_ID            INT UNSIGNED AUTO_INCREMENT    not null,
    JOUEUR_NUM_LICENCE   CHAR(8)                               ,
    EQUIPE_ID            INT UNSIGNED                           , 
    constraint pk_joueur  primary key (JOUEUR_ID),
    constraint fk1_joueur foreign key (JOUEUR_ID)  
    	       		  references PERSONNE (PERSONNE_ID),
    constraint fk2_joueur foreign key (EQUIPE_ID)  
    	       		  references EQUIPE  (EQUIPE_ID)

);

-- ============================================================
--   Table : ENTRAINE                                           
-- ============================================================
create table ENTRAINE
(
    ENTRAINEUR_ID        INT UNSIGNED AUTO_INCREMENT       not null,
    EQUIPE_ID            INT UNSIGNED                      not null,
    constraint pk_entraine  primary key (ENTRAINEUR_ID, EQUIPE_ID),
    constraint fk1_entraine foreign key (ENTRAINEUR_ID) 
    	       		    references PERSONNE (PERSONNE_ID),
    constraint fk2_entraine foreign key (EQUIPE_ID)  
    	       		    references EQUIPE  (EQUIPE_ID)

);

-- ============================================================
--   Table : RENCONTRE                                            
-- ============================================================
create table RENCONTRE
(
   
    RENCONTRE_ID         INT UNSIGNED AUTO_INCREMENT       not null,
    EQUIPE_A_ID          INT UNSIGNED                      not null,
    EQUIPE_B_ID          INT UNSIGNED                      not null,
    RENCONTRE_DATE       DATE                              not null,
    NUMERO_JOURNEE 	 INT UNSIGNED			   not null,
    NUMERO_SAISON	 INT UNSIGNED			   not null,
    SCORE_EQUIPE_A       INT UNSIGNED                              ,
    SCORE_EQUIPE_B       INT UNSIGNED                        	   ,
    constraint pk_rencontre primary key (RENCONTRE_ID),
    constraint fk1_rencontre foreign key (EQUIPE_A_ID)  
    	       		     references EQUIPE (EQUIPE_ID),
    constraint fk2_rencontre foreign key (EQUIPE_B_ID)  
    	       		     references EQUIPE (EQUIPE_ID)

);


-- ============================================================
--   Table : STATS                                            
-- ============================================================
create table STATS
(
    JOUEUR_ID                 INT UNSIGNED           not null,
    RENCONTRE_ID              INT UNSIGNED           not null,
    STATS_POINTS              INT UNSIGNED	     	     ,
    STATS_FAUTES              INT UNSIGNED                   ,
    constraint pk_stats primary key (JOUEUR_ID,RENCONTRE_ID),
    constraint fk1_stats foreign key (JOUEUR_ID)  
    	       		 references PERSONNE (PERSONNE_ID),
    constraint fk2_stats foreign key (RENCONTRE_ID) 
    	       		 references RENCONTRE(RENCONTRE_ID)

);
