-- FCT CLASSEMENT
-- @ARGS CATEGORIE, NUM SAISON
-- CLASSEMENT DES EQUIPES DE LA CATEGORIE POUR LA SAISON

SELECT G.CLUB_NOM AS CLUB, PTS, V.nb_victoires AS V, N.nb_nuls AS N, D.nb_defaites AS D, POUR, CONTRE
FROM 
-- Table indiquant pour une équipe la somme des paniers marqués (POUR)
-- et la somme des paniers encaissés (CONTRE)
(SELECT EQUIPE, SUM(POUR) AS POUR, SUM(CONTRE) AS CONTRE
 FROM (
      -- Enregistre pour une équipe les points qu'elle a marqué ou 
      -- encaissé lorsqu'elle était l'équipe A lors de la rencontre
      SELECT e.EQUIPE_ID AS EQUIPE, 
      	     SCORE_EQUIPE_A AS POUR, 
	     SCORE_EQUIPE_B AS CONTRE, 
	     adv.EQUIPE_ID AS ADVERSAIRE
      FROM RENCONTRE r, EQUIPE e, EQUIPE adv
      WHERE r.EQUIPE_A_ID = e.EQUIPE_ID
      AND r.EQUIPE_B_ID = adv.EQUIPE_ID 
      AND e.CATEGORIE_ID = 8
      AND r.NUMERO_SAISON = 1
      UNION
      -- Enregistre pour une équipe les points qu'elle a marqué ou 
      -- encaissé lorsqu'elle était l'équipe B
      SELECT e.EQUIPE_ID AS EQUIPE, 
      	     SCORE_EQUIPE_B AS POUR, 
	     SCORE_EQUIPE_A AS CONTRE, 
	     adv.EQUIPE_ID AS ADVERSAIRE
      FROM RENCONTRE r, EQUIPE adv, EQUIPE e 
      WHERE r.EQUIPE_B_ID = e.EQUIPE_ID 
      AND r.EQUIPE_A_ID = adv.EQUIPE_ID 
      AND e.CATEGORIE_ID = 8
      AND r.NUMERO_SAISON = 1
      
      ORDER BY EQUIPE) AS RESULT
GROUP BY EQUIPE) AS A,

-- Table calculant le total de points pour le classement d'une équipe
-- 3pts: Victoires, 1pt: Nul et 0pts: Défaites

(select EQUIPE_ID, sum(points) as PTS
 from (
       -- Cas où l'équipe est victorieuse
       select 3*count(*) points, EQUIPE_ID
       from (
       	    -- On regarde tour à tour le résultat quand l'équipe et l'équipe A
       	    select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID
     	    from RENCONTRE r, EQUIPE e
	    where e.CATEGORIE_ID = 8
	    and r.EQUIPE_A_ID = e.EQUIPE_ID
	    and r.NUMERO_SAISON = 1
	    and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B
	    UNION
	    -- Puis ses résultats lorsqu'elle est l'équipe B
	    select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID
	    from RENCONTRE r, EQUIPE e 
	    where e.CATEGORIE_ID = 8 
	    and r.EQUIPE_B_ID = e.EQUIPE_ID
	    and r.NUMERO_SAISON = 1
	    and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B) victoire
      	    group by EQUIPE_ID
      UNION
      -- Cas où l'on a match nul
      select count(*) points,EQUIPE_ID 
      from (select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID
      	   from RENCONTRE r,  EQUIPE e
	   where e.CATEGORIE_ID = 8
	   and r.EQUIPE_A_ID = e.EQUIPE_ID 
	   and r.NUMERO_SAISON = 1
	   and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B
	   UNION
	   select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID
	   from RENCONTRE r,  EQUIPE e 
	   where  e.CATEGORIE_ID = 8 
	   and r.EQUIPE_B_ID = e.EQUIPE_ID
	   and r.NUMERO_SAISON = 1
	   and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B) nul
      group by EQUIPE_ID
      union
      select 0*count(*) points, EQUIPE_ID AS ID
       from (
       	    -- On regarde tour à tour le résultat quand l'équipe et l'équipe A
       	    select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID
     	    from RENCONTRE r, EQUIPE e
	    where e.CATEGORIE_ID = 8
	    and r.EQUIPE_A_ID = e.EQUIPE_ID
	    and r.NUMERO_SAISON = 1
	    and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B
	    UNION
	    -- Puis ses résultats lorsqu'elle est l'équipe B
	    select e.EQUIPE_ID,r.SCORE_EQUIPE_A,r.SCORE_EQUIPE_B,r.RENCONTRE_ID
	    from RENCONTRE r, EQUIPE e 
	    where e.CATEGORIE_ID = 8 
	    and r.EQUIPE_B_ID = e.EQUIPE_ID
	    and r.NUMERO_SAISON = 1
	    and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B) victoire
      	    group by EQUIPE_ID) as total
group by EQUIPE_ID
ORDER BY sum(points) desc) as B,

-- Table affichant pour une équipe le nombre de match nul (0 inclu)
(SELECT e.EQUIPE_ID, COUNT(ID_NUL) AS nb_nuls
 FROM EQUIPE e
 LEFT JOIN (
      	   select r.RENCONTRE_ID, e.EQUIPE_ID AS ID_NUL
	   from RENCONTRE r, EQUIPE e
	   where e.EQUIPE_ID = r.EQUIPE_A_ID
	   and r.NUMERO_SAISON = 1
	   and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B

	   UNION

	   select r.RENCONTRE_ID, e.EQUIPE_ID AS ID_NUL
	   from RENCONTRE r, EQUIPE e
	   where e.EQUIPE_ID = r.EQUIPE_B_ID
	   and r.NUMERO_SAISON = 1
	   and r.SCORE_EQUIPE_A = r.SCORE_EQUIPE_B

ORDER BY RENCONTRE_ID
) AS NUL
ON e.EQUIPE_ID = NUL.ID_NUL
WHERE e.CATEGORIE_ID = 8
group by e.equipe_id) AS N,

(
SELECT e.EQUIPE_ID, COUNT(VAINQUEUR_ID) AS nb_victoires
FROM EQUIPE e
LEFT JOIN 
(
select r.RENCONTRE_ID, e.EQUIPE_ID AS VAINQUEUR_ID
from RENCONTRE r, EQUIPE e
where e.EQUIPE_ID = r.EQUIPE_A_ID
and r.NUMERO_SAISON = 1
and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B

UNION

select r.RENCONTRE_ID, e.EQUIPE_ID AS VAINQUEUR_ID
from RENCONTRE r, EQUIPE e
where e.EQUIPE_ID = r.EQUIPE_B_ID
and r.NUMERO_SAISON = 1
and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B

ORDER BY RENCONTRE_ID
) AS VICTOIRE 
ON e.EQUIPE_ID = VICTOIRE.VAINQUEUR_ID
WHERE e.CATEGORIE_ID = 8
GROUP BY e.EQUIPE_ID) AS V,

(
SELECT e.EQUIPE_ID, COUNT(PERDANT_ID) AS nb_defaites
FROM EQUIPE e
LEFT JOIN 
(
select r.RENCONTRE_ID, e.EQUIPE_ID AS PERDANT_ID
from RENCONTRE r, EQUIPE e
where e.EQUIPE_ID = r.EQUIPE_A_ID
and e.CATEGORIE_ID = 8
and r.NUMERO_SAISON = 1
and r.SCORE_EQUIPE_A < r.SCORE_EQUIPE_B

UNION

select r.RENCONTRE_ID, e.EQUIPE_ID AS PERDANT_ID
from RENCONTRE r, EQUIPE e
where e.EQUIPE_ID = r.EQUIPE_B_ID
and e.CATEGORIE_ID = 8
and r.NUMERO_SAISON = 1
and r.SCORE_EQUIPE_A > r.SCORE_EQUIPE_B

ORDER BY RENCONTRE_ID
) AS DEFAITE
ON e.EQUIPE_ID = DEFAITE.PERDANT_ID
WHERE e.CATEGORIE_ID = 8
GROUP BY e.EQUIPE_ID) AS D,

EQUIPE E, GetEquipe G

WHERE E.EQUIPE_ID = A.EQUIPE
AND E.EQUIPE_ID = B.EQUIPE_ID
AND E.EQUIPE_ID = V.EQUIPE_ID
AND E.EQUIPE_ID = D.EQUIPE_ID
AND E.EQUIPE_ID = N.EQUIPE_ID
AND E.EQUIPE_ID = G.EQUIPE_ID

ORDER BY PTS DESC

