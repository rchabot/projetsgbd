-- @arg: numSaison, numJournee, nomClubA, String categorieEquipeA, int numeroEquipeA, nomClubB, String categorieEquipeB, int numeroEquipeB
-- fct: feuille du match
-- revoit liste des joueurs du match, ou rien si le match n'existe pas entre les deux équipes

select p.PERSONNE_NOM, p.PERSONNE_PRENOM, eqA.CLUB_NOM AS EQUIPE_NOM, eqA.CATEGORIE_NOM AS EQUIPE_CATEGORIE, eqA.EQUIPE_NUMERO, s.STATS_POINTS, s.STATS_FAUTES 
from RENCONTRE r,  GetEquipe eqA, PERSONNE p, JOUEUR j, STATS s
where r.EQUIPE_A_ID = eqA.EQUIPE_ID  
and r.NUMERO_JOURNEE = 1 
and p.PERSONNE_ID = j.JOUEUR_ID 
and j.EQUIPE_ID = eqA.EQUIPE_ID 
and eqA.CLUB_NOM = 'CSP'
AND eqA.CATEGORIE_NOM = 'SENIOR'
AND eqA.EQUIPE_NUMERO = 1
AND r.RENCONTRE_ID = s.RENCONTRE_ID
AND j.JOUEUR_ID = s.JOUEUR_ID

UNION

select p.PERSONNE_NOM, p.PERSONNE_PRENOM, eqB.CLUB_NOM AS EQUIPE_NOM, eqB.CATEGORIE_NOM AS EQUIPE_CATEGORIE, eqB.EQUIPE_NUMERO, s.STATS_POINTS, s.STATS_FAUTES
from RENCONTRE r,  GetEquipe eqB, PERSONNE p, JOUEUR j, STATS s
where r.EQUIPE_B_ID = eqB.EQUIPE_ID
and eqB.CLUB_NOM = 'SA'
AND eqB.CATEGORIE_NOM = 'SENIOR'
AND eqB.EQUIPE_NUMERO = 1
and r.NUMERO_JOURNEE = 1 
and p.PERSONNE_ID = j.JOUEUR_ID 
and j.EQUIPE_ID = eqB.EQUIPE_ID 
AND r.RENCONTRE_ID = s.RENCONTRE_ID
AND j.JOUEUR_ID = s.JOUEUR_ID
