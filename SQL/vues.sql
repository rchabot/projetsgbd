-- creation vues 

create or replace view GetEquipe as
select e.EQUIPE_ID, c.CLUB_NOM, cat.CATEGORIE_NOM, e.EQUIPE_NUMERO
from EQUIPE e, CLUB c, CATEGORIE cat
where e.CLUB_ID = c.CLUB_ID
and e.CATEGORIE_ID = cat.CATEGORIE_ID
